//コンスタントバッファ
cbuffer global
{
	float4x4 g_WVP;			//座標変換の合成行列
	float g_Bright;			//ライトの明るさ
};


//頂点シェーダの出力用構造体
struct VS_OUT
{
	float4 pos    : SV_POSITION;		//位置座標
	float4 posLocal	: TEXCOORD0;		//ローカル座標の位置
};


//頂点シェーダ
//引数1：pos(頂点位置)
VS_OUT VS(float4 pos : POSITION)
{
	//出力
	VS_OUT outData;

	//スクリーン座標に変換
	outData.pos = mul(pos, g_WVP);

	//位置をローカル座標のまま渡す
	outData.posLocal = pos;

	return outData;
}


//ピクセルシェーダ
//引数1：inData(頂点情報)
float4 PS(VS_OUT inData) : SV_Target
{
	//明るさ
	float bright = 1.0f + 16.0f * (1.0f - g_Bright);

	//中心との距離
	float dis = length(inData.posLocal);

	//明かりの大きさ
	float lightBright = 1.0f - saturate((dis * dis - 1.0f) * bright);

	//最終的な色
	return float4(1.0f, 1.0f, 1.0f, lightBright);
}