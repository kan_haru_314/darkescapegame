/* コントロールポイント：トライ　 */
/* アウトプットタイプ　：ポリゴン */

//配列テクスチャのサンプラー
SamplerState g_ArraySampler : register(s0);

//テクスチャサンプラー
SamplerState g_TexSampler : register(s1);

//高さ用のテクスチャー
Texture2D<float> g_HeightTexture: register(t0);

//法線用のテクスチャー
Texture2D<float4> g_NormalTexture: register(t1);

//割合用のテクスチャー
Texture2D<float> g_RateTexture1: register(t2);
Texture2D<float> g_RateTexture2: register(t3);
Texture2D<float> g_RateTexture3: register(t4);
Texture2D<float> g_RateTexture4: register(t5);
Texture2D<float> g_RateTexture5: register(t6);


///////////////////////  [ 1 ]  //////////////////////////

//Dispテクスチャー
Texture2D<float4> g_DispTexture1: register(t7);

//Diffuseテクスチャー
Texture2D<float4> g_DiffTexture1: register(t8);

//Normalテクスチャー
Texture2D<float4> g_NorTexture1: register(t9);


///////////////////////  [ 2 ]  //////////////////////////

//Dispテクスチャー
Texture2D<float4> g_DispTexture2: register(t10);

//Diffuseテクスチャー
Texture2D<float4> g_DiffTexture2: register(t11);

//Normalテクスチャー
Texture2D<float4> g_NorTexture2: register(t12);


///////////////////////  [ 3 ]  //////////////////////////

//Dispテクスチャー
Texture2D<float4> g_DispTexture3: register(t13);

//Diffuseテクスチャー
Texture2D<float4> g_DiffTexture3: register(t14);

//Normalテクスチャー
Texture2D<float4> g_NorTexture3: register(t15);


///////////////////////  [ 4 ]  //////////////////////////

//Dispテクスチャー
Texture2D<float4> g_DispTexture4: register(t16);

//Diffuseテクスチャー
Texture2D<float4> g_DiffTexture4: register(t17);

//Normalテクスチャー
Texture2D<float4> g_NorTexture4: register(t18);


///////////////////////  [ 5 ]  //////////////////////////

//Dispテクスチャー
Texture2D<float4> g_DispTexture5: register(t19);

//Diffuseテクスチャー
Texture2D<float4> g_DiffTexture5: register(t20);

//Normalテクスチャー
Texture2D<float4> g_NorTexture5: register(t21);


////////////////////////////////////////////////////////



//コンスタントバッファ
cbuffer CONSTANT
{
	float4x4 g_WVP;			//座標変換の合成行列
	float4x4 g_NormalTrans;	//法線変換の合成行列
	float4x4 g_World;		//ワールド合成行列
	float4 g_EyePos;		//カメラの位置
	float4 g_LightPos;		//ライトの位置
	float g_MaxDistance;	//距離の最大値
	float g_MinDistance;	//距離の最低値
	float g_Bright;			//ライトの明るさ
	float g_SampleOffset;	//サンプルレベルの倍率
	float g_Height;			//高さの倍率
	int g_MaxDevide;		//最大分割数
};

//頂点シェーダの出力用構造体
struct VS_OUTPUT
{
	float3 pos : POSITION;			//位置座標
	float2 uv : TEXCOORD0;			//uv座標
};

//パッチ定数関数の出力用構造体
struct HS_CONSTANT_OUTPUT
{
	float outFactor[3] : SV_TessFactor;			//外側の分割数(辺)
	float inFactor : SV_InsideTessFactor;		//内側の分割数(点生成)
};

//ハルシェーダの出力用構造体
struct HS_OUTPUT
{
	float3 pos : POSITION;		//位置座標
	float2 uv : TEXCOORD0;		//uv座標
};

//ドメインシェーダの出力用構造体
struct DS_OUTPUT
{
	float4 pos : SV_POSITION;			//位置座標
	float4 posWorld : TEXCOORD0;		//ワールド座標の位置
	float4 normal : TEXCOORD1;			//法線
	float2 uv : TEXCOORD2;				//uv座標
};


//頂点シェーダ
//引数1：pos(頂点位置)
//引数2：uv(uv座標)
VS_OUTPUT VS(float4 pos : POSITION, float2 uv : TEXCOORD0)
{
	//出力
	VS_OUTPUT outData;

	//データトリミング
	outData.pos = pos.xyz;
	outData.uv = uv;

	return outData;
}


//パッチ定数関数
//引数1：ip(コントロールポイント)<頂点 * 3>
//引数2：pid(パッチの番号)
HS_CONSTANT_OUTPUT HSConstant(InputPatch<VS_OUTPUT, 3> ip, uint pid : SV_PrimitiveID)
{
	//出力
	HS_CONSTANT_OUTPUT outData;
	outData.inFactor = 0;

	//距離計算
	float3 centerPos[3];
	centerPos[0] = ip[2].pos + (ip[1].pos - ip[2].pos) / 2.0f;
	centerPos[1] = ip[2].pos + (ip[0].pos - ip[2].pos) / 2.0f;
	centerPos[2] = ip[1].pos + (ip[0].pos - ip[1].pos) / 2.0f;

	//分割計算
	for (int i = 0; i < 3; i++)
	{
		//ワールド座標に変換
		centerPos[i] = mul(float4(centerPos[i], 0.0f), g_World).xyz;

		//視点との距離計算
		float eyeDistance = clamp(distance(g_EyePos, centerPos[i]), g_MinDistance, g_MaxDistance);

		//分割率
		float rate = 1.0f - (eyeDistance - g_MinDistance) / (g_MaxDistance - g_MinDistance);

		//外側の分割数
		outData.outFactor[i] = clamp(rate * g_MaxDevide, 1, g_MaxDevide);

		//内側の分割数
		outData.inFactor += outData.outFactor[i];
	}

	//内側の分割数を平均値に
	outData.inFactor /= 3;

	return outData;
}


//ハルシェーダの設定
[domain("tri")]							//ドメインタイプの指定
[partitioning("integer")]				//int(整数) or float(小数点数)
[outputtopology("triangle_cw")]			//分割後ポリゴンの形指定
[outputcontrolpoints(3)]				//出力コントロールポイントの数（HS起動回数）
[patchconstantfunc("HSConstant")]		//パッチ定数関数の名前指定

//ハルシェーダ
//引数1：ip(コントロールポイント)<頂点 * 3>
//引数2：cpid(出力パッチの番号)
//引数3：pid(パッチの番号)
HS_OUTPUT HS(InputPatch<VS_OUTPUT, 3> ip, uint cpid : SV_OutputControlPointID, uint pid : SV_PrimitiveID)
{
	//出力
	HS_OUTPUT outData;

	//編集せずそのまま受け渡し
	outData.pos = ip[cpid].pos;
	outData.uv = ip[cpid].uv;

	return outData;
}


//ドメインシェーダの設定
[domain("tri")]		//ドメインタイプの指定

//ドメインシェーダ
//引数1：inData(分割データ)
//引数2：varia(偏差値)
//引数3：patch(コントロールポイント)
DS_OUTPUT DS(HS_CONSTANT_OUTPUT inData, float3 varia : SV_DomaInLocation, const OutputPatch<HS_OUTPUT, 3> patch)
{
	//出力
	DS_OUTPUT outData;
	
	//変化計算
	float4 pos = float4(patch[0].pos * varia.x + patch[1].pos * varia.y + patch[2].pos * varia.z, 1.0f);
	float2 uv = patch[0].uv * varia.x + patch[1].uv * varia.y + patch[2].uv * varia.z;
	
	//画像から値を取得
	float height = g_HeightTexture.SampleLevel(g_ArraySampler, uv, 0) * g_Height;
	float4 normal = g_NormalTexture.SampleLevel(g_ArraySampler, uv, 0);

	float4 disp = float4(0, 0, 0, 0);
	disp = g_DispTexture1.SampleLevel(g_TexSampler, uv * g_SampleOffset, 0) * g_RateTexture1.SampleLevel(g_ArraySampler, uv * g_SampleOffset, 0);
	disp += g_DispTexture2.SampleLevel(g_TexSampler, uv * g_SampleOffset, 0) * g_RateTexture2.SampleLevel(g_ArraySampler, uv * g_SampleOffset, 0);
	disp += g_DispTexture3.SampleLevel(g_TexSampler, uv * g_SampleOffset, 0) * g_RateTexture3.SampleLevel(g_ArraySampler, uv * g_SampleOffset, 0);
	disp += g_DispTexture4.SampleLevel(g_TexSampler, uv * g_SampleOffset, 0) * g_RateTexture4.SampleLevel(g_ArraySampler, uv * g_SampleOffset, 0);
	disp += g_DispTexture5.SampleLevel(g_TexSampler, uv * g_SampleOffset, 0) * g_RateTexture5.SampleLevel(g_ArraySampler, uv * g_SampleOffset, 0);
	disp -= 0.5f;
	
	//位置
	pos.y += height + disp.x;
	outData.pos = mul(pos, g_WVP);

	//ワールド座標の位置
	outData.posWorld = mul(pos, g_World);

	//法線
	normal = normal * 2.0f - 1.0f;
	normal.w = 0.0f;
	outData.normal = mul(normal, g_NormalTrans);
	
	//uv
	outData.uv = uv;

	return outData;
}


//ピクセルシェーダ
//引数1：inData(頂点情報)
float4 PS(DS_OUTPUT inData) : SV_Target
{
	//色
	float4 diffuse = float4(0,0,0,0);
	diffuse = g_DiffTexture1.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture1.SampleLevel(g_ArraySampler, inData.uv* g_SampleOffset, 0);
	diffuse += g_DiffTexture2.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture2.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	diffuse += g_DiffTexture3.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture3.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	diffuse += g_DiffTexture4.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture4.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	diffuse += g_DiffTexture5.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture5.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	diffuse.a = 1.0f;

	//バンプ法線
	float4 bump = float4(0, 0, 0, 0);
	bump = g_NorTexture1.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture1.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	bump += g_NorTexture2.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture2.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	bump += g_NorTexture3.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture3.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	bump += g_NorTexture4.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture4.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	bump += g_NorTexture5.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * g_RateTexture5.SampleLevel(g_ArraySampler, inData.uv * g_SampleOffset, 0);
	bump.a = 0.0f;

	bump = bump * 2.0f - 1.0f;

	//float4 bump = g_BumpTexture.SampleLevel(g_TexSampler, inData.uv * g_SampleOffset, 0) * 2.0f - 1.0f;
	//bump = bump.rbga;		//gleen, blue データ入れ替え
	//bump.g *= -1;			//gleenの反転

	/*
	//ライトの向き
	float4 lightDir = g_LightPos - inData.posWorld;
	lightDir.w = 0.0f;

	//ライトとの距離
	float lightLength = length(lightDir) / 12.0f;

	//正規化
	lightDir = normalize(lightDir);							//ライトの向き
	inData.normal = normalize(inData.normal + bump);		//法線

	//ポイントライトの照らす範囲
	float bright = 1.0f - g_Bright;
	float lightBright = saturate(1.0f / (bright + bright * lightLength + bright * lightLength * lightLength)) * 2.0f;
	*/

	//ライト
	float4 lightDir = float4(1, 1, 0, 0);

	//正規化
	lightDir = normalize(lightDir);							//ライトの向き
	inData.normal = normalize(inData.normal * bump);				//法線

	//影
	float4 shade = saturate(dot(inData.normal, -lightDir));// *lightBright;
	shade.a = 1.0f;

	//環境光（アンビエント）
	float4 ambient = float4(0.2f, 0.2f, 0.2f, 1.0f);

	//最終的な色
	return diffuse * shade + diffuse * ambient;
}  