#pragma once

//インクルード
#include "Collider.h"


//箱型の当たり判定
class BoxCollider :	public Collider
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フレンドクラス ------------------//
	friend class Collider;


	//------------------ メソッド ------------------//

	//接触判定
	//引数：Collider*(相手の当たり判定)
	//返値：bool(true = 接触, false = 未接触)
	bool IsHit(Collider* target) override;



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ（当たり判定の作成）
	//引数：XMVECTOR(当たり判定の中心位置(ゲームオブジェクトの原点から見た位置))
	//引数：XMVECTOR(当たり判定のサイズ(幅、高さ、奥行き))
	//返値：なし
	BoxCollider(XMVECTOR basePos, XMVECTOR size);
};