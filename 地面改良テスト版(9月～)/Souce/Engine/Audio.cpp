/********************************************************/
/*						未勉強領域						*/
/********************************************************/

//インクルード
#include <xaudio2.h>
#include <vector>
#include "Audio.h"

#include "Global.h"

//サウンドを管理する（XAudio使用）
namespace Audio
{
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//

	//ファイル毎に必要な情報
	struct AudioData
	{
		//サウンド情報
		XAUDIO2_BUFFER buf_ = {};

		//ソースボイス
		IXAudio2SourceVoice** pSourceVoice_ = nullptr;

		//同時再生最大数
		int svNum_;

		//ファイル名
		std::string fileName_;
	};


	//------------------ 変数 ------------------//
	IXAudio2* pXAudio_ = nullptr;							//XAudio本体
	IXAudio2MasteringVoice* pMasteringVoice_ = nullptr;		//マスターボイス
	std::vector<AudioData>	audioDatas_;					//オーディオ情報
	HRESULT checkResult_;									//状態チェック用
}



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------//

//初期化
HRESULT Audio::Initialize()
{
	checkResult_ = CoInitializeEx(0, COINIT_MULTITHREADED);
	CHECK_MESSAGE(checkResult_, "Audio::Initialize - Error");

	checkResult_ = XAudio2Create(&pXAudio_);
	CHECK_MESSAGE(checkResult_, "Audio::Initialize - Error");

	checkResult_ = pXAudio_->CreateMasteringVoice(&pMasteringVoice_);
	CHECK_MESSAGE(checkResult_, "Audio::Initialize - Error");
}

//サウンドファイル(.wav）をロード
int Audio::Load(std::string fileName, int svNum)
{
	//すでに同じファイルを使ってないかチェック
	for (int i = 0; i < audioDatas_.size(); i++)
	{
		if (audioDatas_[i].fileName_ == fileName)
		{
			return i;
		}
	}


	struct Chunk
	{
		char	id[4]; 			// ID
		unsigned int	size;	// サイズ
	};

	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	DWORD dwBytes = 0;

	Chunk riffChunk;
	ReadFile(hFile, &riffChunk, 8, &dwBytes, NULL);

	char wave[4];
	ReadFile(hFile, &wave, 4, &dwBytes, NULL);

	Chunk formatChunk;
	ReadFile(hFile, &formatChunk, 8, &dwBytes, NULL);

	WAVEFORMATEX	fmt;
	ReadFile(hFile, &fmt, formatChunk.size, &dwBytes, NULL);

	Chunk data;
	ReadFile(hFile, &data, 8, &dwBytes, NULL);

	char* pBuffer = new char[data.size];
	ReadFile(hFile, pBuffer, data.size, &dwBytes, NULL);

	CloseHandle(hFile);


	AudioData ad;

	ad.fileName_ = fileName;

	ad.buf_.pAudioData = (BYTE*)pBuffer;
	ad.buf_.Flags = XAUDIO2_END_OF_STREAM;
	ad.buf_.AudioBytes = data.size;

	ad.pSourceVoice_ = new IXAudio2SourceVoice*[svNum];
	for (int i = 0; i < svNum; i++)
	{
		pXAudio_->CreateSourceVoice(&ad.pSourceVoice_[i], &fmt);
	}
	ad.svNum_ = svNum;
	audioDatas_.push_back(ad);

	//SAFE_DELETE_ARRAY(pBuffer);

	return (int)audioDatas_.size() - 1;
}

//再生
void Audio::Play(int ID)
{
	for (int i = 0; i < audioDatas_[ID].svNum_; i++)
	{
		XAUDIO2_VOICE_STATE state;
		audioDatas_[ID].pSourceVoice_[i]->GetState(&state);

		if (state.BuffersQueued == 0)
		{
			audioDatas_[ID].pSourceVoice_[i]->SubmitSourceBuffer(&audioDatas_[ID].buf_);
			audioDatas_[ID].pSourceVoice_[i]->Start();
			break;
		}
	}
}

//すべて開放
void Audio::Release()
{
	for (int i = 0; i < audioDatas_.size(); i++)
	{
		for (int j = 0; j < audioDatas_[i].svNum_; j++)
		{
			audioDatas_[i].pSourceVoice_[j]->DestroyVoice();
		}
		SAFE_DELETE_ARRAY(audioDatas_[i].buf_.pAudioData);
	}

	CoUninitialize();
	if (pMasteringVoice_)
	{
		pMasteringVoice_->DestroyVoice();
	}
	pXAudio_->Release();
}
