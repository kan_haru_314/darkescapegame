#pragma once

//インクルード
#include <fbxsdk.h>
#include <d3d11.h>
#include <DirectXMath.h>

#include "Texture.h"
#include "Transform.h"

//ネームスペース
using namespace DirectX;

//プロトタイプ宣言
class Fbx;
struct RayCastData;


//FBXの１つのパーツを扱うクラス
class FbxParts
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//

	//一つの頂点情報を格納する構造体
	struct VERTEX
	{
		XMVECTOR position_;		//位置座標
		XMVECTOR normal_;		//法線
		XMVECTOR uv_;			//テクスチャ座標
	};

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX worldVewProj_;		//ワールド、ビュー、プロジェクション行列の合成（頂点変換に使用）
		XMMATRIX normalTrans_;		//回転行列と拡大行列の逆行列を合成したもの（法線の変形に使用）
		XMMATRIX worldTrans_;		//ワールド行列の合成（頂点変換に使用）
		XMFLOAT4 cameraPos_;		//カメラの位置ベクトル
		XMFLOAT4 lightPos_;			//点光源の位置ベクトル
		XMFLOAT4 diffuse_;			//ディフューズカラー。マテリアルの色。（テクスチャ貼ってるときは使わない）
		XMFLOAT4 ambient_;			//アンビエント
		XMFLOAT4 speculer_;			//スペキュラー（Lambertの場合は0）
		FLOAT	 bright_;			//ライトの明るさ
		FLOAT	 shininess_;		//ハイライトの強さ（MayaのCosinePower）
		FLOAT	 sampleOffset_;		//オフセット
		BOOL	 isTexture_;		//テクスチャの有無
		BOOL	 isNormalTexture_;	//ノーマルマップテクスチャの有無
	};

	// マテリアル情報（質感の情報）
	struct MATERIAL
	{
		XMFLOAT4 diffuse_;			//拡散反射光（ディフューズ）への反射強度
		XMFLOAT4 ambient_;			//環境光（アンビエント）への反射強度
		XMFLOAT4 specular_;			//鏡面反射光（スペキュラ）への反射強度
		DWORD	 polygonCount_;		//マテリアルのポリゴン数
		float	 shininess_;		//ハイライトの強さ（サイズ）
		Texture* pTexture_;			//テクスチャ
		Texture* pNormalTexture_;	//ノーマルマップテクスチャ
	} *pMaterial_;

	// ボーン構造体（関節情報）
	struct Bone
	{
		XMMATRIX bindPose_;      //初期ポーズ時のボーン変換行列
		XMMATRIX newPose_;       //アニメーションで変化したときのボーン変換行列
		XMMATRIX diffPose_;      //mBindPose に対する mNowPose の変化量
	};

	// ウェイト構造体（ボーンと頂点の関連付け）
	struct Weight
	{
		XMVECTOR posOrigin_;			//元々の頂点座標
		XMVECTOR normalOrigin_;		//元々の法線ベクトル
		int*	 pBoneIndex_;		//関連するボーンのID
		float*	 pBoneWeight_;		//ボーンの重み
	};

	//------------------- フィールド -------------------//
	HRESULT checkResult_;					//状態チェック用

	//------------------- データ数 -------------------//
	DWORD vertexCount_;				//頂点数
	DWORD polygonCount_;			//ポリゴン数
	DWORD indexCount_;				//インデックス数
	DWORD materialCount_;			//マテリアルの個数
	DWORD polygonVertexCount_;		//ポリゴン頂点インデックス数 

	//------------------- データ -------------------//
	VERTEX* pVertexData_;			//頂点情報
	DWORD** ppIndexData_;			//インデックス情報

	//------------------- バッファ -------------------//
	ID3D11Buffer*  pVertexBuffer_;			//頂点バッファ
	ID3D11Buffer** ppIndexBuffer_;			//インデックスバッファ
	ID3D11Buffer*  pConstantBuffer_;		//定数バッファ

	//------------------- ボーン制御情報 -------------------//
	FbxSkin*		pSkinInfo_;			//スキンメッシュ情報（スキンメッシュアニメーションのデータ本体）
	FbxCluster**	ppCluster_;			//クラスタ情報（関節ごとに関連付けられた頂点情報）
	Bone*			pBoneArray_;		//各関節の情報
	Weight*			pWeightArray_;		//ウェイト情報（頂点の対する各関節の影響度合い）
	int				numBone_;			//FBXに含まれている関節の数


	//------------------- メソッド -------------------//

	//頂点バッファ準備
	//引数：fbxsdk::FbxMesh*(fbxファイル名)
	//返値：HRESULT(状態)
	HRESULT InitVertex(fbxsdk::FbxMesh* mesh);

	//インデックスバッファ準備
	//引数：fbxsdk::FbxMesh*(fbxファイル名)
	//返値：HRESULT(状態)
	HRESULT InitIndex(fbxsdk::FbxMesh* mesh);

	//テクスチャ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitTexture(fbxsdk::FbxSurfaceMaterial* pMaterial, const DWORD& i);

	//コンスタントバッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitConstantBuffer();

	//マテリアル準備
	//引数：fbxsdk::FbxNode*(fbxノード)
	//返値：HRESULT(状態)
	HRESULT InitMaterial(fbxsdk::FbxNode * pNode);

	//骨の情報を準備
	//引数：fbxsdk::FbxNode*(fbxノード)
	//返値：void
	void InitSkelton(FbxMesh * pMesh);



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------- メソッド -------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	FbxParts();

	//デストラクタ
	//引数：なし
	//返値：なし
	~FbxParts();

	//初期化
	//引数：FbxNode*(情報が入っているノード)
	//返値：HRESULT(状態)
	HRESULT Init(FbxNode* pNode);

	//描画
	//引数：Transform&(ワールド行列)
	//引数：float(uvの縮尺)
	//返値：void
	void Draw(Transform& transform, float sampleOffset);

	//ボーン有りのモデルを描画
	//引数：Transform&(行列情報)
	//引数：FbxTime(現在のフレーム情報)
	//引数：float(uvの縮尺)
	//返値：void
	void DrawSkinAnime(Transform& transform, FbxTime time, float offset);

	//ボーン無しのモデルを描画
	//引数：Transform&(行列情報)
	//引数：FbxTime(現在のフレーム情報)
	//引数：FbxScene*(シーン情報)
	//引数：float(uvの縮尺)
	//返値：void
	void DrawMeshAnime(Transform& transform, FbxTime time, FbxScene* scene, float offset);

	//任意のボーンの位置を取得
	//引数：std::string(取得したいボーンの名前)
	//引数：XMVECTOR*(ワールド座標での位置) { 出力用 }
	//返値：bool(true = hit, false = nohit)
	bool GetBonePosition(std::string boneName, XMVECTOR* position);

	//スキンメッシュ情報を取得
	//引数：なし
	//返値：FbxSkin*(スキンメッシュ情報)
	FbxSkin* GetSkinInfo() { return pSkinInfo_; }

	//レイキャスト（レイを飛ばして当たり判定）
	//引数：RayCastData*(飛ばすレイのデータ)
	//返値：void
	void RayCast(RayCastData* data);
};

