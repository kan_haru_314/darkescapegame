//インクルード
#include "Text.h"

#include <vector>

#include "Global.h"

//文字表示する名前空間
namespace Text
{
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//

	//テキストの書式
	struct TextFormat
	{
		IDWriteTextFormat* pTextFormat_;		//フォーマット
		float fontSize_;						//文字の大きさ
		std::string fontType_;					//文字の書式
	};

	//テキストの色
	struct TextColor
	{
		ID2D1SolidColorBrush* pSolidBrush_;		//ブラシ
		D2D1::ColorF::Enum tectColor_;			//色
	};

	//テキストのデータ
	struct TextData
	{
		std::wstring text_;		//テキスト
		Point point_;			//座標
		TextFormat* format_;	//書式
		TextColor* color_;		//色
	};


	//------------------ 変数 ------------------//

	std::vector<TextData*> datas_;		//テキストデータのリスト
	std::vector<TextFormat*> formats_;	//テキストの書式のリスト
	std::vector<TextColor*> colors_;	//テキストの色のリスト

	std::vector<bool> isDraw_;			//描画するかどうかのリスト


	//------------------ 関数 ------------------//

	//String -> WString へ変換
	//引数：std::string(変換したい文字列)
	//返値：std::wstring(変換した文字列)
	std::wstring StringToWString(std::string str)
	{
		//SJIS → wstring
		int bufferSize = MultiByteToWideChar(CP_ACP, 0, str.c_str()
			, -1, (wchar_t*)NULL, 0);

		//バッファの取得
		wchar_t* cpUCS2 = new wchar_t[bufferSize];

		//SJIS → wstring
		MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, cpUCS2
			, bufferSize);

		//stringの生成
		std::wstring ret(cpUCS2, cpUCS2 + bufferSize - 1);

		//バッファの破棄
		delete[] cpUCS2;

		//変換結果を返す
		return(ret);
	}

	//フォーマット作成
	//引数：string(フォント)
	//引数：float(大きさ)
	//返値：TextFormat*(要求されたフォーマット)
	TextFormat* CreateFormat(std::string fontType, float fontSize)
	{
		//フォーマット探し
		TextFormat* pFormat = nullptr;
		for (TextFormat *format_ : formats_)
		{
			if (format_->fontSize_ == fontSize && format_->fontType_ == fontType)
			{
				pFormat = format_;
			}
		}

		//新規作成
		if (!pFormat)
		{
			pFormat = new TextFormat;
			pFormat->fontSize_ = fontSize;
			pFormat->fontType_ = fontType;

			Direct2D::pDWriteFactory->CreateTextFormat(
				std::wstring(fontType.begin(), fontType.end()).c_str(),
				nullptr,
				DWRITE_FONT_WEIGHT_NORMAL,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				fontSize,
				L"",
				&pFormat->pTextFormat_
			);
			pFormat->pTextFormat_->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		}

		return pFormat;
	}

	//色のブラシ作成
	//引数：ColorF::Enum(色)
	//返値：TextColor*(要求された色のブラシ)
	TextColor* CreateColor(D2D1::ColorF::Enum color)
	{
		//カラー探し
		TextColor* pColor = nullptr;
		for (TextColor *color_ : colors_)
		{
			if (color_->tectColor_)
			{
				pColor = color_;
			}
		}

		//新規作成
		if (!pColor)
		{
			pColor = new TextColor;
			pColor->tectColor_ = color;

			Direct2D::pRT->CreateSolidColorBrush(D2D1::ColorF(color), &pColor->pSolidBrush_);
		}

		return pColor;
	}
}

//テキストデータをセット
int Text::SetText(std::string text, std::string fontType, Point point, float fontSize, D2D1::ColorF::Enum color)
{
	//書式作成
	TextFormat* pFormat = CreateFormat(fontType, fontSize);

	//色作成
	TextColor* pColor = CreateColor(color);

	//テキストデータを登録
	TextData* pData = new TextData;
	pData->format_ = pFormat;
	pData->color_ = pColor;
	pData->point_ = point;
	pData->text_ = StringToWString(text);

	datas_.push_back(pData);
	isDraw_.push_back(true);


	//番号を返す
	return (int)(datas_.size()) - 1;
}

//文字変更
void Text::ChangeText(int handle, std::string text)
{
	datas_[handle]->text_ = std::wstring(text.begin(), text.end());
}

//位置変更
void Text::ChangePoint(int handle, Point point)
{
	datas_[handle]->point_ = point;
}

//色変更
void Text::ChangeColor(int handle, D2D1::ColorF::Enum color)
{
	datas_[handle]->color_ = CreateColor(color);
}

//書式変更
void Text::ChangeFormat(int handle, std::string fontType, float fontSize)
{
	datas_[handle]->format_ = CreateFormat(fontType, fontSize);
}

//rgb -> ColorF::Enum に変換
D2D1::ColorF::Enum Text::ConvertColor(unsigned char red, unsigned char gleen, unsigned char blue)
{
	int iRed = (int)red << 16;
	int iGreen = (int)gleen << 8;
	int iBlue = (int)blue;

	return (D2D1::ColorF::Enum)(iRed + iGreen + iBlue);
}

//描画許可
void Text::Visible(int handle)
{
	isDraw_[handle] = true;
}

//描画拒否
void Text::Invisible(int handle)
{
	isDraw_[handle] = false;
}

//描画
void Text::Draw()
{
	//テキストの描画
	for (int i = 0; i < datas_.size(); i++)
	{
		if (isDraw_[i])
		{
			TextData* data = datas_[i];

			Direct2D::pRT->DrawText(
				data->text_.c_str(),
				(int)(data->text_.size()),
				data->format_->pTextFormat_,
				D2D1::RectF(data->point_.x_, data->point_.y_, 1920, 1080),
				data->color_->pSolidBrush_,
				D2D1_DRAW_TEXT_OPTIONS_NONE
			);
		}
	}
}

//開放処理
void Text::Release()
{
	//テキストデータ解放
	for (TextData* data : datas_)
	{
		SAFE_DELETE(data);
	}
	datas_.clear();

	//色データの解放
	for (TextColor* color : colors_)
	{
		SAFE_RELEASE(color->pSolidBrush_);
		SAFE_DELETE(color);
	}
	colors_.clear();

	//書式データの解放
	for (TextFormat* format : formats_)
	{
		SAFE_RELEASE(format->pTextFormat_);
		SAFE_DELETE(format);
	}
	formats_.clear();

	//描画判断のクリア
	isDraw_.clear();
}
