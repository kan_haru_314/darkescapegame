#pragma once

//インクルード
#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

//リンカ
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

//ネームスペース
using namespace DirectX;


//画面の描画に関する処理
namespace Direct3D
{
	//----------------------------- [ public ] -----------------------------//
	
	//------------------ 列挙型 ------------------//
	
	//シェーダーの種類
	enum SHADER_TYPE {
		SHADER_3D,			//基本的な3D
		SHADER_2D,			//基本的な2D
		SHADER_UNLIT,		//衝突判定確認用
		SHADER_DISPLAY,		//ディスプレイメント
		SHADER_LIGHT,		//ポイントライト
		SHADER_MAX			//シェーダー総数
	};


	//------------------ 構造体 ------------------//
	
	//使用するシェーダー機能
	struct SHADER_BUNDLE
	{
		ID3D11InputLayout* pVertexLayout_;				//頂点インプットレイアウト
		ID3D11VertexShader* pVertexShader_;				//頂点シェーダ
		ID3D11PixelShader* pPixelShader_;				//ピクセルシェーダ
		ID3D11HullShader* pHullShader_;					//ハルシェーダ
		ID3D11DomainShader* pDomainShader_;				//ドメインシェーダ
		ID3D11RasterizerState* pRasterizerState_;		//ラスタライザ
	};


	//------------------ 変数 ------------------//
	extern ID3D11Device*           pDevice_;			//描画を行うための環境やリソースの作成に使う
	extern ID3D11DeviceContext*    pContext_;			//GPUに命令を出すためのもの
	extern IDXGISwapChain* pSwapChain_;					//スワップチェーン
	extern SHADER_BUNDLE shaderBundle_[SHADER_MAX];		//シェーダーのタイプ別機能

	extern int		screenWidth_;						//スクリーンの幅
	extern int		screenHeight_;						//スクリーンの高さ
	extern bool		isDrawCollision_;					//コリジョンを表示するかフラグ


	//------------------ 関数 ------------------//

	//初期化
	//引数：HWND(ウィンドウハンドル)
	//引数：int(ウィンドウ幅)
	//引数：int(ウィンドウ高さ)
	//返値：HRESULT(状態)
	HRESULT Initialize(HWND hWnd, int screenWidth, int screenHeight);

	//シェーダー準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitShaderBundle();

	//描画するShaderBundleを設定
	//引数：SHADER_TYPE(シェーダーの種類）
	//返値：void
	void SetShader(SHADER_TYPE type);

	//描画開始
	//引数：なし
	//返値：void
	void BeginDraw();

	//描画終了
	//引数：なし
	//返値：void
	void EndDraw();

	//解放
	//引数：なし
	//返値：void
	void Release();

	//三角形と線分（レイ）の衝突判定（衝突判定に使用）
	//引数：XMVECTOR&(レイのスタート位置）
	//引数：XMVECTOR&(レイの方向）
	//引数：XMVECTOR&(三角形の頂点位置1)
	//引数：XMVECTOR&(三角形の頂点位置2)
	//引数：XMVECTOR&(三角形の頂点位置3)
	//引数：float*(衝突点までの距離を返す)
	//返値：bool(true = 衝突した, false = 衝突してない)
	bool Intersect(XMVECTOR& start, XMVECTOR& direction, XMVECTOR& v0, XMVECTOR& v1, XMVECTOR& v2, float* distance);

	//Zバッファへの書き込みON/OFF
	//引数：bool(true = 書き込む, false = 書き込みしない)
	//返値：void
	void SetDepthBafferWriteEnable(bool isWrite);
};

