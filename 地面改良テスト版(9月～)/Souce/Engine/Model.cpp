//インクルード
#include "Model.h"

#include "Global.h"


//3Dモデル（FBXファイル）を管理する
namespace Model
{
	//----------------------------- [ private ] -----------------------------//
	
	//------------------ 変数 ------------------//
	 
	//ロード済みのモデルデータ一覧
	std::vector<ModelData*>	datas_;
}



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------//

//初期化
void Model::Initialize()
{
	AllRelease();
}

//モデルをロード
int Model::Load(std::string fileName)
{
	ModelData* pData = new ModelData;


	//開いたファイル一覧から同じファイル名のものが無いか探す
	bool isExist = false;
	for (int i = 0; i < datas_.size(); i++)
	{
		//すでに開いている場合
		if (datas_[i] != nullptr && datas_[i]->fileName_ == fileName)
		{
			pData->pFbx_ = datas_[i]->pFbx_;
			isExist = true;
			break;
		}
	}

	//新たにファイルを開く
	if (isExist == false)
	{
		pData->pFbx_ = new Fbx;
		if (pData->pFbx_->Load(fileName) != S_OK)
		{
			//開けなかった
			SAFE_DELETE(pData->pFbx_);
			SAFE_DELETE(pData);
			return -1;
		}

		//無事開けた
		pData->fileName_ = fileName;
	}


	//使ってない番号が無いか探す
	for (int i = 0; i < datas_.size(); i++)
	{
		if (datas_[i] == nullptr)
		{
			datas_[i] = pData;
			return i;
		}
	}

	//新たに追加
	datas_.push_back(pData);
	return (int)datas_.size() - 1;
}



//描画
void Model::Draw(int handle, float offset)
{
	if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
	{
		return;
	}

	//アニメーションを進める
	datas_[handle]->nowFrame_ += datas_[handle]->animSpeed_;

	//最後までアニメーションしたら戻す
	if (datas_[handle]->nowFrame_ > (float)datas_[handle]->endFrame_)
		datas_[handle]->nowFrame_ = (float)datas_[handle]->startFrame_;



	if (datas_[handle]->pFbx_)
	{
		datas_[handle]->pFbx_->Draw(datas_[handle]->transform_, (int)datas_[handle]->nowFrame_, offset);
	}
}


//任意のモデルを開放
void Model::Release(int handle)
{
	if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
	{
		return;
	}

	//同じモデルを他でも使っていないか
	bool isExist = false;
	for (int i = 0; i < datas_.size(); i++)
	{
		//すでに開いている場合
		if (datas_[i] != nullptr && i != handle && datas_[i]->pFbx_ == datas_[handle]->pFbx_)
		{
			isExist = true;
			break;
		}
	}

	//使ってなければモデル解放
	if (isExist == false)
	{
		SAFE_DELETE(datas_[handle]->pFbx_);
	}


	SAFE_DELETE(datas_[handle]);
}


//全てのモデルを解放
void Model::AllRelease()
{
	for (int i = 0; i < datas_.size(); i++)
	{
		if (datas_[i] != nullptr)
		{
			Release(i);
		}
	}
	datas_.clear();
}


//アニメーションのフレーム数をセット
void Model::SetAnimFrame(int handle, int startFrame, int endFrame, float animSpeed)
{
	datas_[handle]->SetAnimFrame(startFrame, endFrame, animSpeed);
}

//アニメーションスピードをセット
void Model::SetAnimSpeedFrame(int handle, float frame)
{
	datas_[handle]->animSpeed_ = frame;
}

//現在のアニメーションのフレームをセット
void Model::SetNowAnimFrame(int handle, float frame)
{
	datas_[handle]->nowFrame_ = frame;
}

//現在のアニメーションのフレームを取得
int Model::GetNowAnimFrame(int handle)
{
	return (int)datas_[handle]->nowFrame_;
}


//任意のボーンの位置を取得
XMVECTOR Model::GetBonePosition(int handle, std::string boneName)
{
	XMVECTOR pos = datas_[handle]->pFbx_->GetBonePosition(boneName);
	return  XMVector3TransformCoord(pos, datas_[handle]->transform_.GetWorldMatrix());
}


//ワールド行列を設定
void Model::SetTransform(int handle, Transform& transform)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return;
	}

	datas_[handle]->transform_ = transform;
}


//ワールド行列の取得
XMMATRIX Model::GetMatrix(int handle)
{
	return datas_[handle]->transform_.GetWorldMatrix();
}


//レイキャスト（レイを飛ばして当たり判定）
void Model::RayCast(int handle, RayCastData* data)
{
	XMVECTOR target = data->start + data->dir;
	XMMATRIX matInv = XMMatrixInverse(nullptr, datas_[handle]->transform_.GetWorldMatrix());
	data->start = XMVector3TransformCoord(data->start, matInv);
	target = XMVector3TransformCoord(target, matInv);
	data->dir = target - data->start;

	datas_[handle]->pFbx_->RayCast(data);
}