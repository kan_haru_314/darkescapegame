//インクルード
#include "BoxCollider.h"

#include "SphereCollider.h"
#include "Model.h"


//コンストラクタ（当たり判定の作成）
BoxCollider::BoxCollider(XMVECTOR basePos, XMVECTOR size)
{
	center_ = basePos;
	size_ = size;
	type_ = COLLIDER_BOX;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	hDebugModel_ = Model::Load("DebugCollision/boxCollider.fbx");
#endif
}

//接触判定
bool BoxCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsBox((BoxCollider*)target, this);
	else
		return IsHitBoxVsCircle(this, (SphereCollider*)target);
}