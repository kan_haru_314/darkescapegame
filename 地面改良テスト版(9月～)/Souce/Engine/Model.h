#pragma once

//インクルード
#include <DirectXMath.h>
#include <assert.h>
#include <vector>
#include <string>

#include "Fbx.h"
#include "Transform.h"


//3Dモデル（FBXファイル）を管理する
namespace Model
{
	//----------------------------- [ public ] -----------------------------//

	//------------------ 構造体 ------------------//
	
	//モデル情報
	struct ModelData
	{
		//ファイル名
		std::string fileName_;

		//ロードしたモデルデータのアドレス
		Fbx*		pFbx_;

		//行列
		Transform 	transform_;

		//アニメーションのフレーム
		float nowFrame_;		//現在のフレーム数
		float animSpeed_;		//アニメーションスピード
		int startFrame_;		//開始フレーム
		int endFrame_;			//終了フレーム


		//初期化
		ModelData()
			: fileName_("")
			, pFbx_(nullptr)
			, nowFrame_(0)
			, startFrame_(0)
			, endFrame_(0)
			, animSpeed_(0)
		{
		}

		//アニメーションのフレーム数をセット
		//引数：int(開始フレーム)
		//引数：int(終了フレーム)
		//引数：int(アニメーション速度)
		void SetAnimFrame(int start, int end, float speed)
		{
			startFrame_ = start;
			endFrame_ = end;
			animSpeed_ = speed;
		}
	};


	//------------------ 関数 ------------------//
	
	//初期化
	//引数：なし
	//返値：void
	void Initialize();

	//モデルをロード
	//引数：std::string(ファイル名)
	//返値：int(そのモデルデータに割り当てられた番号)
	int Load(std::string fileName);

	//描画
	//引数：int(描画したいモデルの番号)
	//引数：float(uvの縮尺)
	//返値：void
	void Draw(int handle, float offset = 1.0f);

	//任意のモデルを開放
	//引数：int(開放したいモデルの番号)
	//返値：void
	void Release(int handle);

	//全てのモデルを解放
	//引数：なし
	//返値：void
	void AllRelease();

	//アニメーションのフレーム数をセット
	//引数：int(設定したいモデルの番号)
	//引数：int(開始フレーム)
	//引数：int(終了フレーム)
	//引数：float(アニメーション速度)
	//返値：void
	void SetAnimFrame(int handle, int startFrame, int endFrame, float animSpeed);

	//アニメーションスピードをセット
	//引数：int(モデルの番号)
	//引数：float(アニメーション速度)
	//返値：void
	void SetAnimSpeedFrame(int handle, float frame);

	//現在のアニメーションのフレームをセット
	//引数：int(モデルの番号)
	//引数：float(フレーム数)
	//返値：void
	void SetNowAnimFrame(int handle, float frame);

	//現在のアニメーションのフレームを取得
	//引数：int(モデルの番号)
	//返値：int(フレーム数)
	int GetNowAnimFrame(int handle);

	//任意のボーンの位置を取得
	//引数：int(調べたいモデルの番号)
	//引数：std::string(調べたいボーンの名前)
	//返値：XMVECTOR(ボーンの位置(ワールド座標))
	XMVECTOR GetBonePosition(int handle, std::string boneName);

	//ワールド行列を設定
	//引数：int(設定したいモデルの番号)
	//引数：Transform&(ワールド行列)
	//返値：void
	void SetTransform(int handle, Transform& transform);

	//ワールド行列の取得
	//引数：int(知りたいモデルの番号)
	//返値：XMMATRIX(ワールド行列)
	XMMATRIX GetMatrix(int handle);

	//レイキャスト(レイを飛ばして当たり判定)
	//引数：int(判定したいモデルの番号)
	//引数：RayCastData*(飛ばすレイのデータ)
	//返値：void
	void RayCast(int handle, RayCastData* data);

};