#pragma once

//インクルード
#include "Direct3D.h"

//------------------ 安全にメモリを開放するためのマクロ ------------------// 
#define SAFE_DELETE(p) {if ((p)!=nullptr) { delete (p); (p)=nullptr;}}				//変数の解放
#define SAFE_DELETE_ARRAY(p) {if ((p)!=nullptr) { delete[] (p); (p)=nullptr;}}		//配列の解放
#define SAFE_RELEASE(p) {if ((p)!=nullptr) { p->Release(); (p)=nullptr;}}			//Release関数を呼び出す
#define SAFE_DESTROY(p) {if ((p)!=nullptr) { p->Destroy(); (p)=nullptr;}}			//Destroy関数を呼び出す

//------------------ 正常かチェックするためのマクロ ------------------// 
#define CHECK_MESSAGE(hr, message) {if (hr != S_OK) { MessageBox(nullptr, message, "エラー", MB_OK); return E_FAIL;}}		//動作チェック、メッセージ付き
#define CHECK_RETURN(hr) {if (hr != S_OK) { return E_FAIL; }}																//動作チェック

//------------------ 変換系のマクロ ------------------// 
#define CONVERT_TO_SECOND(time) ((float)(time) / 60.0f)		//フレーム数を秒数に変換
#define CONVERT_TO_FRAME(time) ((float)(time) * 60.0f)		//秒数をフレーム数に変換