#pragma once

//インクルード
#include "GameObject.h"

//ゲームに登場するシーン
enum SCENE_ID
{
	SCENE_ID_GAME = 0			//ゲーム画面
};

//シーン切り替えを担当するオブジェクト
class SceneManager : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	SCENE_ID currentSceneID_;		//現在のシーン
	SCENE_ID nextSceneID_;			//次のシーン



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト(基本的にゲームマネージャー))
	//返値：なし
	SceneManager(GameObject* parent);

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//引数：SCENE_ID(次のシーンのID)
	//返値：void
	void ChangeScene(SCENE_ID next);
};