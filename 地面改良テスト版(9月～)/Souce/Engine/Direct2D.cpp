//インクルード
#include "Direct2D.h"

#include <wchar.h>
#include <Winuser.h>

#include "Direct3D.h"
#include "Text.h"
#include "Global.h"

//画面の描画に関する処理
namespace Direct2D
{
	//----------------------------- [ private ] -----------------------------//

	//------------------ 変数 ------------------//
	ID2D1Factory* pD2DFactory = nullptr;
	IDWriteFactory* pDWriteFactory = nullptr;
	ID2D1RenderTarget* pRT = nullptr;
	IDXGISurface* pDXGISurface = nullptr;

	UINT createDeviceFlags = 0;
}



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------//

//初期化
HRESULT Direct2D::Init(HWND hWnd)
{
	//状態チェック用
	HRESULT checkResult_ = S_OK;

	//ファクトリオブジェクト作成
	checkResult_ = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &pD2DFactory);
	CHECK_MESSAGE(checkResult_, "ファクトリの作成に失敗しました");

	//スワップチェインからバッファを受け取る
	checkResult_ = Direct3D::pSwapChain_->GetBuffer(0, IID_PPV_ARGS(&pDXGISurface));
	CHECK_MESSAGE(checkResult_, "バッファの受け取りにしました");

	FLOAT dpi;
	dpi = (FLOAT)GetDpiForWindow(hWnd);

	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpi, dpi);

	//レンダーターゲット作成
	checkResult_ = pD2DFactory->CreateDxgiSurfaceRenderTarget(pDXGISurface, &props, &pRT);
	CHECK_MESSAGE(checkResult_, "レンダーターゲットの作成に失敗しました");

	//2D書き込みのファクトリの作成
	checkResult_ = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown * *>(&pDWriteFactory));
	CHECK_MESSAGE(checkResult_, "2D書き込みのファクトリの作成に失敗しました");

	return S_OK;
}

//描画
void Direct2D::Draw()
{
	pRT->BeginDraw();

	//テキスト描画
	Text::Draw();
	
	pRT->EndDraw();
}

//解放
void Direct2D::Release()
{
	Text::Release();
	SAFE_RELEASE(pD2DFactory);
	SAFE_RELEASE(pDWriteFactory);
	SAFE_RELEASE(pRT);
	SAFE_RELEASE(pDXGISurface);
}

//2D表示もできるフラグ
UINT Direct2D::Get2dFlags()
{
	//下記を追加
	createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;//DirectX11上でDirect2Dを使用するために必要	//この値をCreateDeviceの第4引数に入れなければ、　Direct2Dの描画、　レンダーターゲットビューを取得できない//逆に言えば、　これを第4引数に入れるだけで、　Direct2Dによって、　書き込まれる準備が整う
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	return createDeviceFlags;
}
