#pragma once

//インクルード
#include "Direct2D.h"

//文字表示する名前空間
namespace Text
{
	//----------------------------- [ public ] -----------------------------//

	//------------------ 構造体 ------------------//

	//座標
	struct Point
	{
		float x_;		//x座標
		float y_;		//y座標
	};


	//------------------ 関数 ------------------//

	//テキストデータをセット
	//引数：string(表示する文字)
	//引数：string(フォントの種類)
	//引数：Point(位置座標)
	//引数：float(フォントサイズ)
	//引数：ColorF::Enum(色)
	//返値：int(識別番号)
	int SetText(std::string text, std::string fontType, Point point, float fontSize, D2D1::ColorF::Enum color);

	//文字変更
	//引数：int(識別番号)
	//引数：string(表示する文字)
	//返値：void
	void ChangeText(int handle, std::string text);

	//位置変更
	//引数：int(識別番号)
	//引数：Point(位置座標)
	//返値：void
	void ChangePoint(int handle, Point point);

	//色変更
	//引数：int(識別番号)
	//引数：ColorF::Enum(色)
	//返値：void
	void ChangeColor(int handle, D2D1::ColorF::Enum color);

	//書式変更
	//引数：int(識別番号)
	//引数：string(フォント)
	//引数：float(大きさ)
	//返値：void
	void ChangeFormat(int handle, std::string fontType, float fontSize);
	
	//rgb -> ColorF::Enum に変換
	//引数：UCHAR(赤)
	//引数：UCHAR(緑)
	//引数：UCHAR(青)
	//返値：ColorF::Enum(色)
	D2D1::ColorF::Enum ConvertColor(unsigned char red, unsigned char gleen, unsigned char blue);

	//描画許可
	//引数：int(識別番号)
	//返値：void
	void Visible(int handle);			

	//描画拒否
	//引数：int(識別番号)
	//返値：void
	void Invisible(int handle);

	//描画
	//引数：なし
	//返値：void
	void Draw();

	//解放
	//引数：なし
	//返値：void
	void Release();
}