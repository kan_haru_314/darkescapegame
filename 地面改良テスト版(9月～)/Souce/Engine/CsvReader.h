#pragma once

//インクルード
#include <vector>
#include <string>


//CSVファイルを扱うクラス
class CsvReader
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//

	//読み込んだデータを入れておく2次元配列
	std::vector<std::vector<std::string>> data_;


	//------------------ メソッド ------------------//

	//「,」か「改行」までの文字列を取得
	//引数：std::string(結果を入れるアドレス)
	//引数：std::string(もとの文字列データ)
	//引数：DWORD*(何文字目から調べるか)
	//返値：void
	void GetToComma(std::string* result, std::string data, DWORD* index);



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	CsvReader();

	//デストラクタ
	//引数：なし
	//返値：なし
	~CsvReader();

	//CSVファイルのロード
	//引数：std::string(ファイル名)
	//返値：bool(true = 成功, false = 失敗)
	bool Load(std::string fileName);

	//指定した位置のデータを文字列で取得
	//引数：DWORD(取得したい列)
	//引数：DWORD(取得したい行)
	//返値：std::string(取得した文字列)
	std::string GetString(DWORD x, DWORD y);

	//指定した位置のデータを整数で取得
	//引数：DWORD(取得したい列)
	//引数：DWORD(取得したい行)
	//返値：int(取得した値)
	int GetValue(DWORD x, DWORD y);

	//ファイルの列数を取得
	//引数：なし
	//返値：size_t(列数)
	size_t GetWidth();

	//ファイルの行数を取得
	//引数：なし
	//返値：size_t(行数)
	size_t GetHeight();
};

