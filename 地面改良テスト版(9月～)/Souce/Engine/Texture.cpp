//インクルード
#include "Texture.h"

#include "Direct3D.h"
#include "Global.h"

//コンストラクタ
Texture::Texture()
	: pSampleLinear_(nullptr)
	, pTextureSRV_(nullptr)
	, pFactory_(nullptr)
	, pDecoder_(nullptr)
	, pFrame_(nullptr)
	, pFormatConverter_(nullptr)
	, size_(XMVectorSet(0,0,0,0))
	, checkResult_(S_OK)
{
}

//デストラクタ
Texture::~Texture()
{
	SAFE_RELEASE(pSampleLinear_);
	SAFE_RELEASE(pTextureSRV_);

	SAFE_RELEASE(pFormatConverter_);
	SAFE_RELEASE(pFrame_);
	SAFE_RELEASE(pDecoder_);
	SAFE_RELEASE(pFactory_);
}

//ロード
HRESULT Texture::Load(std::string fileName)
{
	wchar_t wtext[FILENAME_MAX];
	size_t ret;
	mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());


	// テクスチャを読み込む
	checkResult_ = CoInitialize(NULL);
	//CHECK_MESSAGE(checkResult_ ,"COMの初期化に失敗しました");

	checkResult_ = CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&pFactory_));
	CHECK_MESSAGE(checkResult_ ,"COMのインスタンス作成に失敗しました");

	checkResult_ = pFactory_->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder_);
	CHECK_MESSAGE(checkResult_ ,"デコーダーの作成、ファイル読み込みに失敗しました");

	checkResult_ = pDecoder_->GetFrame(0, &pFrame_);
	CHECK_MESSAGE(checkResult_ ,"フレームの読み込みに失敗しました");

	checkResult_ = pFactory_->CreateFormatConverter(&pFormatConverter_);
	CHECK_MESSAGE(checkResult_ ,"フォーマットコンバーターの作成に失敗しました");

	checkResult_ = pFormatConverter_->Initialize(pFrame_, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut);
	CHECK_MESSAGE(checkResult_ ,"フォーマットコンバーターの初期化に失敗しました");

	UINT imgWidth;
	UINT imgHeight;
	checkResult_ = pFormatConverter_->GetSize(&imgWidth, &imgHeight);
	CHECK_MESSAGE(checkResult_ ,"サイズの取得に失敗しました");
	
	size_ = XMVectorSet((float)imgWidth, (float)imgHeight, 0, 0);

	// テクスチャの設定
	ID3D11Texture2D*	pTexture;			// テクスチャデータ
	D3D11_TEXTURE2D_DESC texdec;
	texdec.Width = imgWidth;
	texdec.Height = imgHeight;
	texdec.MipLevels = 1;
	texdec.ArraySize = 1;
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texdec.SampleDesc.Count = 1;
	texdec.SampleDesc.Quality = 0;
	texdec.Usage = D3D11_USAGE_DYNAMIC;
	texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texdec.MiscFlags = 0;
	checkResult_ = Direct3D::pDevice_->CreateTexture2D(&texdec, NULL, &pTexture);
	CHECK_MESSAGE(checkResult_ ,"2Dテクスチャの作成に失敗しました");

	if (pTexture)
	{
		// テクスチャを送る
		D3D11_MAPPED_SUBRESOURCE hMappedres;
		checkResult_ = Direct3D::pContext_->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
		CHECK_MESSAGE(checkResult_ ,"ビデオカードとの接続に失敗しました");

		checkResult_ = pFormatConverter_->CopyPixels(NULL, imgWidth * 4, imgWidth * imgHeight * 4, (BYTE*)hMappedres.pData);
		Direct3D::pContext_->Unmap(pTexture, 0);
	}
	CHECK_MESSAGE(checkResult_ ,"ピクセルのコピーに失敗しました");

	// シェーダリソースビュー(テクスチャ用)の設定
	D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
	srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srv.Texture2D.MipLevels = 1;
	checkResult_ = Direct3D::pDevice_->CreateShaderResourceView(pTexture, &srv, &pTextureSRV_);
	CHECK_MESSAGE(checkResult_ ,"シェーダーリソースビューの作成に失敗しました");

	// テクスチャー用サンプラー作成
	D3D11_SAMPLER_DESC  SamDesc;
	ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));
	SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	checkResult_ = Direct3D::pDevice_->CreateSamplerState(&SamDesc, &pSampleLinear_);
	CHECK_MESSAGE(checkResult_ ,"サンプラーの作成に失敗しました");

	SAFE_RELEASE(pFormatConverter_);
	SAFE_RELEASE(pFrame_);
	SAFE_RELEASE(pDecoder_);
	SAFE_RELEASE(pFactory_);

	return S_OK;
}
