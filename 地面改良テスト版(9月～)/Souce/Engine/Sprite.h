#pragma once

//インクルード
#include <d3d11.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>

#include "string"
#include "Texture.h"
#include "Transform.h"

//ネームスペース
using namespace DirectX;


//2D画像を表示するためのクラス
//とは言っても実態はポリゴンを平面的に表示しているだけ
class Sprite
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//
	
	//一つの頂点情報を格納する構造体
	struct VERTEX
	{
		XMVECTOR position_;		//位置座標
		XMVECTOR uv_;			//テクスチャ座標
	};

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	worldTrans_;		//頂点座標変換行列
		XMMATRIX	uvTrans_;			//テクスチャ座標変換行列
		XMFLOAT4	color_;				//テクスチャとの合成色
	};

	
	//------------------- フィールド -------------------//
	ID3D11Buffer *pVertexBuffer_;			//頂点バッファ
	ID3D11Buffer *pIndexBuffer_;			//インデックスバッファ
	ID3D11Buffer *pConstantBuffer_;			//定数バッファ
	Texture* pTexture_;						//テクスチャ
	HRESULT checkResult_;					//状態チェック用


	//------------------- メソッド -------------------//


	//頂点バッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitVertex();

	//インデックスバッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitIndex();

	//コンスタントバッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitConstantBuffer();



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------- メソッド -------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	Sprite();

	//デストラクタ
	//引数：なし
	//返値：なし
	~Sprite();

	//ロード
	//引数：std::string(画像ファイル名)
	//返値：HRESULT(状態)
	HRESULT Load(std::string fileName);

	//描画
	//引数：Transform&(変換行列(ワールド行列))
	//引数：RECT(画像の切り抜き範囲)
	//引数：float(アルファ値(不透明度))
	//返値：void
	void Draw(Transform& transform, RECT rect, float alpha);


	//画像サイズの取得
	//引数：なし
	//返値：XMVECTOR(画像サイズ)
	XMVECTOR GetTextureSize() {	return pTexture_->GetSize();}

};