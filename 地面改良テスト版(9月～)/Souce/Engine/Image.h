#pragma once

//インクルード
#include <vector>
#include <string>

#include "Sprite.h"
#include "Transform.h"


//2D画像を管理する
namespace Image
{
	//----------------------------- [ public ] -----------------------------//

	//------------------ 構造体 ------------------//

	//画像情報
	struct ImageData
	{
		//ファイル名
		std::string fileName_;

		//ロードした画像データのアドレス
		Sprite*		pSprite_;

		//切り抜き範囲
		RECT		rect_;

		//アルファ
		float		alpha_;

		//行列
		Transform transform_;

		//コンストラクタ
		ImageData()
			: fileName_("")
			, pSprite_(nullptr)
			, alpha_(1.0f)
		{
		}
	};


	//------------------ 関数 ------------------//
	
	//初期化
	//引数：なし
	//返値：void
	void Initialize();

	//画像をロード
	//引数：std::string(ファイル名)
	//返値：int(その画像データに割り当てられた番号)
	int Load(std::string fileName);

	//描画
	//引数：int(描画したい画像の番号)
	//返値：void
	void Draw(int handle);

	//任意の画像を開放
	//引数：int(開放したい画像の番号)
	//返値：void
	void Release(int handle);

	//全ての画像を解放
	//引数：なし
	//返値：void
	void AllRelease();

	//切り抜き範囲の設定
	//引数：int(設定したい画像の番号)
	//引数：int(切り抜きたい範囲の左端Ｘ座標)
	//引数：int(切り抜きたい範囲の上端Ｙ座標)
	//引数：int(切り抜きたい範囲の幅)
	//引数：int(切り抜きたい範囲の高さ)
	//返値：void
	void SetRect(int handle, int x, int y, int width, int height);

	//切り抜き範囲をリセット（画像全体を表示する）
	//引数：int(設定したい画像の番号)
	//返値：void
	void ResetRect(int handle);

	//アルファ値を指定
	//引数：int(設定したい画像の番号)
	//引数：int(アルファ値：0〜255)
	//返値：void
	void SetAlpha(int handle, int alpha);

	//ワールド行列を設定
	//引数：int(設定したい画像の番号)
	//引数：Transform&(ワールド行列)
	//返値：void
	void SetTransform(int handle, Transform& transform);

	//ワールド行列の取得
	//引数：int(知りたい画像の番号)
	//返値：XMMATRIX(ワールド行列)
	XMMATRIX GetMatrix(int handle);
}