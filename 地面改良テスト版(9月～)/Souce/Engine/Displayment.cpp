//インクルード
#include "Displayment.h"

#include <random>

#include "Direct3D.h"
#include "Camera.h"
#include "Light.h"
#include "Global.h"

#include "../GameObject/Map.h"

//定数宣言
#define MAX_BYTE 255		//1バイトの最大値
#define STD_SCALE 512.0f	//標準サイズ
#define HEIGHT_POS 32.0f	//高さ

//コンストラクタ
Displayment::Displayment(Map* pMap)
	: checkResult_(S_OK)
	, pMap_(pMap)
	, pFbxManager_(nullptr)
	, pFbxScene_(nullptr)
	, pVertexBuffer_(nullptr)
	, pConstantBuffer_(nullptr)
	, pIndexBuffer_(nullptr)
	, pSampleLinear_(nullptr)
	, pHeightSRV_(nullptr)
	, pNormalSRV_(nullptr)
	, vertexCount_(0)
	, polygonCount_(0)
{
	ZeroMemory(heightTexture_, sizeof(HEIGHT_TEX) * TEX_SIZE * TEX_SIZE);
	ZeroMemory(normalTexture_, sizeof(NORMAL_TEX) * TEX_SIZE * TEX_SIZE);

	for (int i = 0; i < TEX_FILE_COUNT; i++)
	{
		pRateSRV_[i] = nullptr;
	}

	diffHeight_ = (float)GetPrivateProfileInt("MAP", "Diff", 1, ".\\GameSetting.ini");
	textureNumber_ = (float)GetPrivateProfileInt("MAP", "Tex", 0, ".\\GameSetting.ini");

	//種まき
	srand(time(NULL));
	radNum1_ = (float)rand() / (float)rand() + 10.0f;
	radNum2_ = (float)rand() / (float)rand() + 10.0f;
	radNum3_ = (float)rand() / (float)rand() + 10.0f;
}

//デストラクタ
Displayment::~Displayment()
{
	//テクスチャ解放
	for (COLOR_TEX colorTex : colorTexture_)
	{
		SAFE_DELETE(colorTex.pDispTexture_);
		SAFE_DELETE(colorTex.pDiffTexture_);
		SAFE_DELETE(colorTex.pNorTexture_);
	}
	SAFE_RELEASE(pNormalSRV_);
	SAFE_RELEASE(pHeightSRV_);
	for (int i = 0; i < TEX_FILE_COUNT; i++)
	{
		SAFE_RELEASE(pRateSRV_[i]);
	}
	SAFE_RELEASE(pSampleLinear_);

	//バッファ解放
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pVertexBuffer_);

	//FBXデータ破棄
	SAFE_DESTROY(pFbxScene_);
	SAFE_DESTROY(pFbxManager_);
}

//値を範囲内にとどめる（※公式リファレンスから抜粋）
template<class T>
inline constexpr const T& Displayment::Clamp(const T& value, const T& low, const T& high)
{
	if (high < value)
	{
		return high;
	}

	if (value < low)
	{
		return low;
	}

	return value;
}

//少数切り捨ての整数に変換
XMVECTOR Displayment::Floor(XMVECTOR value)
{
	value.vecX = floor(value.vecX);
	value.vecY = floor(value.vecY);
	value.vecZ = floor(value.vecZ);
	value.vecW = floor(value.vecW);

	return value;
}

//少数切り捨ての整数に変換
float Displayment::Floor(float value)
{
	value = floor(value);

	return value;
}

//小数点以下の数字に変換
XMVECTOR Displayment::Frac(XMVECTOR value)
{
	return value - Floor(value);
}

//小数点以下の数字に変換
float Displayment::Frac(float value)
{
	return value - floor(value);
}

//指定した要素数に同じ値を格納
XMVECTOR Displayment::SetMultiVector(float value, int level)
{
	XMVECTOR vec = XMVectorZero();
	for (int i = 0; i < level; i++)
	{
		vec.m128_f32[i] = value;
	}

	return vec;
}

//線形補間
float Displayment::Lerp(float x, float y, float a)
{
	return x * (1.0f - a) + y * a;
}

//平面ポリゴンを作成
HRESULT Displayment::Create()
{
	//数値計算
	vertexCount_ = MODEL_SIZE * MODEL_SIZE;						//頂点数
	polygonCount_ = (MODEL_SIZE - 1) * (MODEL_SIZE - 1) * 2;	//ポリゴン数

	//頂点バッファ準備
	checkResult_ = InitVertex();
	CHECK_RETURN(checkResult_);

	//インデックスバッファ準備
	checkResult_ = InitIndex();
	CHECK_RETURN(checkResult_);

	//テクスチャ準備
	checkResult_ = InitTexture();
	CHECK_RETURN(checkResult_);

	//コンスタントバッファ準備
	checkResult_ = InitConstantBuffer();
	CHECK_RETURN(checkResult_);

	return S_OK;
}

//頂点バッファ準備
HRESULT Displayment::InitVertex()
{
	vertexCount_ = MODEL_SIZE * MODEL_SIZE;

	VERTEX *pVertexData = new VERTEX[vertexCount_];

	for (int v = 0; v < MODEL_SIZE; v++)
	{
		for (int u = 0; u < MODEL_SIZE; u++)
		{
			float posX = float(u) / float(MODEL_SIZE - 1);
			float posZ = float(v) / float(MODEL_SIZE - 1);
			pVertexData[u + v * MODEL_SIZE].position_ = XMVectorSet(posX - 0.5f, 0.0f, posZ - 0.5f, 0.0f);
			pVertexData[u + v * MODEL_SIZE].uv_ = XMVectorSet(1.0f - posX, posZ, 0.0f, 0.0f);
		}
	}


	//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DYNAMIC;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertexData;
	checkResult_ = Direct3D::pDevice_->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);

	//解放
	SAFE_DELETE_ARRAY(pVertexData);

	//状態チェックメッセージ
	CHECK_MESSAGE(checkResult_, "頂点バッファの作成に失敗しました");

	return S_OK;
}

//インデックスバッファ準備
HRESULT Displayment::InitIndex()
{
	//インデックス情報
	DWORD *pIndex = new DWORD[polygonCount_ * 3];
	ZeroMemory(pIndex, polygonCount_ * 3);
	int indexCount = 0;

	for (int v = 0; v < MODEL_SIZE - 1; v++)
	{
		for (int u = 0; u < MODEL_SIZE - 1; u++)
		{
			//左上
			pIndex[indexCount++] = u + v * MODEL_SIZE;					//左上
			pIndex[indexCount++] = (u + 1) + v * MODEL_SIZE;			//右上
			pIndex[indexCount++] = u + (v + 1) * MODEL_SIZE;			//左下

			//右下
			pIndex[indexCount++] = (u + 1) + v * MODEL_SIZE;			//右上
			pIndex[indexCount++] = (u + 1) + (v + 1) * MODEL_SIZE;		//右下
			pIndex[indexCount++] = u + (v + 1) * MODEL_SIZE;			//左下
		}
	}

	//インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * polygonCount_ * 3;
	bd.BindFlags = D3D10_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	checkResult_ = Direct3D::pDevice_->CreateBuffer(&bd, &InitData, &pIndexBuffer_);

	//解放
	SAFE_DELETE_ARRAY(pIndex);

	//状態チェックメッセージ
	CHECK_MESSAGE(checkResult_, "インデックスバッファの作成に失敗しました");

	return S_OK;
}

//テーブルあり乱数を取得
float Displayment::Random(XMVECTOR st) {
	st.vecZ = 0;
	st.vecW = 0;
	return Frac(sin(XMVector2Dot(st,
		XMVectorSet(radNum1_, radNum2_, 0, 0)).vecX) *
		radNum3_);
}

//ノイズ生成
float Displayment::Noise(XMVECTOR st) {
	XMVECTOR i = Floor(st);
	XMVECTOR f = Frac(st);

	float a = Random(i);
	float b = Random(i + XMVectorSet(1.0f, 0.0f, 0, 0));
	float c = Random(i + XMVectorSet(0.0f, 1.0f, 0, 0));
	float d = Random(i + XMVectorSet(1.0f, 1.0f, 0, 0));

	XMVECTOR u = f * f * (SetMultiVector(3.0f, 2) - 2.0f * f);

	return Lerp(a, b, u.vecX) +
		(c - a) * u.vecY * (1.0f - u.vecX) +
		(d - b) * u.vecX * u.vecY;
}

//高さ計算
float Displayment::CalcHeight(XMVECTOR uv)
{
	float value = 0.0f;
	float amplitude = 0.5f;
	float frequency = 3.0f;

	for (int i = 0; i < 4; i++) {
		value += amplitude * Noise(frequency * uv);
		uv *= 2.0f;
		amplitude *= 0.5f;
	}

	return value;
}

//テクスチャ準備
HRESULT Displayment::InitTexture()
{
	

	//カラー用テクスチャ
	{
		for (int i = 0; i < TEX_FILE_COUNT; i++)
		{
			const std::string textureNum = std::to_string(i + 1);

			colorTexture_[i].pDiffTexture_ = new Texture;
			colorTexture_[i].pNorTexture_ = new Texture;
			colorTexture_[i].pDispTexture_ = new Texture;

			colorTexture_[i].pDiffTexture_->Load("MapTexture/" + textureNum + "/Diffuse.png");
			CHECK_MESSAGE(checkResult_, "Diffuse用テクスチャの作成に失敗しました");

			colorTexture_[i].pNorTexture_->Load("MapTexture/" + textureNum + "/Normal.png");
			CHECK_MESSAGE(checkResult_, "Normal用テクスチャの作成に失敗しました");

			colorTexture_[i].pDispTexture_->Load("MapTexture/" + textureNum + "/Displayment.png");
			CHECK_MESSAGE(checkResult_, "Displayment用テクスチャの作成に失敗しました");
		}
	}

	//色割合テクスチャ
	{
		for (int i = 0; i < TEX_FILE_COUNT; i++)
		{
			RATE_TEX rateTexture[TEX_SIZE][TEX_SIZE];

			//データ格納
			for (int v = 0; v < TEX_SIZE; v++)
			{
				for (int u = 0; u < TEX_SIZE; u++)
				{
					if (i == textureNumber_)
					{
						rateTexture[v][u].rate_ = 255;
					}
					else
					{
						rateTexture[v][u].rate_ = 0;
					}
				}
			}

			//テクスチャの設定
			ID3D11Texture2D* pTexture;			// テクスチャデータ
			D3D11_TEXTURE2D_DESC texdec;
			texdec.Width = TEX_SIZE;
			texdec.Height = TEX_SIZE;
			texdec.MipLevels = 1;
			texdec.ArraySize = 1;
			texdec.Format = DXGI_FORMAT_R8_UNORM;
			texdec.SampleDesc.Count = 1;
			texdec.SampleDesc.Quality = 0;
			texdec.Usage = D3D11_USAGE_DYNAMIC;
			texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			texdec.MiscFlags = 0;
			checkResult_ = Direct3D::pDevice_->CreateTexture2D(&texdec, NULL, &pTexture);
			CHECK_MESSAGE(checkResult_, "テクスチャの作成に失敗しました");

			if (pTexture)
			{
				//テクスチャを送る
				D3D11_MAPPED_SUBRESOURCE hMappedres;
				checkResult_ = Direct3D::pContext_->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
				CHECK_MESSAGE(checkResult_, "ビデオカードとの接続に失敗しました");

				memcpy(hMappedres.pData, rateTexture, sizeof(RATE_TEX) * TEX_SIZE * TEX_SIZE);
				Direct3D::pContext_->Unmap(pTexture, 0);

				//シェーダリソースビュー(テクスチャ用)の設定
				D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
				srv.Format = DXGI_FORMAT_R8_UNORM;
				srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				srv.Texture2D.MipLevels = 1;
				checkResult_ = Direct3D::pDevice_->CreateShaderResourceView(pTexture, &srv, &pRateSRV_[i]);
				CHECK_MESSAGE(checkResult_, "色割合用テクスチャのSRV作成に失敗しました");
			}
		}
	}

	//高さ用テクスチャ
	{
		//データ格納
		for (int v = 0; v < TEX_SIZE; v++)
		{
			for (int u = 0; u < TEX_SIZE; u++)
			{
				XMVECTOR uv = XMVectorSet((float)u, (float)v, 0.0f, 0.0f) / XMVectorSet(TEX_SIZE, TEX_SIZE, 1.0f, 1.0f);

				heightTexture_[v][u].height_ = (BYTE)(CalcHeight(uv * diffHeight_) * MAX_BYTE);
			}
		}

		//テクスチャの設定
		ID3D11Texture2D*	pTexture;			// テクスチャデータ
		D3D11_TEXTURE2D_DESC texdec;
		texdec.Width = TEX_SIZE;
		texdec.Height = TEX_SIZE;
		texdec.MipLevels = 1;
		texdec.ArraySize = 1;
		texdec.Format = DXGI_FORMAT_R8_UNORM;
		texdec.SampleDesc.Count = 1;
		texdec.SampleDesc.Quality = 0;
		texdec.Usage = D3D11_USAGE_DYNAMIC;
		texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		texdec.MiscFlags = 0;
		checkResult_ = Direct3D::pDevice_->CreateTexture2D(&texdec, NULL, &pTexture);
		CHECK_MESSAGE(checkResult_ ,"テクスチャの作成に失敗しました");

		if (pTexture)
		{
			//テクスチャを送る
			D3D11_MAPPED_SUBRESOURCE hMappedres;
			checkResult_ = Direct3D::pContext_->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
			CHECK_MESSAGE(checkResult_ ,"ビデオカードとの接続に失敗しました");

			memcpy(hMappedres.pData, heightTexture_, sizeof(HEIGHT_TEX) * TEX_SIZE * TEX_SIZE);
			Direct3D::pContext_->Unmap(pTexture, 0);

			//シェーダリソースビュー(テクスチャ用)の設定
			D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
			srv.Format = DXGI_FORMAT_R8_UNORM;
			srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			srv.Texture2D.MipLevels = 1;
			checkResult_ = Direct3D::pDevice_->CreateShaderResourceView(pTexture, &srv, &pHeightSRV_);
			CHECK_MESSAGE(checkResult_ ,"高さ用テクスチャのSRV作成に失敗しました");
		}
	}

	//法線用テクスチャ
	{
		//データ格納
		for (int v = 0; v < TEX_SIZE; v++)
		{
			for (int u = 0; u < TEX_SIZE; u++)
			{
				//高さの差を計算
				float diffX = (float)(heightTexture_[v][Clamp(u + 1, 0, TEX_SIZE - 1)].height_ - heightTexture_[v][Clamp(u - 1, 0, TEX_SIZE - 1)].height_);
				float diffY = (float)(heightTexture_[Clamp(v + 1, 0, TEX_SIZE - 1)][u].height_ - heightTexture_[Clamp(v - 1, 0, TEX_SIZE - 1)][u].height_);

				//差をベクトルに
				XMVECTOR deltaX = XMVectorSet(1, 0, diffX / 2.0f, 0);
				XMVECTOR deltaY = XMVectorSet(0, 1, diffY / 2.0f, 0);

				//外積
				XMVECTOR nor = XMVector3Cross(deltaY, deltaX);
				nor.vecX *= -1;
				nor = (XMVector3Normalize(nor) + SetMultiVector(1.0f, 3)) / 2.0f;

				//値代入
				normalTexture_[v][u].normalX_ = (BYTE)(nor.vecX * MAX_BYTE);
				normalTexture_[v][u].normalY_ = (BYTE)(nor.vecZ * MAX_BYTE);
				normalTexture_[v][u].normalZ_ = (BYTE)(nor.vecY * MAX_BYTE);
				normalTexture_[v][u].normalW_ = MAX_BYTE;
			}
		}

		//テクスチャの設定
		ID3D11Texture2D*	pTexture;			// テクスチャデータ
		D3D11_TEXTURE2D_DESC texdec;
		texdec.Width = TEX_SIZE;
		texdec.Height = TEX_SIZE;
		texdec.MipLevels = 1;
		texdec.ArraySize = 1;
		texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texdec.SampleDesc.Count = 1;
		texdec.SampleDesc.Quality = 0;
		texdec.Usage = D3D11_USAGE_DYNAMIC;
		texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		texdec.MiscFlags = 0;
		checkResult_ = Direct3D::pDevice_->CreateTexture2D(&texdec, NULL, &pTexture);
		CHECK_MESSAGE(checkResult_ ,"テクスチャの作成に失敗しました");

		if (pTexture)
		{
			//テクスチャを送る
			D3D11_MAPPED_SUBRESOURCE hMappedres;
			checkResult_ = Direct3D::pContext_->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
			CHECK_MESSAGE(checkResult_ ,"ビデオカードとの接続に失敗しました");

			memcpy(hMappedres.pData, normalTexture_, sizeof(NORMAL_TEX)* TEX_SIZE* TEX_SIZE);
			Direct3D::pContext_->Unmap(pTexture, 0);

			//シェーダリソースビュー(テクスチャ用)の設定
			D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
			srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			srv.Texture2D.MipLevels = 1;
			checkResult_ = Direct3D::pDevice_->CreateShaderResourceView(pTexture, &srv, &pNormalSRV_);
			CHECK_MESSAGE(checkResult_ ,"法線用テクスチャのSRV作成に失敗しました");
		}
	}

	//テクスチャー用サンプラー作成
	D3D11_SAMPLER_DESC  SamDesc;
	ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));
	SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	checkResult_ = Direct3D::pDevice_->CreateSamplerState(&SamDesc, &pSampleLinear_);
	CHECK_MESSAGE(checkResult_ ,"サンプラーの作成に失敗しました");

	return S_OK;
}

//コンスタントバッファ準備
HRESULT Displayment::InitConstantBuffer()
{
	//必要な設定項目
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//定数バッファの作成
	checkResult_ = Direct3D::pDevice_->CreateBuffer(&cb, NULL, &pConstantBuffer_);

	//状態チェックメッセージ
	CHECK_MESSAGE(checkResult_, "コンスタントバッファの作成に失敗しました");

	return checkResult_;
}

//描画
void Displayment::Draw(Transform& transform)
{
	//使用するシェーダーをセット
	Direct3D::SetShader(Direct3D::SHADER_DISPLAY);

	//zバッファ書き込みON
	Direct3D::SetDepthBafferWriteEnable(true);

	//パラメータの受け渡し
	D3D11_MAPPED_SUBRESOURCE pdata;
	CONSTANT_BUFFER cb;

	//コンスタントバッファにデータ格納
	cb.worldVewProj_ = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());
	cb.normalTrans_ = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));
	cb.worldTrans_ = XMMatrixTranspose(transform.GetWorldMatrix());
	cb.cameraPos_ = Camera::GetConvertPosition();
	cb.lightPos_ = Light::GetConvertPosition();
	cb.maxDistance_ = 128.0f;
	cb.minDistance_ = 64.0f;
	cb.bright_ = Light::GetBright();
	cb.sampleOffset_ = (pMap_->GetMapScale() / STD_SCALE) * 16.0f;
	cb.height_ = HEIGHT_POS;
	cb.maxDevide_ = 64;

	//データ投げ
	Direct3D::pContext_->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	//GPUからのリソースアクセスを一時止める
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));					//リソースへ値を送る
	Direct3D::pContext_->Unmap(pConstantBuffer_, 0);									//GPUからのリソースアクセスを再開

	//サンプラー
	ID3D11SamplerState*	pSampler = colorTexture_[0].pDiffTexture_->GetSampler();
	Direct3D::pContext_->DSSetSamplers(0, 1, &pSampleLinear_);		//ドメインシェーダー
	Direct3D::pContext_->DSSetSamplers(1, 1, &pSampler);			//ドメインシェーダー
	Direct3D::pContext_->PSSetSamplers(0, 1, &pSampleLinear_);		//ピクセルシェーダー
	Direct3D::pContext_->PSSetSamplers(1, 1, &pSampler);			//ピクセルシェーダー

	//シェーダーリソースビュー
	ID3D11ShaderResourceView* pSRV;
	Direct3D::pContext_->DSSetShaderResources(0, 1, &pHeightSRV_);
	Direct3D::pContext_->DSSetShaderResources(1, 1, &pNormalSRV_);
	
	for (int i = 0; i < TEX_FILE_COUNT; i++)
	{
		//割合テクスチャーをシェーダーに渡す
		Direct3D::pContext_->DSSetShaderResources(i + 2, 1, &pRateSRV_[i]);
		Direct3D::pContext_->PSSetShaderResources(i + 2, 1, &pRateSRV_[i]);

		//テクスチャーをドメインシェーダーに渡す
		pSRV = colorTexture_[i].pDispTexture_->GetSRV();
		Direct3D::pContext_->DSSetShaderResources(i * 3 + 7, 1, &pSRV);

		//テクスチャーをピクセルシェーダーに渡す
		pSRV = colorTexture_[i].pDiffTexture_->GetSRV();
		Direct3D::pContext_->PSSetShaderResources(i * 3 + 8, 1, &pSRV);
		pSRV = colorTexture_[i].pNorTexture_->GetSRV();
		Direct3D::pContext_->PSSetShaderResources(i * 3 + 9, 1, &pSRV);
	}

	//このコンスタントバッファーを、どのシェーダーで使うかを指定
	Direct3D::pContext_->VSSetConstantBuffers(0, 1, &pConstantBuffer_);		//バーテックスシェーダー
	Direct3D::pContext_->HSSetConstantBuffers(0, 1, &pConstantBuffer_);		//ハルシェーダー
	Direct3D::pContext_->DSSetConstantBuffers(0, 1, &pConstantBuffer_);		//ドメインシェーダー
	Direct3D::pContext_->PSSetConstantBuffers(0, 1, &pConstantBuffer_);		//ピクセルシェーダー

	//バーテックスバッファーをセット
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	Direct3D::pContext_->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	//インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext_->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//プリミティブ・トポロジーをセット
	Direct3D::pContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);

	//ポリゴンメッシュを描画する
	Direct3D::pContext_->DrawIndexed(polygonCount_ * 3, 0, 0);

	//元に戻す
	Direct3D::SetShader(Direct3D::SHADER_3D);
}

//指定位置の高さを取得
float Displayment::GetPositionHeight(XMVECTOR pos, XMVECTOR scale)
{
	//誤差の修正
	const float ERROR_CORRECTION = 1.002f;
	scale.vecX /= ERROR_CORRECTION;
	scale.vecZ /= ERROR_CORRECTION;

	//-0.5 〜 0.5に変換
	XMVECTOR bytePos = pos / scale.vecX;

	//配列と座標ではx軸が逆なので反転
	bytePos.vecX *= -1.0f;

	//0 〜 1に変換
	bytePos += XMVectorSet(0.5f, 0.5f, 0.5f, 0.0f);

	//0 〜 255に変換
	bytePos *= (TEX_SIZE - 1);

	//ポリゴンの頂点位置
	XMVECTOR point[3];
	point[0] = XMVectorSet(Floor(bytePos.vecX), 0.0f, Floor(bytePos.vecZ), 0.0f);
	point[2] = XMVectorSet(Floor(bytePos.vecX) + 1.0f, 0.0f, Floor(bytePos.vecZ) + 1.0f, 0.0f);

	//下にあるポリゴンが右上か左下か
	if (Frac(bytePos.vecX) - Frac(bytePos.vecZ) > 0)
	{
		//左下
		point[1] = XMVectorSet(Floor(bytePos.vecX) + 1.0f, 0.0f, Floor(bytePos.vecZ), 0.0f);
	}
	else
	{
		//右上
		point[1] = XMVectorSet(Floor(bytePos.vecX), 0.0f, Floor(bytePos.vecZ) + 1.0f, 0.0f);
	}
	
	for (int i = 0; i < 3; i++)
	{
		//高さの倍率を求める
		point[i].vecY = (float)(heightTexture_[(int)(point[i].vecZ)][(int)(point[i].vecX)].height_) / (float)(MAX_BYTE);

		//こちらも倍率にしておく(-0.5 〜 0.5)
		point[i].vecX = (point[i].vecX / (float)(TEX_SIZE - 1) - 0.5f) * -1.0f;
		point[i].vecZ = (point[i].vecZ / (float)(TEX_SIZE - 1) - 0.5f);

		//大きさを実寸大に変換
		point[i] *= scale;
		point[i].vecY *= HEIGHT_POS;
	}

	//レイを飛ばす準備
	float dist = 0.0f;												//高さ
	XMVECTOR underDir = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//上に向かうディレクトリ
	pos.vecY = 0.0f;												//レイのスタートを高さ0に

	//レイ
	Direct3D::Intersect(pos, underDir, point[0], point[1], point[2], &dist);

	return dist;
}
