#pragma once

//インクルード
#include "GameObject.h"


//カメラ
namespace Camera
{
	//----------------------------- [ public ] -----------------------------//
	 
	//------------------ 関数 ------------------// 
	
	//初期化（プロジェクション行列作成）
	//引数：なし
	//返値：void
	void Initialize();

	//更新（ビュー行列作成）
	//引数：なし
	//返値：void
	void Update();

	//視点（カメラの位置）を設定
	//引数：XMVECTOR(カメラの座標)
	//返値：void
	void SetPosition(XMVECTOR position);

	//焦点（見る位置）を設定
	//引数：XMVECTOR(カメラの見る座標)
	//返値：void
	void SetTarget(XMVECTOR target);

	//位置を取得
	XMVECTOR GetPosition();

	//位置をXMFLOAT4で取得
	//引数：なし
	//返値：XMFLOAT4(位置)
	XMFLOAT4 GetConvertPosition();

	//焦点を取得
	//引数：なし
	//返値：XMVECTOR（焦点）
	XMVECTOR GetTarget();

	//ビュー行列を取得
	//引数：なし
	//返値：XMMATRIX(ビュー行列)
	XMMATRIX GetViewMatrix();

	//プロジェクション行列を取得
	//引数：なし
	//返値：XMMATRIX(プロジェクション行列)
	XMMATRIX GetProjectionMatrix();
};