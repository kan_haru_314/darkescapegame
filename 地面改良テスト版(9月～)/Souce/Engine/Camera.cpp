//インクルード
#include "camera.h"

#include "Direct3D.h"

//カメラ
namespace Camera
{
	//----------------------------- [ private ] -----------------------------//

	//------------------ 変数 ------------------// 
	XMVECTOR position_;		//カメラの位置
	XMVECTOR target_;		//カメラの焦点
	XMMATRIX view_;			//ビュー行列
	XMMATRIX proj_;			//プロジェクション行列
};



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------// 
	
//初期化(プロジェクション行列作成)
void Camera::Initialize()
{
	position_ = XMVectorSet(0, 3, -10, 0);	//カメラの位置
	target_ = XMVectorSet(0, 0, 0, 0);		//カメラの焦点

	//プロジェクション行列
	proj_ = XMMatrixPerspectiveFovLH(XM_PIDIV4, (FLOAT)Direct3D::screenWidth_ / (FLOAT)Direct3D::screenHeight_, 0.1f, 4096.0f);
}

//更新(ビュー行列作成)
void Camera::Update()
{
	//ビュー行列
	view_ = XMMatrixLookAtLH(XMVectorSet(position_.vecX, position_.vecY, position_.vecZ, 0),
		XMVectorSet(target_.vecX, target_.vecY, target_.vecZ, 0), XMVectorSet(0, 1, 0, 0));
}

//焦点を設定
void Camera::SetTarget(XMVECTOR target) { target_ = target; }

//位置を設定
void Camera::SetPosition(XMVECTOR position) { position_ = position; }

//焦点を取得
XMVECTOR Camera::GetTarget() { return target_; }

//位置を取得
XMVECTOR Camera::GetPosition() { return position_; }

//位置をXMFLOAT4で取得
XMFLOAT4 Camera::GetConvertPosition()
{
	return XMFLOAT4(position_.m128_f32);
}

//ビュー行列を取得
XMMATRIX Camera::GetViewMatrix() { return view_; }

//プロジェクション行列を取得
XMMATRIX Camera::GetProjectionMatrix() { return proj_; }