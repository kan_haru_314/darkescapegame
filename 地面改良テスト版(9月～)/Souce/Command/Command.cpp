//インクルード
#include "Command.h"

#include "../Engine/Global.h"

#include "../GameObject/Charactor.h"

//コンストラクタ
Command::Command()
	: exeTime_(0)
	, exeMaxTime_(0)
{
}

//デストラクタ
Command::~Command()
{
}

//動作時間を超えたか
bool Command::IsExecute()
{
	if (exeTime_ > 0.0f)
	{
		return true;
	}

	return false;
}

//時間処理
void Command::ExeTime()
{
	if (!IsExecute())
	{
		exeTime_ = exeMaxTime_;
	}

	//時間計測
	exeTime_ -= CONVERT_TO_SECOND(1.0f);
}

//処理時間の設定
void Command::SetMaxTime(float time)
{
	exeMaxTime_ = time;
}
