#pragma once

//インクルード
#include <list>
#include <vector>

//プロトタイプ宣言
class Command;
class Charactor;

//コマンド番号
enum CommandNumber
{
	WAIT,			//待機
	WALK,			//歩く
	RUN				//走る
};


//コマンドを管理するクラス
class Invoker
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	std::list<Command*> cmdQueue_;			//コマンドキュー
	std::vector<Command*> cmdList_;			//コマンドリスト
	Charactor* chara_;						//所属するオブジェクト
	bool stackMode_;						//キューのコマンド数を複数か一つか



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：Charactor*(キャラオブジェクト)
	//引数：bool(true = コマンド1つまで, false = コマンド複数可)
	//返値：なし
	Invoker(Charactor* chara, bool mode = false);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Invoker();

	//コマンド設定
	//引数：CommandNumber(コマンド名)
	//返値：void
	void SetCmmand(CommandNumber cmdNum);

	//コマンド実行
	//引数：なし
	//返値：void
	void Execute();
};