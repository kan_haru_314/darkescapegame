#pragma once

//インクルード
#include "Command.h"

//歩行コマンド
class WalkCommand : public Command
{
public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//
	
	//コンストラクタ
	//引数：なし
	//返値：なし
	WalkCommand();

	//デストラクタ
	//引数：なし
	//返値：なし
	~WalkCommand();

	//コマンド動作
	//引数：Charactor*(キャラオブジェクト)
	//返値：void
	void Execute(Charactor* chara) override;
};