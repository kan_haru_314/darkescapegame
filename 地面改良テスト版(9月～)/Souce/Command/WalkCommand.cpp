//インクルード
#include "WalkCommand.h"
#include "../GameObject/Charactor.h"

//コンストラクタ
WalkCommand::WalkCommand()
{
	//時間設定
	exeMaxTime_ = 0.0f;
	exeTime_ = 0.0f;
}

//デストラクタ
WalkCommand::~WalkCommand()
{
}

//コマンド動作
void WalkCommand::Execute(Charactor* chara)
{
	//実行
	chara->Walk();
}