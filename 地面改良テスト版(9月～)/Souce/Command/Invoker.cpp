//インクルード
#include "Invoker.h"

#include "../Engine/GameObject.h"

#include "Command.h"
#include "WaitCommand.h"
#include "WalkCommand.h"
#include "RunCommand.h"

//コンストラクタ
Invoker::Invoker(Charactor* chara, bool mode)
{
	chara_ = chara;
	stackMode_ = mode;

	//使用するコマンドを登録
	cmdList_.push_back(new WaitCommand);		//待機
	cmdList_.push_back(new WalkCommand);		//歩き
	cmdList_.push_back(new RunCommand);			//走り
}

//デストラクタ
Invoker::~Invoker()
{
	//コマンドリストの開放
	for (Command* cmd : cmdList_)
	{
		delete cmd;
	}

	//一応クリア
	cmdQueue_.clear();
}

//コマンド設定
void Invoker::SetCmmand(CommandNumber cmdNum)
{
	//名前に対応したコマンドを設定
	if (stackMode_)
	{
		//コマンド一つのみ
		if (cmdQueue_.empty())
		{
			cmdQueue_.push_back(cmdList_[cmdNum]);
		}
	}
	else
	{
		//コマンド複数可
		cmdQueue_.push_back(cmdList_[cmdNum]);
	}
}

//コマンド実行
void Invoker::Execute()
{
	//コマンドあり
	if (!cmdQueue_.empty()) {

		//キューの先頭を実行
		Command* cmd = cmdQueue_.front();
		cmd->Execute(chara_);

		//時間計算
		cmd->ExeTime();

		//実行時間は過ぎたか
		if (!cmd->IsExecute())
		{
			//コマンド削除
			cmdQueue_.pop_front();
		}
	}
}