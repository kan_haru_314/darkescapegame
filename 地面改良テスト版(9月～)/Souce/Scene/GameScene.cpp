//インクルード
#include "GameScene.h"

#include <random>
#include <time.h>

#include "../Engine/Text.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Global.h"

#include "../GameObject/Map.h"
#include "../GameObject/Player.h"

//コンストラクタ
GameScene::GameScene(GameObject * parent)
	: GameObject(parent, "GameScene")
{
}

//初期化
void GameScene::Initialize()
{
	//乱数の種セット
	srand((UINT)time(NULL));

	//マップ作成
	Instantiate<Map>(this);

	//プレイヤー作成
	Instantiate<Player>(this);
}

//更新
void GameScene::Update()
{
}

//描画
void GameScene::Draw()
{
}

//開放
void GameScene::Release()
{
}