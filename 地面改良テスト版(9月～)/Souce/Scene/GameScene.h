#pragma once

//インクルード
#include "../Engine/GameObject.h"

//ゲームシーンを管理するクラス
class GameScene : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//


	//------------------ メソッド ------------------//


public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	GameScene(GameObject* parent);

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//ゲームオーバー処理
	//引数：なし
	//返値：void
	void GameOver();
};