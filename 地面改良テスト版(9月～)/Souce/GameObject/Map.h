#pragma once

//インクルード
#include "../Engine/GameObject.h"

//プロトタイプ宣言
class Displayment;


//マップを管理するクラス
class Map : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const float MAP_RANGE;	//マップの使用許可範囲
	float mapScale_;		//マップの大きさ
	Displayment* map_;		//マップデータ



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	Map(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Map();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//指定位置の高さを取得
	//引数：XMVECTOR(位置)
	//返値：float(高さ)
	float GetPositionHeight(XMVECTOR pos);

	//マップの大きさを取得
	//引数：なし
	//返値：float(大きさ)
	float GetMapScale();

	//マップの使用許可範囲を取得
	//引数：なし
	//返値：float(範囲)
	float GetMapRange();
};