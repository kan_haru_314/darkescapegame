#pragma once

//インクルード
#include "../Engine/GameObject.h"

#include "../Command/Invoker.h"


//キャラクターを管理するクラス
class Charactor : public GameObject
{
protected:
	//----------------------------- [ protected ] -----------------------------//

	//------------------ フィールド ------------------//
	int hModel_;			//モデル番号
	Invoker* inv_;			//コマンド管理
	XMVECTOR moveDir_;		//移動方向



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//引数：std::string(オブジェクト名)
	//返値：なし
	Charactor(GameObject* parent, std::string name);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Charactor();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//モデル番号を取得
	//引数：なし
	//返値：int(モデル番号)
	int GetModelHandle();


	//------------------ コマンド動作 ------------------//

	//待機
	//引数：なし
	//返値：void
	virtual void Wait() = 0;

	//歩く
	//引数：なし
	//返値：void
	virtual void Walk() = 0;

	//走る
	//引数：なし
	//返値：void
	virtual void Run() = 0;
};