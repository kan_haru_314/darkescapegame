#pragma once

//インクルード
#include "../Engine/GameObject.h"
#include "Charactor.h"

//プロトタイプ宣言
class Map;


//プレイヤーを管理するクラス
class Player : public Charactor
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//

	//移動の情報
	struct MoveState
	{
		float SPEED_;				//移動スピード
		int animeStart_;			//アニメーションの開始フレーム
		int animeRoopStart_;		//ループアニメーションの開始フレーム
		int animeEnd_;				//アニメーションの終了フレーム
		float animeCount_;			//アニメーションのカウント
	};


	//------------------ フィールド ------------------//
	const float CAMERA_LENGTH;		//カメラの離れる距離
	const MoveState WALK_STATE;		//歩きについての情報
	const MoveState RUN_STATE;		//走りについての情報
	const XMVECTOR DEPTH_VEC;		//奥に行くベクトル
	const XMVECTOR CAMERA_HEIGHT;	//カメラの高さ

	XMVECTOR camAngle_;				//カメラの角度
	Map* map_;						//マップのポインタ
	float dirAngle_;				//向いた方向の角度
	float camSensitivity_;			//カメラ感度(数値が低いほど、感度が高くなる)


	//------------------ メソッド ------------------//

	//通行止め
	//引数：なし
	//返値：bool(true = 通行止め, false = 通行許可)
	bool IsBackRange();



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	Player(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Player();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;


	//------------------ コマンド動作 ------------------//

	//待機
	//引数：なし
	//返値：void
	void Wait() override;

	//歩く
	//引数：なし
	//返値：void
	void Walk() override;

	//走る
	//引数：なし
	//返値：void
	void Run() override;
};