//コンスタントバッファ
cbuffer global
{
	float4x4	g_WVP;    // 変換行列
};

//頂点シェーダ
//引数1：pos(頂点位置)
float4 VS(float4 pos : POSITION) : SV_POSITION
{
	return mul(pos, g_WVP);
}

//ピクセルシェーダ
//引数1：pos(頂点情報)
float4 PS(float4 pos : SV_POSITION) : SV_Target
{
	return float4(1, 1, 1, 0.3);
}