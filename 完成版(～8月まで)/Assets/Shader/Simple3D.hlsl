//テクスチャー
Texture2D		g_Texture: register(t0);

//ノーマルマップテクスチャー
Texture2D		g_NormalTexture: register(t1);

//サンプラー
SamplerState	g_Sampler : register(s0);

//コンスタントバッファ
cbuffer global
{
	float4x4	g_WVP;				//座標変換の合成行列
	float4x4	g_NormalTrans;		//法線変換の合成行列
	float4x4	g_World;			//ワールド合成行列
	float4		g_EyePos;			//カメラの位置
	float4		g_LightPos;			//ライトの位置
	float4		g_Diffuse;			//ディフューズカラー（マテリアルの色）
	float4		g_Ambient;			//アンビエントカラー（影の色）
	float4		g_Speculer;			//スペキュラーカラー（ハイライトの色）
	float		g_Bright;			//明るさ
	float		g_Shuniness;		//ハイライトの強さ（テカリ具合）
	float		g_SampleOffset;		//サンプルレベルの倍率
	bool		g_IsTexture;		//テクスチャ貼ってあるかどうか
	bool		g_IsNormalTexture;	//ノーマルマップテクスチャ貼ってあるかどうか
};

//頂点シェーダの出力用構造体
struct VS_OUT
{
	float4 pos		: SV_POSITION;	//位置座標
	float4 posWorld : TEXCOORD0;	//ワールド座標の位置
	float4 normal	: TEXCOORD1;	//法線
	float4 eye		: TEXCOORD2;	//視線
	float2 uv		: TEXCOORD3;	//uv座標
};


//頂点シェーダ
//引数1：pos(頂点位置)
//引数2：normal(法線)
//引数3：uv(uv座標)
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD)
{
	//出力
	VS_OUT outData;

	//スクリーン座標に変換
	outData.pos = mul(pos, g_WVP);	

	//ワールド座標に変換
	outData.posWorld = mul(pos, g_World);

	//法線の変形
	normal.w = 0;
	normal = mul(normal, g_NormalTrans);
	outData.normal = normal;

	//視線ベクトル
	outData.eye = normalize(g_EyePos - outData.posWorld);

	//uz座標はそのまま渡す
	outData.uv = uv;

	return outData;
}


//ピクセルシェーダ
//引数1：input(頂点情報)
float4 PS(VS_OUT inData) : SV_Target
{
	//色
	float4 diffuse;

	//テクスチャ有無
	if (g_IsTexture == true)
	{
		//テクスチャの色
		diffuse = g_Texture.Sample(g_Sampler, inData.uv * g_SampleOffset);
	}
	else
	{
		//マテリアルの色
		diffuse = g_Diffuse;
	}


	//ノーマルマップ
	float4 normal;

	//テクスチャ有無
	if (g_IsNormalTexture == true)
	{
		//テクスチャの色
		normal = g_NormalTexture.Sample(g_Sampler, inData.uv * g_SampleOffset) * 2.0f - 1.0f;
		normal = normal.rbga;		//gleen, blue データ入れ替え
		normal.b *= -1;				//blueの反転
	}
	else
	{
		//なにもなし
		normal = float4(0.0f, 0.0f, 0.0f, 0.0f);
	}

	//ライトの向き
	float4 lightDir = inData.posWorld - g_LightPos;
	lightDir.w = 0.0f;

	//ライトとの距離
	float lightLength = length(lightDir) / 8.0f;

	//正規化
	lightDir = normalize(lightDir);							//ライトの向き
	inData.normal = normalize(inData.normal + normal);		//法線

	//ポイントライトの照らす範囲
	float bright = 1.0f - g_Bright;
	float lightBright = saturate(1.0f / (bright + bright * lightLength + bright * lightLength * lightLength));

	//影
	float4 shade;

	//テクスチャ有無
	if (g_IsNormalTexture == true)
	{
		shade = saturate(dot(inData.normal, -lightDir)) * lightBright * 2.0f;
	}
	else
	{
		shade = saturate(dot(inData.normal, -lightDir)) * lightBright + lightBright / 1.2f;
	}

	shade.a = 1.0f;

	//環境光（アンビエント）
	float4 ambient = g_Ambient;
	ambient.a = 1.0f;

	//鏡面反射光（スペキュラー）
	float4 speculer = float4(0, 0, 0, 0);

	//ハイライトの有無
	if (g_Speculer.a > 0)
	{
		float4 ref = reflect(lightDir, inData.normal);											//正反射ベクトル
		speculer = pow(saturate(dot(ref, inData.eye)), g_Shuniness) * g_Speculer * g_Bright;	//ハイライトを求める
	}

	//最終的な色
	return diffuse * shade + diffuse * ambient + speculer;
}