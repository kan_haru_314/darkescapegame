//テクスチャー
Texture2D		g_Texture: register(t0);

//サンプラー
SamplerState	g_Sampler : register(s0);

//コンスタントバッファ
cbuffer global
{
	float4x4 g_World;				//ワールド合成行列
	float4x4 g_TextureTrans;		//テクスチャ座標変換行列
	float4 g_Color;					//テクスチャ合成色
};

//頂点シェーダの出力用構造体
struct VS_OUTPUT
{
	float4 pos	: SV_POSITION;		//位置座標
	float2 uv	: TEXCOORD0;		//uv座標
};


//頂点シェーダ
//引数1：pos(頂点位置)
//引数2：uv(uv座標)
VS_OUTPUT VS(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	//出力
	VS_OUTPUT output;

	//ワールド座標に変換
	output.pos = mul(pos, g_World);

	//テクスチャーに合わせる
	output.uv = mul(uv, g_TextureTrans);
	
	return output;
}


//ピクセルシェーダ
//引数1：input(頂点情報)
float4 PS(VS_OUTPUT input) : SV_Target
{
	return g_Texture.Sample(g_Sampler, input.uv) * g_Color;
}