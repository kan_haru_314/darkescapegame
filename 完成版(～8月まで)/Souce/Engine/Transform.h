#pragma once

//インクルード
#include <DirectXMath.h>

//定数宣言
#define vecX  m128_f32[0]		//Xベクトル
#define vecY  m128_f32[1]		//Yベクトル
#define vecZ  m128_f32[2]		//Zベクトル
#define vecW  m128_f32[3]		//Wベクトル
#define XMVectorOne g_XMOne

//ネームスペース
using namespace DirectX;


//位置、向き、拡大率などを管理するクラス
class Transform
{
public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ フィールド ------------------//

	XMMATRIX matTranslate_;		//移動行列
	XMMATRIX matRotate_;		//回転行列	
	XMMATRIX matScale_;			//拡大行列
	XMVECTOR position_;			//位置
	XMVECTOR rotate_;			//向き
	XMVECTOR scale_;			//拡大率
	Transform* pParent_;		//親オブジェクトの情報

	//コンストラクタ
	//引数：なし
	//返値：なし
	Transform();

	//デストラクタ
	//引数：なし
	//返値：なし
	~Transform();

	//各行列の計算
	//引数：なし
	//返値：void
	void Calclation();

	//ワールド行列を取得
	//引数：bool(true = ビルボード, false = 普通に描画)
	//返値：XMMATRIX(その時点でのワールド行列)
	XMMATRIX GetWorldMatrix(bool isBillBoard = false);
};

