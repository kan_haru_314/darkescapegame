//インクルード
#include "Fbx.h"

#include "Direct3D.h"
#include "FbxParts.h"
#include "Global.h"

//コンストラクタ
Fbx::Fbx()
	: pFbxManager_(nullptr)
	, pFbxScene_(nullptr)
	, frameRate_(FbxTime::eDefaultMode)
	, animSpeed_(0.0f)
	, startFrame_(0)
	, endFrame_(0)
	, checkResult_(S_OK)
{
	parts_.clear();
}

//デストラクタ
Fbx::~Fbx()
{
	for (FbxParts* parts : parts_)
	{
		SAFE_DELETE(parts);
	}
	parts_.clear();

	SAFE_DESTROY(pFbxScene_);
	SAFE_DESTROY(pFbxManager_);
}

//fbxファイルをロード
HRESULT Fbx::Load(std::string fileName)
{
	// FBXの読み込み
	pFbxManager_ = FbxManager::Create();
	pFbxScene_ = FbxScene::Create(pFbxManager_, "fbxscene");
	FbxString FileName(fileName.c_str());
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager_, "imp");
	if (!fbxImporter->Initialize(FileName.Buffer(), -1, pFbxManager_->GetIOSettings()))
	{
		//失敗
		SAFE_DESTROY(fbxImporter);
		checkResult_ = E_FAIL;
		CHECK_RETURN(checkResult_);
	}
	fbxImporter->Import(pFbxScene_);
	SAFE_DESTROY(fbxImporter);

	//アニメーションのタイムモードの取得
	frameRate_ = pFbxScene_->GetGlobalSettings().GetTimeMode();

	//現在のカレントディレクトリを覚えておく
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをファイルがあった場所に変更
	char dir[MAX_PATH];
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);

	//ルートノードを取得して
	FbxNode* rootNode = pFbxScene_->GetRootNode();

	//子供の数を調べて
	int childCount = rootNode->GetChildCount();

	//1個ずつチェック
	for (int i = 0; childCount > i; i++)
	{
		checkResult_ = CheckNode(rootNode->GetChild(i), &parts_);
		CHECK_RETURN(checkResult_);
	}

	//カレントディレクトリを元の位置に戻す
	SetCurrentDirectory(defaultCurrentDir);

	return S_OK;
}

//ノードの中身を調べる
HRESULT Fbx::CheckNode(FbxNode * pNode, std::vector<FbxParts*>* pPartsList)
{
	//そのノードにはメッシュ情報が入っているだろうか？
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr != nullptr && attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//パーツを用意
		FbxParts* pParts = new FbxParts;
		checkResult_ = pParts->Init(pNode);
		CHECK_RETURN(checkResult_);

		//パーツ情報を動的配列に追加
		pPartsList->push_back(pParts);
	}



	//子ノードにもデータがあるかも！！
	{
		//子供の数を調べて
		int childCount = pNode->GetChildCount();

		//一人ずつチェック
		for (int i = 0; i < childCount; i++)
		{
			checkResult_ = CheckNode(pNode->GetChild(i), pPartsList);
			CHECK_RETURN(checkResult_);
		}
	}

	return S_OK;
}

//解放
void Fbx::Release()
{

}

//任意のボーンの位置を取得
XMVECTOR Fbx::GetBonePosition(std::string boneName)
{
	XMVECTOR position = XMVectorSet(0, 0, 0, 0);
	for (FbxParts* parts : parts_)
	{
		if (parts->GetBonePosition(boneName, &position))
			break;
	}

	return position;
}

//描画
void Fbx::Draw(Transform& transform, int frame, float offset)
{
	//パーツを1個ずつ描画
	for (FbxParts* parts : parts_)
	{
		//その瞬間の自分の姿勢行列を得る
		FbxTime     time;
		time.SetTime(0, 0, 0, frame, 0, 0, frameRate_);


		if (parts->GetSkinInfo() != nullptr)
		{
			//------------ スキンアニメーション（ボーン有り） ------------//
			parts->DrawSkinAnime(transform, time, offset);
		}
		else
		{
			//------------------ メッシュアニメーション ------------------//
			parts->DrawMeshAnime(transform, time, pFbxScene_, offset);
		}
	}
}

//レイキャスト（レイを飛ばして当たり判定）
void Fbx::RayCast(RayCastData * data)
{
	//すべてのパーツと判定
	for (FbxParts* parts : parts_)
	{
		parts->RayCast(data);
	}
}
