//インクルード
#include "Input.h"

#include "Global.h"


//DirectInputを使ったキーボード入力処理
namespace Input
{
	//----------------------------- [ private ] -----------------------------//

	//------------------- 変数 -------------------//
	HWND hWnd_  = nullptr;						//ウィンドウハンドル
	LPDIRECTINPUT8 pDInput_ = nullptr;			//DirectInputオブジェクト
	HRESULT checkResult_ = S_OK;				//状態チェック用

	//------------------- キーボード -------------------//
	LPDIRECTINPUTDEVICE8	pKeyDevice_ = nullptr;		//デバイスオブジェクト
	BYTE keyState_[256];								//現在の各キーの状態
	BYTE prevKeyState_[256];							//前フレームでの各キーの状態

	//------------------- マウス -------------------//
	LPDIRECTINPUTDEVICE8	pMouseDevice_ = nullptr;	//デバイスオブジェクト
	DIMOUSESTATE mouseState_;							//マウスの状態
	DIMOUSESTATE prevMouseState_;						//前フレームのマウスの状態
	POINT mousePos_;									//マウスカーソルの位置

	//------------------- パッド -------------------//
	const int MAX_PAD_NUM = 4;							//パッドの最大接続台数
	XINPUT_STATE controllerState_[MAX_PAD_NUM];			//パッドの状態
	XINPUT_STATE prevControllerState_[MAX_PAD_NUM];		//前フレームのパッドの状態


	//------------------ 関数 ------------------//

	//アナログ入力をデジタルに変換
	//引数：int(入力値)
	//引数：int(最大値)
	//引数：int(デッドゾーン)
	//返値：float(変換結果)
	float GetAnalogValue(int raw, int max, int deadZone)
	{
		float result = (float)raw;

		if (result > 0)
		{
			//デッドゾーン
			if (result < deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result - deadZone) / (max - deadZone);
			}
		}

		else
		{
			//デッドゾーン
			if (result > -deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result + deadZone) / (max - deadZone);
			}
		}

		return result;
	}
}



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------//

//初期化
HRESULT Input::Initialize(HWND hWnd)
{
	//ウィンドウハンドル
	hWnd_ = hWnd;

	//DirectInput本体
	checkResult_ = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (VOID**)&pDInput_, nullptr);
	CHECK_MESSAGE(checkResult_ ,"入力デバイスのアクセスに失敗しました");


	/////////////////////////// キーボード ///////////////////////////
	checkResult_ = pDInput_->CreateDevice(GUID_SysKeyboard, &pKeyDevice_, nullptr);
	CHECK_MESSAGE(checkResult_ ,"デバイスをキーボードに設定できませんでした");
	
	checkResult_ = pKeyDevice_->SetDataFormat(&c_dfDIKeyboard);
	CHECK_MESSAGE(checkResult_ ,"キーボードのフォーマットに失敗しました");
	
	checkResult_ = pKeyDevice_->SetCooperativeLevel(NULL, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	//CHECK_MESSAGE(checkResult_ ,"キーボードの協調レベルの設定に失敗しました");


	/////////////////////////// マウス ///////////////////////////
	checkResult_ = pDInput_->CreateDevice(GUID_SysMouse, &pMouseDevice_, nullptr);
	CHECK_MESSAGE(checkResult_ ,"デバイスをマウスに設定できませんでした");
	
	checkResult_ = pMouseDevice_->SetDataFormat(&c_dfDIMouse);
	CHECK_MESSAGE(checkResult_ ,"マウスのフォーマットに失敗しました");
	
	checkResult_ = pMouseDevice_->SetCooperativeLevel(hWnd_, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	//CHECK_MESSAGE(checkResult_ ,"マウスの協調レベルの設定に失敗しました");

	return S_OK;
}

//更新
void Input::Update()
{
	/////////////////////////// キーボード ///////////////////////////
	pKeyDevice_->Acquire();
	memcpy(prevKeyState_, keyState_, sizeof(keyState_));
	pKeyDevice_->GetDeviceState(sizeof(keyState_), &keyState_);

	/////////////////////////// マウス ///////////////////////////
	pMouseDevice_->Acquire();
	memcpy(&prevMouseState_, &mouseState_, sizeof(mouseState_));
	pMouseDevice_->GetDeviceState(sizeof(mouseState_), &mouseState_);

	/////////////////////////// パッド ///////////////////////////
	for (int i = 0; i < MAX_PAD_NUM; i++)
	{
		memcpy(&prevControllerState_[i], &controllerState_[i], sizeof(controllerState_[i]));
		XInputGetState(i, &controllerState_[i]);
	}

}

//開放
void Input::Release()
{
	SAFE_RELEASE(pMouseDevice_);
	SAFE_RELEASE(pKeyDevice_);
	SAFE_RELEASE(pDInput_);
}


//------------------ キーボード ------------------//

//キーが押されているか調べる
bool Input::IsKey(int keyCode)
{
	//押してる
	if (keyState_[keyCode] & 0x80)
	{
		return true;
	}
	return false;
}

//キーを今押したか調べる（押しっぱなしは無効）
bool Input::IsKeyDown(int keyCode)
{
	//今は押してて、前回は押してない
	if (IsKey(keyCode) && !(prevKeyState_[keyCode] & 0x80))
	{
		return true;
	}
	return false;
}

//キーを今放したか調べる
bool Input::IsKeyUp(int keyCode)
{
	//今押してなくて、前回は押してる
	if (!IsKey(keyCode) && prevKeyState_[keyCode] & 0x80)
	{
		return true;
	}
	return false;
}


//------------------ マウス ------------------//

//マウスのボタンが押されているか調べる
bool Input::IsMouseButton(int buttonCode)
{
	//押してる
	if (mouseState_.rgbButtons[buttonCode] & 0x80)
	{
		return true;
	}
	return false;
}

//マウスのボタンを今押したか調べる（押しっぱなしは無効）
bool Input::IsMouseButtonDown(int buttonCode)
{
	//今は押してて、前回は押してない
	if (IsMouseButton(buttonCode) && !(prevMouseState_.rgbButtons[buttonCode] & 0x80))
	{
		return true;
	}
	return false;
}

//マウスのボタンを今放したか調べる
bool Input::IsMouseButtonUp(int buttonCode)
{
	//今押してなくて、前回は押してる
	if (!IsMouseButton(buttonCode) && prevMouseState_.rgbButtons[buttonCode] & 0x80)
	{
		return true;
	}
	return false;
}

//マウスカーソルの位置を取得
XMVECTOR Input::GetMousePosition()
{
	XMVECTOR result = XMVectorSet((float)mousePos_.x, (float)mousePos_.y, 0, 0);
	return result;
}

//マウスカーソルの位置をセット
void Input::SetMousePosition(int x, int y)
{
	mousePos_.x = x;
	mousePos_.y = y;
}


//そのフレームでのマウスの移動量を取得
XMVECTOR Input::GetMouseMove()
{
	XMVECTOR result = XMVectorSet((float)mouseState_.lX, (float)mouseState_.lY, (float)mouseState_.lZ, 0);
	return result;
}


//------------------ パッド ------------------//

//パッドのボタンが押されているか調べる
bool Input::IsPadButton(int buttonCode, int padID)
{
	if (controllerState_[padID].Gamepad.wButtons & buttonCode)
	{
		return true; //押してる
	}
	return false; //押してない
}

//パッドのボタンを今押したか調べる（押しっぱなしは無効）
bool Input::IsPadButtonDown(int buttonCode, int padID)
{
	//今は押してて、前回は押してない
	if (IsPadButton(buttonCode, padID) && !(prevControllerState_[padID].Gamepad.wButtons & buttonCode))
	{
		return true;
	}
	return false;
}

//パッドのボタンを今放したか調べる
bool Input::IsPadButtonUp(int buttonCode, int padID)
{
	//今押してなくて、前回は押してる
	if (!IsPadButton(buttonCode, padID) && prevControllerState_[padID].Gamepad.wButtons & buttonCode)
	{
		return true;
	}
	return false;
}

//左スティックの傾きを取得
XMVECTOR Input::GetPadStickL(int padID)
{
	float x = GetAnalogValue(controllerState_[padID].Gamepad.sThumbLX, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
	float y = GetAnalogValue(controllerState_[padID].Gamepad.sThumbLY, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
	return XMVectorSet(x, y, 0, 0);
}

//右スティックの傾きを取得
XMVECTOR Input::GetPadStickR(int padID)
{
	float x = GetAnalogValue(controllerState_[padID].Gamepad.sThumbRX, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
	float y = GetAnalogValue(controllerState_[padID].Gamepad.sThumbRY, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
	return XMVectorSet(x, y, 0, 0);
}

//左トリガーの押し込み具合を取得
float Input::GetPadTrrigerL(int padID)
{
	return GetAnalogValue(controllerState_[padID].Gamepad.bLeftTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
}

//右トリガーの押し込み具合を取得
float Input::GetPadTrrigerR(int padID)
{
	return GetAnalogValue(controllerState_[padID].Gamepad.bRightTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
}

//振動させる
void Input::SetPadVibration(int l, int r, int padID)
{
	XINPUT_VIBRATION vibration;
	ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
	vibration.wLeftMotorSpeed = l; // 左モーターの強さ
	vibration.wRightMotorSpeed = r;// 右モーターの強さ
	XInputSetState(padID, &vibration);
}