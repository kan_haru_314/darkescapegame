#pragma once

//インクルード
#include <fbxsdk.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <string>

#include "Texture.h"
#include "Transform.h"

//定数宣言
#define TEX_SIZE 256		//高低差テクスチャーのサイズ

//ネームスペース
using namespace DirectX;

//プロトタイプ宣言
class Map;


//平面を凸凹にするクラス
class Displayment
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//
	
	//一つの頂点情報を格納する構造体
	struct VERTEX
	{
		XMVECTOR position_;		//位置座標
		XMVECTOR uv_;			//テクスチャ座標
	};

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX worldVewProj_;		//ワールド、ビュー、プロジェクション行列の合成（頂点変換に使用）
		XMMATRIX normalTrans_;		//回転行列と拡大行列の逆行列を合成したもの（法線の変形に使用）
		XMMATRIX worldTrans_;		//ワールド行列の合成（頂点変換に使用）
		XMFLOAT4 cameraPos_;		//カメラの位置ベクトル
		XMFLOAT4 lightPos_;			//点光源の位置ベクトル
		FLOAT maxDistance_;			//距離の最大値
		FLOAT minDistance_;			//距離の最低値
		FLOAT bright_;				//ライトの明るさ
		FLOAT sampleOffset_;		//オフセット
		INT maxDevide_;				//最大分割数
	};

	//高さ用のピクセルデータ
	struct HEIGHT_TEX
	{
		BYTE height_;		//高さ
	};

	//法線用のピクセルデータ
	struct NORMAL_TEX
	{
		BYTE normalX_;		//red
		BYTE normalY_;		//green
		BYTE normalZ_;		//blue
		BYTE normalW_;		//alpha
	};


	//------------------- フィールド -------------------//
	HRESULT checkResult_;					//状態チェック用
	Map* pMap_;								//呼び出した本人クラス
	float diffHeight_;						//高低差(激しさ)


	//------------------- FBXデータ -------------------//
	FbxManager* pFbxManager_;		//FBXファイルを扱う機能の本体
	FbxScene*	pFbxScene_;			//FBXファイルのシーン（Mayaで作ったすべての物体）を扱う


	//------------------- バッファ -------------------//
	ID3D11Buffer* pVertexBuffer_;		//頂点バッファ
	ID3D11Buffer* pConstantBuffer_;		//定数バッファ
	ID3D11Buffer* pIndexBuffer_;		//インデックスバッファ


	//------------------- テクスチャ -------------------//
	HEIGHT_TEX heightTexture_[TEX_SIZE][TEX_SIZE];	//高さ用テクスチャ
	NORMAL_TEX normalTexture_[TEX_SIZE][TEX_SIZE];	//法線用テクスチャ

	ID3D11SamplerState*			pSampleLinear_;		//テクスチャサンプラー
	ID3D11ShaderResourceView*	pHeightSRV_;		//高さ用シェーダーリソースビュー
	ID3D11ShaderResourceView*	pNormalSRV_;		//法線用シェーダーリソースビュー

	Texture* pDiffTexture_;							//カラー用テクスチャ
	Texture* pBumpTexture_;							//バンプ用テクスチャ
	Texture* pDispTexture_;							//高さ用テクスチャ


	//------------------- データ数 -------------------//
	DWORD vertexCount_;				//頂点数
	DWORD polygonCount_;			//ポリゴン数
	DWORD indexCount_;				//インデックス数
	DWORD polygonVertexCount_;		//ポリゴン頂点インデックス数 


	//------------------- メソッド -------------------//

	//値を範囲内にとどめる（※公式リファレンスから抜粋）
	//引数：template&(操作する値)
	//引数：template&(最低値)
	//引数：template&(最大値)
	//返値：template&(範囲内の値)
	template <class T>
	constexpr const T& Clamp(const T& value, const T& low, const T& high);

	//少数切り捨ての整数に変換
	//引数：XMVECTOR(変換元)
	//返値：XMVECTOR(整数のみ)
	XMVECTOR Floor(XMVECTOR x);

	//少数切り捨ての整数に変換
	//引数：float(変換元)
	//返値：float(整数のみ)
	float Floor(float x);

	//小数点以下の数字に変換
	//引数：XMVECTOR(変換元)
	//返値：XMVECTOR(少数部のみ)
	XMVECTOR Frac(XMVECTOR x);

	//小数点以下の数字に変換
	//引数：float(変換元)
	//返値：float(少数部のみ)
	float Frac(float x);

	//指定した要素数に同じ値を格納
	//引数：float(格納する値)
	//引数：int(格納する要素数)
	//返値：XMVECTOR(格納したベクトル)
	XMVECTOR SetMultiVector(float x, int level);

	//線形補間
	//引数：float(初期値)
	//引数：float(最終値)
	//引数：float(割合)
	//返値：float(補間した値)
	float Lerp(float x, float y, float a);

	//fbxファイル準備
	//引数：std::string(fbxファイル名)
	//返値：HRESULT(状態)
	HRESULT InitFbx(std::string fileName);

	//頂点バッファ準備
	//引数：fbxsdk::FbxMesh*(fbxファイル名)
	//返値：HRESULT(状態)
	HRESULT InitVertex(fbxsdk::FbxMesh* mesh);

	//インデックスバッファ準備
	//引数：fbxsdk::FbxMesh*(fbxファイル名)
	//返値：HRESULT(状態)
	HRESULT InitIndex(fbxsdk::FbxMesh* mesh);

	//テーブルあり乱数を取得
	//引数：XMVECTOR(種)
	//返値：float(乱数 0 〜 1)
	float Random(XMVECTOR st);

	//ノイズ生成
	//引数：XMVECTOR(位置)
	//返値：float(ノイズ)
	float Noise(XMVECTOR st);

	//高さ計算
	//引数：XMVECTOR(位置)
	//返値：float(高さ)
	float CalcHeight(XMVECTOR uv);

	//テクスチャ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitTexture();

	//コンスタントバッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitConstantBuffer();



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------- メソッド -------------------//

	//コンストラクタ
	//引数：Map*(呼び出したクラス)
	//返値：なし
	Displayment(Map* pMap);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Displayment();

	//平面なfbxファイルをロード
	//引数：std::string(ファイル名)
	//返値：HRESULT(状態)
	HRESULT Load(std::string fileName);

	//描画
	//引数：Transform&(座標)
	//返値：void
	void Draw(Transform& transform);

	//指定位置の高さを取得
	//引数：XMVECTOR(位置)
	//引数：XMVECTOR(大きさ倍率)
	//返値：float(高さ)
	float GetPositionHeight(XMVECTOR pos, XMVECTOR scale);
};