#pragma once

//インクルード
#include "GameObject.h"

//ゲームに登場するシーン
enum SCENE_ID
{
	SCENE_ID_TITLE = 0,		//タイトル画面
	SCENE_ID_GAME,			//ゲーム画面
	SCENE_ID_END			//終了画面
};

//ゲーム状態
struct GameState
{
	bool isClear_;		//ゲームクリアか(true = クリア, false = ゲームオーバー)
	int second_;		//経過時間(s)
	int coinCount_;		//獲得コイン枚数
};


//シーン切り替えを担当するオブジェクト
class SceneManager : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	SCENE_ID currentSceneID_;		//現在のシーン
	SCENE_ID nextSceneID_;			//次のシーン
	GameState gameState_;			//ゲームの状態



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト(基本的にゲームマネージャー))
	//返値：なし
	SceneManager(GameObject* parent);

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//引数：SCENE_ID(次のシーンのID)
	//返値：void
	void ChangeScene(SCENE_ID next);

	//ゲームの状態を取得
	//引数：なし
	//返値：GameState(ゲームの状態)
	GameState GetGameState();

	//ゲームの状態をセット
	//引数：GameState(ゲームの状態)
	//返値：void
	void SetGameState(GameState state);
};