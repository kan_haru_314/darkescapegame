#pragma once

//インクルード
#include <d3d11.h>
#include <fbxsdk.h>
#include <vector>
#include <string>

#include "Transform.h"

//プロトタイプ宣言
class FbxParts;

//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR start;		//レイ発射位置
	XMVECTOR dir;		//レイの向きベクトル
	XMVECTOR normal;	//法線
	float dist;			//衝突点までの距離
	BOOL hit;			//レイが当たったか

	//初期化
	RayCastData()
		: start(XMVectorZero())
		, dir(XMVectorZero())
		, normal(XMVectorZero())
		, dist(99999.0f)
		, hit(false)
	{
	}
};


//FBXファイルを扱うクラス
class Fbx
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フレンドクラス ------------------//
	friend class FbxParts;


	//------------------ フィールド ------------------//
	 
	//モデルの各パーツ（複数あるかも）
	std::vector<FbxParts*> parts_;

	//FBXファイルを扱う機能の本体
	FbxManager* pFbxManager_;

	//FBXファイルのシーン（Mayaで作ったすべての物体）を扱う
	FbxScene* pFbxScene_;

	// アニメーションのフレームレート
	FbxTime::EMode frameRate_;

	//アニメーション速度
	float animSpeed_;

	//アニメーションの最初のフレーム
	int startFrame_;

	//アニメーションの最後のフレーム
	int endFrame_;

	//状態チェック用
	HRESULT checkResult_;					


	//------------------ メソッド ------------------//

	//ノードの中身を調べる
	//引数：FbxNode*(調べるノード)
	//引数：std::vector<FbxParts*>(パーツのリスト)
	//返値：HRESULT(状態)
	HRESULT CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList);



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	Fbx();

	//デストラクタ
	//引数：なし
	//返値：なし
	~Fbx();

	//fbxファイルをロード
	//引数：std::string(ファイル名)
	//返値：HRESULT(状態)
	virtual HRESULT Load(std::string fileName);

	//描画
	//引数：Transform&(ワールド行列)
	//引数：int(フレーム数)
	//引数：float(uvの縮尺)
	//返値：void
	void Draw(Transform& transform, int frame, float offset);

	//解放
	//引数：なし
	//返値：void
	void Release();

	//任意のボーンの位置を取得
	//引数：std::string(取得したいボーンの位置)
	//戻値：XMVECTOR(ボーンの位置)
	XMVECTOR GetBonePosition(std::string boneName);

	//レイキャスト（レイを飛ばして当たり判定）
	//引数：RayCastData*(レイのデータ)
	//返値：void
	void RayCast(RayCastData* data);

};

