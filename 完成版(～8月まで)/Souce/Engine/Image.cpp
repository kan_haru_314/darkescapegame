//インクルード
#include "Image.h"

#include "Global.h"


//3D画像を管理する
namespace Image
{
	//----------------------------- [ private ] -----------------------------//

	//------------------ 変数 ------------------//
	
	//ロード済みの画像データ一覧
	std::vector<ImageData*>	datas_;
}



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------//

//初期化
void Image::Initialize()
{
	AllRelease();
}

//画像をロード
int Image::Load(std::string fileName)
{
	ImageData* pData = new ImageData;

	//開いたファイル一覧から同じファイル名のものが無いか探す
	bool isExist = false;
	for (int i = 0; i < datas_.size(); i++)
	{
		//すでに開いている場合
		if (datas_[i] != nullptr && datas_[i]->fileName_ == fileName)
		{
			pData->pSprite_ = datas_[i]->pSprite_;
			isExist = true;
			break;
		}
	}

	//新たにファイルを開く
	if (isExist == false)
	{
		pData->pSprite_ = new Sprite;
		if (pData->pSprite_->Load(fileName) != S_OK)
		{
			//開けなかった
			SAFE_DELETE(pData->pSprite_);
			SAFE_DELETE(pData);
			return -1;
		}

		//無事開けた
		pData->fileName_ = fileName;
	}


	//使ってない番号が無いか探す
	for (int i = 0; i < datas_.size(); i++)
	{
		if (datas_[i] == nullptr)
		{
			datas_[i] = pData;
			return i;
		}
	}

	//新たに追加
	datas_.push_back(pData);

	//画像番号割り振り
	int handle = (int)datas_.size() - 1;

	//切り抜き範囲をリセット
	ResetRect(handle);

	return handle;
}



//描画
void Image::Draw(int handle)
{
	if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
	{
		return;
	}
	datas_[handle]->transform_.Calclation();
	datas_[handle]->pSprite_->Draw(datas_[handle]->transform_, datas_[handle]->rect_, datas_[handle]->alpha_);
}



//任意の画像を開放
void Image::Release(int handle)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return;
	}

	//同じモデルを他でも使っていないか
	bool isExist = false;
	for (int i = 0; i < datas_.size(); i++)
	{
		//すでに開いている場合
		if (datas_[i] != nullptr && i != handle && datas_[i]->pSprite_ == datas_[handle]->pSprite_)
		{
			isExist = true;
			break;
		}
	}

	//使ってなければモデル解放
	if (isExist == false)
	{
		SAFE_DELETE(datas_[handle]->pSprite_);
	}

	SAFE_DELETE(datas_[handle]);
}



//全ての画像を解放
void Image::AllRelease()
{
	for (int i = 0; i < datas_.size(); i++)
	{
		Release(i);
	}
	datas_.clear();
}


//切り抜き範囲の設定
void Image::SetRect(int handle, int x, int y, int width, int height)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return;
	}

	datas_[handle]->rect_.left = x;
	datas_[handle]->rect_.top = y;
	datas_[handle]->rect_.right = width;
	datas_[handle]->rect_.bottom = height;
}


//切り抜き範囲をリセット（画像全体を表示する）
void Image::ResetRect(int handle)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return;
	}

	XMVECTOR size = datas_[handle]->pSprite_->GetTextureSize();

	datas_[handle]->rect_.left = 0;
	datas_[handle]->rect_.top = 0;
	datas_[handle]->rect_.right = (long)size.vecX;
	datas_[handle]->rect_.bottom = (long)size.vecY;

}

//アルファ値設定
void Image::SetAlpha(int handle, int alpha)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return;
	}
	datas_[handle]->alpha_ = (float)alpha / 255.0f;
}


//ワールド行列を設定
void Image::SetTransform(int handle, Transform& transform)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return;
	}

	datas_[handle]->transform_ = transform;
}


//ワールド行列の取得
XMMATRIX Image::GetMatrix(int handle)
{
	if (handle < 0 || handle >= datas_.size())
	{
		return XMMatrixIdentity();
	}
	return datas_[handle]->transform_.GetWorldMatrix();
}