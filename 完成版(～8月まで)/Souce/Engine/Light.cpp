//インクルード
#include "Light.h"

#include <d3d11.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>

#include "Direct3D.h"
#include "Camera.h"
#include "Global.h"

//ネームスペース
using namespace DirectX;


//ライト
namespace Light
{
	//----------------------------- [ private ] -----------------------------//
	
	//------------------- 構造体 -------------------//

	//コンスタントバッファ
	struct CONSTANT_BUFFER		
	{
		XMMATRIX	worldVewProj;		//ワールド、ビュー、プロジェクション行列の合成（頂点変換に使用）
		FLOAT		bright;				//明るさ
	};


	//------------------ 変数 ------------------//
	Transform transform_;				//位置、向き、大きさ
	float bright_ = 0.5f;				//明るさ
	bool visible_ = false;				//描画するか(true = 描画する, false = 描画しない)
	HRESULT checkResult_  = S_OK;		//状態チェック用


	//------------------- バッファ -------------------//
	ID3D11Buffer* pVertexBuffer_ = nullptr;			//頂点バッファ
	ID3D11Buffer* pConstantBuffer_ = nullptr;		//定数バッファ
	ID3D11Buffer* pIndexBuffer_ = nullptr;			//インデックスバッファ


	//------------------ 関数 ------------------//

	//頂点バッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitVertex()
	{
		// 頂点データ宣言
		XMVECTOR vertices[] =
		{
			XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f), 
			XMVectorSet( 1.0f,  1.0f, 0.0f, 0.0f),
			XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f),
			XMVectorSet( 1.0f, -1.0f, 0.0f, 0.0f)
		};


		// 頂点データ用バッファの設定
		D3D11_BUFFER_DESC bd_vertex;
		bd_vertex.ByteWidth = sizeof(vertices);
		bd_vertex.Usage = D3D11_USAGE_DEFAULT;
		bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd_vertex.CPUAccessFlags = 0;
		bd_vertex.MiscFlags = 0;
		bd_vertex.StructureByteStride = 0;
		D3D11_SUBRESOURCE_DATA data_vertex;
		data_vertex.pSysMem = vertices;
		checkResult_ = Direct3D::pDevice_->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
		CHECK_MESSAGE(checkResult_, "頂点バッファの作成に失敗しました");

		return S_OK;
	}

	//インデックスバッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitIndex()
	{
		int index[] = { 2,1,0, 2,3,1 };

		// インデックスバッファを生成する
		D3D11_BUFFER_DESC   bd;
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(index);
		bd.BindFlags = D3D10_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = index;
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;
		checkResult_ = Direct3D::pDevice_->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
		CHECK_MESSAGE(checkResult_, "インデックスバッファの作成に失敗しました");

		return S_OK;
	}

	//コンスタントバッファ準備
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT InitConstantBuffer()
	{
		//必要な設定項目
		D3D11_BUFFER_DESC cb;
		cb.ByteWidth = sizeof(CONSTANT_BUFFER);
		cb.Usage = D3D11_USAGE_DYNAMIC;
		cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		cb.MiscFlags = 0;
		cb.StructureByteStride = 0;

		// 定数バッファの作成
		checkResult_ = Direct3D::pDevice_->CreateBuffer(&cb, NULL, &pConstantBuffer_);
		CHECK_MESSAGE(checkResult_, "コンスタントバッファの作成に失敗しました");

		return S_OK;
	}
}



//----------------------------- [ public ] -----------------------------//
 

//------------------ 関数 ------------------//

//初期化
HRESULT Light::Initialize()
{
	//頂点情報準備
	checkResult_ = InitVertex();
	CHECK_RETURN(checkResult_);

	//インデックス情報準備
	checkResult_ = InitIndex();
	CHECK_RETURN(checkResult_);

	//コンスタントバッファ準備
	checkResult_ = InitConstantBuffer();
	CHECK_RETURN(checkResult_);

	return S_OK;
}

//描画
void Light::Draw()
{
	if (visible_)
	{
		//いろいろ設定
		Direct3D::SetShader(Direct3D::SHADER_LIGHT);
		UINT stride = sizeof(XMVECTOR);
		UINT offset = 0;
		Direct3D::pContext_->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);
		Direct3D::pContext_->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		Direct3D::pContext_->PSSetConstantBuffers(0, 1, &pConstantBuffer_);
		Direct3D::SetDepthBafferWriteEnable(true);


		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext_->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

		// パラメータの受け渡し
		D3D11_MAPPED_SUBRESOURCE pdata;
		CONSTANT_BUFFER cb;
		cb.worldVewProj = XMMatrixTranspose(transform_.GetWorldMatrix(true) * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());
		cb.bright = bright_;

		Direct3D::pContext_->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのリソースアクセスを一時止める
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));					// リソースへ値を送る

		Direct3D::pContext_->Unmap(pConstantBuffer_, 0);									// GPUからのリソースアクセスを再開

		//ポリゴンメッシュを描画する
		Direct3D::pContext_->DrawIndexed(6, 0, 0);

		Direct3D::SetShader(Direct3D::SHADER_3D);

		Direct3D::SetDepthBafferWriteEnable(true);
	}
}

//解放
void Light::Release()
{
	//バッファ解放
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

//Drawを許可
void Light::Visible()
{
	visible_ = true;
}

//Drawを拒否
void Light::Invisible()
{
	visible_ = false;
}

//位置を設定
void Light::SetPosition(XMVECTOR position) 
{
	transform_.position_ = position; 
}

//大きさを設定
void Light::SetScale(XMVECTOR scale) 
{ 
	transform_.scale_ = scale;
}

//明るさを設定
void Light::SetBright(float bright) 
{ 
	bright_ = bright;
}

//位置を取得
XMVECTOR Light::GetPosition()
{ 
	return transform_.position_;
}

//位置をXMFLOAT4で取得
XMFLOAT4 Light::GetConvertPosition()
{
	return XMFLOAT4(transform_.position_.m128_f32);
}

//大きさを取得
XMVECTOR Light::GetScale()
{
	return transform_.scale_; 
}

//明るさを取得
float Light::GetBright()
{
	return bright_; 
}