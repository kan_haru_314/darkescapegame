//インクルード
#include "SceneManager.h"
#include "Global.h"
#include "Model.h"
#include "Image.h"
#include "Text.h"
//#include "Audio.h"

//使用するシーン
#include "../Scene/TitleScene.h"
#include "../Scene/GameScene.h"
#include "../Scene/EndScene.h"


//コンストラクタ
SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager")
	, currentSceneID_(SCENE_ID_TITLE)
	, nextSceneID_(SCENE_ID_TITLE)
{
	gameState_.isClear_ = false;	//ゲームクリアか(true = クリア, false = ゲームオーバー)
	gameState_.second_ = 0;			//経過時間
	gameState_.coinCount_ = 0;		//獲得コイン枚数
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_TITLE;
	nextSceneID_ = currentSceneID_;
	Instantiate<TitleScene>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		//Audio::Release();
		Model::AllRelease();
		Image::AllRelease();
		Text::Release();

		//次のシーンを作成
		switch (nextSceneID_)
		{
		case SCENE_ID_TITLE: Instantiate<TitleScene>(this); break;
		case SCENE_ID_GAME: Instantiate<GameScene>(this); break;
		case SCENE_ID_END: Instantiate<EndScene>(this); break;

		}
		//Audio::Initialize();
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//解放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}

//ゲームの状態を取得
GameState SceneManager::GetGameState()
{
	return gameState_;
}

//ゲームの状態をセット
void SceneManager::SetGameState(GameState state)
{
	gameState_ = state;
}
