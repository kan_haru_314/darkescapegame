//インクルード
#include "SphereCollider.h"

#include "BoxCollider.h"
#include "Model.h"

//コンストラクタ（当たり判定の作成）
SphereCollider::SphereCollider(XMVECTOR center, float radius)
{
	center_ = center;
	size_ = XMVectorSet(radius, radius, radius, 0);
	type_ = COLLIDER_CIRCLE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	hDebugModel_ = Model::Load("DebugCollision/sphereCollider.fbx");
#endif
}

//接触判定
bool SphereCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsCircle((BoxCollider*)target, this);
	else
		return IsHitCircleVsCircle((SphereCollider*)target, this);
}