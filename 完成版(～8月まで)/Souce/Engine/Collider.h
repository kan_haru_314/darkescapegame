#pragma once

//インクルード
#include <d3d11.h>
#include <DirectXMath.h>

//ネームスペース
using namespace DirectX;

//プロトタイプ宣言
class GameObject;
class BoxCollider;
class SphereCollider;

//あたり判定のタイプ
enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_CIRCLE		//球体
};


//あたり判定を管理するクラス
class Collider
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フレンドクラス ------------------//
	friend class BoxCollider;
	friend class SphereCollider;



protected:
	//----------------------------- [ protected ] -----------------------------//

	//------------------ フィールド ------------------//
	GameObject*		pGameObject_;	//この判定をつけたゲームオブジェクト
	ColliderType	type_;			//種類
	XMVECTOR		center_;		//中心位置（ゲームオブジェクトの原点から見た位置）
	XMVECTOR		size_;			//判定サイズ（幅、高さ、奥行き）
	int				hDebugModel_;	//デバッグ表示用のモデルのID



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	Collider();

	//デストラクタ
	//引数：なし
	//返値：なし
	virtual ~Collider();

	//接触判定（継承先のSphereColliderかBoxColliderでオーバーライド）
	//引数：Collider*(相手の当たり判定)
	//返値：bool(true = 接触, false = 未接触)
	virtual bool IsHit(Collider* target) = 0;

	//箱型同士の衝突判定
	//引数：BoxCollider*(1つ目の箱型判定)
	//引数：BoxCollider*(2つ目の箱型判定)
	//返値：bool(true = 接触, false = 未接触)
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//箱型と球体の衝突判定
	//引数：BoxCollider*(箱型判定)
	//引数：SphereCollider*(球体判定)
	//返値：bool(true = 接触, false = 未接触)
	bool IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere);

	//球体同士の衝突判定
	//引数：SphereCollider*(1つ目の球体判定)
	//引数：SphereCollider*(2つ目の球体判定)
	//返値：bool(true = 接触, false = 未接触)
	bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB);

	//テスト表示用の枠を描画
	//引数：XMVECTOR(オブジェクトの位置)
	//返値：void
	void Draw(XMVECTOR position);

	//ゲームオブジェクトをセット
	//引数：GameObject*(ゲームオブジェクト)
	//返値：void
	void SetGameObject(GameObject* gameObject) { pGameObject_ = gameObject; }

};

