//インクルード
#include "Direct3D.h"

#include <d3dcompiler.h>

#include "Direct2D.h"
#include "Transform.h"
#include "Global.h"

//画面の描画に関する処理
namespace Direct3D
{
	//----------------------------- [ private ] -----------------------------//

	//------------------ 変数 ------------------//
	ID3D11RenderTargetView* pRenderTargetView_ = nullptr;		//レンダーターゲットビュー
	ID3D11Texture2D* pDepthStencil_ = nullptr;					//デプスステンシル
	ID3D11DepthStencilView* pDepthStencilView_ = nullptr;		//デプスステンシルビュー
	ID3D11BlendState* pBlendState_ = nullptr;					//ブレンドステート

	ID3DBlob* pCompileVS_ = nullptr;		//コンパイル時の頂点シェーダー
	ID3DBlob* pCompilePS_ = nullptr;		//コンパイル時のピクセルシェーダー
	ID3DBlob* pCompileHS_ = nullptr;		//コンパイル時のハルシェーダー
	ID3DBlob* pCompileDS_ = nullptr;		//コンパイル時のドメインシェーダー

	HRESULT checkResult_ = S_OK;			//状態チェック用



	//----------------------------- [ public ] -----------------------------//

	//------------------ 変数 ------------------//
	ID3D11Device* pDevice_ = nullptr;						//描画を行うための環境やリソースの作成に使う
	ID3D11DeviceContext* pContext_ = nullptr;				//GPUに命令を出すためのもの
	IDXGISwapChain* pSwapChain_ = nullptr;					//スワップチェーン
	SHADER_BUNDLE shaderBundle_[SHADER_MAX] = { 0 };		//シェーダーのタイプ別機能
	int	screenWidth_ = 0;									//スクリーンの幅
	int	screenHeight_ = 0;									//スクリーンの高さ
	bool isDrawCollision_ = false;							//コリジョンを表示するか
}



//----------------------------- [ public ] -----------------------------//

//------------------ 関数 ------------------//

//初期化
HRESULT Direct3D::Initialize(HWND hWnd, int screenWidth, int screenHeight)
{
	///////////////// いろいろ準備するための設定 /////////////////
	
	//いろいろな設定項目をまとめた構造体
	DXGI_SWAP_CHAIN_DESC scDesc;

	//とりあえず全部0
	ZeroMemory(&scDesc, sizeof(scDesc));

	//描画先のフォーマット
	scDesc.BufferDesc.Width = screenWidth;					//画面幅
	scDesc.BufferDesc.Height = screenHeight;				//画面高さ
	scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 何色使えるか

	//FPS（1/60秒に1回）
	scDesc.BufferDesc.RefreshRate.Numerator = 60;		//分母
	scDesc.BufferDesc.RefreshRate.Denominator = 1;		//分子

	//その他
	scDesc.Windowed = TRUE;									//ウィンドウモードかフルスクリーンか
	scDesc.OutputWindow = hWnd;								//ウィンドウハンドル
	scDesc.BufferCount = 1;									//裏画面の枚数
	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//画面に描画するために使う
	scDesc.SampleDesc.Count = 1;							//MSAA（アンチエイリアス）の設定
	scDesc.SampleDesc.Quality = 0;							//　〃


	///////////////// 上記設定をもとにデバイス、コンテキスト、スワップチェインを作成 /////////////////

	D3D_FEATURE_LEVEL level;
	
	//デバイス、コンテキスト、スワップチェインの作成
	checkResult_ = D3D11CreateDeviceAndSwapChain(
		nullptr,					//どのビデオアダプタを使用するか？既定ならばnullptrで
		D3D_DRIVER_TYPE_HARDWARE,	//ドライバのタイプを渡す。これ以外は基本的にソフトウェア実装で、どうしてもという時やデバグ用に用いるべし.
		nullptr,					//上記をD3D_DRIVER_TYPE_SOFTWAREに設定した際に、その処理を行うDLLのハンドルを渡す。それ以外を指定している際には必ずnullptrを渡す.
		Direct2D::Get2dFlags(),		//何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
		nullptr,					//デバイス、コンテキストのレベルを設定。nullptrにしとけば可能な限り高いレベルにしてくれる
		0,							//上の引数でレベルを何個指定したか
		D3D11_SDK_VERSION,			//SDKのバージョン。必ずこの値
		&scDesc,					//上でいろいろ設定した構造体
		&pSwapChain_,				//無事完成したSwapChainのアドレスが返ってくる
		&pDevice_,					//無事完成したDeviceアドレスが返ってくる
		&level,						//無事完成したDevice、Contextのレベルが返ってくる
		&pContext_);				//無事完成したContextのアドレスが返ってくる

	//失敗したら終了
	CHECK_MESSAGE(checkResult_ ,"デバイス、コンテキスト、スワップチェインの作成に失敗しました");


	///////////////// 描画のための準備 /////////////////
	
	//スワップチェーンからバックバッファを取得（バックバッファ ＝ 裏画面 ＝ 描画先）
	ID3D11Texture2D* pBackBuffer;

	checkResult_ = pSwapChain_->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(LPVOID*)&pBackBuffer);

	//失敗したら終了
	CHECK_MESSAGE(checkResult_ ,"バックバッファの取得に失敗しました");


	//レンダーターゲットビューを作成
	checkResult_ = pDevice_->CreateRenderTargetView(
		pBackBuffer, 
		NULL, 
		&pRenderTargetView_);

	//一時的にバックバッファを取得しただけなので、解放
	SAFE_RELEASE(pBackBuffer);

	//失敗したら終了
	CHECK_MESSAGE(checkResult_ ,"レンダーターゲットビューの作成に失敗しました");


	///////////////// ビューポート（描画範囲）設定 /////////////////

	// ビューポートの設定
	D3D11_VIEWPORT vp;
	vp.Width = (float)screenWidth;			//幅
	vp.Height = (float)screenHeight;		//高さ
	vp.MinDepth = 0.0f;						//手前
	vp.MaxDepth = 1.0f;						//奥
	vp.TopLeftX = 0;						//左
	vp.TopLeftY = 0;						//上

	//各パターンのシェーダーセット準備
	checkResult_= InitShaderBundle();
	CHECK_RETURN(checkResult_);

	Direct3D::SetShader(Direct3D::SHADER_3D);

	//深度ステンシルビューの作成
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = screenWidth;
	descDepth.Height = screenHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	checkResult_ = pDevice_->CreateTexture2D(&descDepth, NULL, &pDepthStencil_);
	checkResult_ = pDevice_->CreateDepthStencilView(pDepthStencil_, NULL, &pDepthStencilView_);

	//ブレンドステート
	D3D11_BLEND_DESC BlendDesc;
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;
	BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	BlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	BlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	checkResult_ = pDevice_->CreateBlendState(&BlendDesc, &pBlendState_);
	float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
	pContext_->OMSetBlendState(pBlendState_, blendFactor, 0xffffffff);

	//パイプラインの構築
	pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView_);            // 描画先を設定（今後はレンダーターゲットビューを介して描画してね）
	pContext_->RSSetViewports(1, &vp);													  // ビューポートのセット


	//コリジョン表示するか
	isDrawCollision_ = GetPrivateProfileInt("DEBUG", "ViewCollider", 0, ".\\setup.ini") != 0;

	//引数を保持
	screenWidth_ = screenWidth;
	screenHeight_ = screenHeight;

	//2D描画の初期化
	checkResult_ = Direct2D::Init(hWnd);
	CHECK_RETURN(checkResult_);

	return S_OK;
}


//シェーダー準備
HRESULT Direct3D::InitShaderBundle()
{
	//ベクター型のバイト数
	DWORD vectorSize = sizeof(XMVECTOR);

	//SHADER_3D
	{
		//頂点シェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateVertexShader(pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_3D].pVertexShader_);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの作成に失敗しました");


		//ピクセルシェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreatePixelShader(pCompilePS_->GetBufferPointer(), pCompilePS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_3D].pPixelShader_);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの作成に失敗しました");


		//ハルシェーダーは不使用
		shaderBundle_[SHADER_3D].pHullShader_ = nullptr;

		//ドメインシェーダーは不使用
		shaderBundle_[SHADER_3D].pDomainShader_ = nullptr;

		//頂点インプットレイアウトの作成
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },	//頂点位置
			{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 2, D3D11_INPUT_PER_VERTEX_DATA, 0 },	//テクスチャ（UV）座標
		};
		checkResult_ = pDevice_->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), &shaderBundle_[SHADER_3D].pVertexLayout_);
		CHECK_MESSAGE(checkResult_ ,"頂点インプットレイアウトの作成に失敗しました");


		//シェーダーが無事作成できたので、コンパイルしたものは解放
		SAFE_RELEASE(pCompileVS_);
		SAFE_RELEASE(pCompilePS_);

		//ラスタライザ作成
		D3D11_RASTERIZER_DESC rdc = {};
		rdc.CullMode = D3D11_CULL_NONE;
		rdc.FillMode = D3D11_FILL_SOLID;
		rdc.FrontCounterClockwise = TRUE;
		checkResult_ = pDevice_->CreateRasterizerState(&rdc, &shaderBundle_[SHADER_3D].pRasterizerState_);
		CHECK_MESSAGE(checkResult_ ,"ラスタライザの作成に失敗しました");
	}


	//SHADER_2D
	{
		//頂点シェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/Simple2D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateVertexShader(pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_2D].pVertexShader_);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの作成に失敗しました");


		//ピクセルシェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreatePixelShader(pCompilePS_->GetBufferPointer(), pCompilePS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_2D].pPixelShader_);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの作成に失敗しました");


		//ハルシェーダーは不使用
		shaderBundle_[SHADER_2D].pHullShader_ = nullptr;

		//ドメインシェーダーは不使用
		shaderBundle_[SHADER_2D].pDomainShader_ = nullptr;

		// 頂点レイアウトの作成
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		checkResult_ = pDevice_->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), &shaderBundle_[SHADER_2D].pVertexLayout_);
		CHECK_MESSAGE(checkResult_ ,"頂点インプットレイアウトの作成に失敗しました");


		//シェーダーが無事作成できたので、コンパイルしたものは解放
		SAFE_RELEASE(pCompileVS_);
		SAFE_RELEASE(pCompilePS_);

		//ラスタライザ作成
		D3D11_RASTERIZER_DESC rdc = {};
		rdc.CullMode = D3D11_CULL_BACK;
		rdc.FillMode = D3D11_FILL_SOLID;
		rdc.FrontCounterClockwise = TRUE;
		checkResult_ = pDevice_->CreateRasterizerState(&rdc, &shaderBundle_[SHADER_2D].pRasterizerState_);
		CHECK_MESSAGE(checkResult_ ,"ラスタライザの作成に失敗しました");
	}


	//SHADER_UNLIT
	{
		//頂点シェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/Debug3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateVertexShader(pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_UNLIT].pVertexShader_);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの作成に失敗しました");


		//ピクセルシェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/Debug3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreatePixelShader(pCompilePS_->GetBufferPointer(), pCompilePS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_UNLIT].pPixelShader_);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの作成に失敗しました");


		//ハルシェーダーは不使用
		shaderBundle_[SHADER_UNLIT].pHullShader_ = nullptr;

		//ドメインシェーダーは不使用
		shaderBundle_[SHADER_UNLIT].pDomainShader_ = nullptr;

		// 頂点レイアウトの作成
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		checkResult_ = pDevice_->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), &shaderBundle_[SHADER_UNLIT].pVertexLayout_);
		CHECK_MESSAGE(checkResult_ ,"頂点インプットレイアウトの作成に失敗しました");


		//シェーダーが無事作成できたので、コンパイルしたものは解放
		SAFE_RELEASE(pCompileVS_);
		SAFE_RELEASE(pCompilePS_);

		//ラスタライザ作成
		D3D11_RASTERIZER_DESC rdc = {};
		rdc.CullMode = D3D11_CULL_BACK;
		rdc.FillMode = D3D11_FILL_WIREFRAME;
		rdc.FrontCounterClockwise = TRUE;
		checkResult_ = pDevice_->CreateRasterizerState(&rdc, &shaderBundle_[SHADER_UNLIT].pRasterizerState_);
		CHECK_MESSAGE(checkResult_ ,"ラスタライザの作成に失敗しました");
	}


	//SHADER_DISPLAY
	{
		//頂点シェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/DisplaymentShader.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateVertexShader(pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_DISPLAY].pVertexShader_);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの作成に失敗しました");


		//ピクセルシェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/DisplaymentShader.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreatePixelShader(pCompilePS_->GetBufferPointer(), pCompilePS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_DISPLAY].pPixelShader_);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの作成に失敗しました");


		//ハルシェーダーの作成
		checkResult_ = D3DCompileFromFile(L"Shader/DisplaymentShader.hlsl", nullptr, nullptr, "HS", "hs_5_0", NULL, 0, &pCompileHS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ハルシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateHullShader(pCompileHS_->GetBufferPointer(), pCompileHS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_DISPLAY].pHullShader_);
		CHECK_MESSAGE(checkResult_ ,"ハルシェーダーの作成に失敗しました");


		//ドメインシェーダーの作成
		checkResult_ = D3DCompileFromFile(L"Shader/DisplaymentShader.hlsl", nullptr, nullptr, "DS", "ds_5_0", NULL, 0, &pCompileDS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ドメインシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateDomainShader(pCompileDS_->GetBufferPointer(), pCompileDS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_DISPLAY].pDomainShader_);
		CHECK_MESSAGE(checkResult_ ,"ドメインシェーダーの作成に失敗しました");


		// 頂点レイアウトの作成（1頂点の情報が何のデータをどんな順番で持っているか）
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//頂点位置
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },	//テクスチャ（UV）座標
		};
		checkResult_ = pDevice_->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), &shaderBundle_[SHADER_DISPLAY].pVertexLayout_);
		CHECK_MESSAGE(checkResult_ ,"頂点インプットレイアウトの作成に失敗しました");


		//シェーダーが無事作成できたので、コンパイルしたものは解放
		SAFE_RELEASE(pCompileVS_);
		SAFE_RELEASE(pCompilePS_);
		SAFE_RELEASE(pCompileHS_);
		SAFE_RELEASE(pCompileDS_);

		//ラスタライザ作成
		D3D11_RASTERIZER_DESC rdc = {};
		rdc.CullMode = D3D11_CULL_BACK;
		rdc.FillMode = D3D11_FILL_SOLID;
		rdc.FrontCounterClockwise = TRUE;
		checkResult_ = pDevice_->CreateRasterizerState(&rdc, &shaderBundle_[SHADER_DISPLAY].pRasterizerState_);
		CHECK_MESSAGE(checkResult_ ,"ラスタライザの作成に失敗しました");
	}


	//SHADER_LIGHT
	{
		//頂点シェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/PointLightShader.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreateVertexShader(pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_LIGHT].pVertexShader_);
		CHECK_MESSAGE(checkResult_ ,"頂点シェーダーの作成に失敗しました");


		//ピクセルシェーダの作成
		checkResult_ = D3DCompileFromFile(L"Shader/PointLightShader.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS_, NULL);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの読み込みに失敗しました");

		checkResult_ = pDevice_->CreatePixelShader(pCompilePS_->GetBufferPointer(), pCompilePS_->GetBufferSize(), NULL, &shaderBundle_[SHADER_LIGHT].pPixelShader_);
		CHECK_MESSAGE(checkResult_ ,"ピクセルシェーダーの作成に失敗しました");


		//ハルシェーダーは不使用
		shaderBundle_[SHADER_LIGHT].pHullShader_ = nullptr;

		//ドメインシェーダーは不使用
		shaderBundle_[SHADER_LIGHT].pDomainShader_ = nullptr;

		// 頂点レイアウトの作成
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		checkResult_ = pDevice_->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS_->GetBufferPointer(), pCompileVS_->GetBufferSize(), &shaderBundle_[SHADER_LIGHT].pVertexLayout_);
		CHECK_MESSAGE(checkResult_ ,"頂点インプットレイアウトの作成に失敗しました");


		//シェーダーが無事作成できたので、コンパイルしたものは解放
		SAFE_RELEASE(pCompileVS_);
		SAFE_RELEASE(pCompilePS_);

		//ラスタライザ作成
		D3D11_RASTERIZER_DESC rdc = {};
		rdc.CullMode = D3D11_CULL_BACK;
		rdc.FillMode = D3D11_FILL_SOLID;
		rdc.FrontCounterClockwise = TRUE;
		checkResult_ = pDevice_->CreateRasterizerState(&rdc, &shaderBundle_[SHADER_LIGHT].pRasterizerState_);
		CHECK_MESSAGE(checkResult_ ,"ラスタライザの作成に失敗しました");
	}

	return S_OK;
}


//描画するShaderBundleを設定
void Direct3D::SetShader(SHADER_TYPE type)
{
	pContext_->RSSetState(shaderBundle_[type].pRasterizerState_);				//ラスタライザをセット
	pContext_->VSSetShader(shaderBundle_[type].pVertexShader_, NULL, 0);        //頂点シェーダをセット
	pContext_->PSSetShader(shaderBundle_[type].pPixelShader_, NULL, 0);         //ピクセルシェーダをセット
	pContext_->HSSetShader(shaderBundle_[type].pHullShader_, NULL, 0);			//ハルシェーダをセット
	pContext_->DSSetShader(shaderBundle_[type].pDomainShader_, NULL, 0);		//ドメインシェーダをセット
	pContext_->IASetInputLayout(shaderBundle_[type].pVertexLayout_);			//頂点インプットレイアウトをセット
	pContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);   //データの入力種類を指定
}


//描画開始
void Direct3D::BeginDraw()
{
	//何か準備できてないものがあったら諦める
	if (NULL == pDevice_) return;
	if (NULL == pContext_) return;
	if (NULL == pRenderTargetView_) return;
	if (NULL == pSwapChain_) return;

	//背景の色
	float clearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

	//画面をクリア
	pContext_->ClearRenderTargetView(pRenderTargetView_, clearColor);

	//深度バッファクリア
	pContext_->ClearDepthStencilView(pDepthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);
}


//描画終了
void Direct3D::EndDraw()
{
	//2D表示
	Direct2D::Draw();

	//スワップ（バックバッファを表に表示する）
	pSwapChain_->Present(0, 0);
}


//解放
void Direct3D::Release()
{
	Direct2D::Release();

	SAFE_RELEASE(pCompileVS_);
	SAFE_RELEASE(pCompilePS_);
	SAFE_RELEASE(pCompileHS_);
	SAFE_RELEASE(pCompileDS_);

	SAFE_RELEASE(pDepthStencil_);
	SAFE_RELEASE(pDepthStencilView_);
	SAFE_RELEASE(pBlendState_);
	SAFE_RELEASE(pRenderTargetView_);

	for (SHADER_BUNDLE shader : shaderBundle_)
	{
		SAFE_RELEASE(shader.pRasterizerState_);
		SAFE_RELEASE(shader.pVertexLayout_);
		SAFE_RELEASE(shader.pVertexShader_);
		SAFE_RELEASE(shader.pPixelShader_);
		SAFE_RELEASE(shader.pHullShader_);
		SAFE_RELEASE(shader.pDomainShader_);
	}

	if (pContext_)
	{
		pContext_->ClearState();
	}

	SAFE_RELEASE(pSwapChain_);
	SAFE_RELEASE(pContext_);
	SAFE_RELEASE(pDevice_);
}


//三角形と線分（レイ）の衝突判定（衝突判定に使用）
bool Direct3D::Intersect(XMVECTOR& start, XMVECTOR& direction, XMVECTOR& v0, XMVECTOR& v1, XMVECTOR& v2, float* distance)
{
	//微小な定数([M?ller97] での値)
	constexpr float kEpsilon = 1e-6f;

	//三角形の2辺
	XMVECTOR edge1 = XMVectorSet(v1.vecX - v0.vecX, v1.vecY - v0.vecY, v1.vecZ - v0.vecZ, 0.0f);
	XMVECTOR edge2 = XMVectorSet(v2.vecX - v0.vecX, v2.vecY - v0.vecY, v2.vecZ - v0.vecZ, 0.0f);

	XMVECTOR alpha = XMVector3Cross(direction, edge2);
	float det = XMVector3Dot(edge1, alpha).vecX;

	// 三角形に対して、レイが平行に入射するような場合 det = 0 となる。
	// det が小さすぎると 1/det が大きくなりすぎて数値的に不安定になるので
	// det ? 0 の場合は交差しないこととする。
	if (-kEpsilon < det && det < kEpsilon)
	{
		return false;
	}

	float invDet = 1.0f / det;
	XMVECTOR r = start - v0;

	// u が 0 <= u <= 1 を満たしているかを調べる。
	float u = XMVector3Dot(alpha, r).vecX * invDet;
	if (u < 0.0f || u > 1.0f)
	{
		return false;
	}

	XMVECTOR beta = XMVector3Cross(r, edge1);

	// v が 0 <= v <= 1 かつ u + v <= 1 を満たすことを調べる。
	// すなわち、v が 0 <= v <= 1 - u をみたしているかを調べればOK。
	float v = XMVector3Dot(direction, beta).vecX * invDet;
	if (v < 0.0f || u + v > 1.0f)
	{
		return false;
	}

	// t が 0 <= t を満たすことを調べる。
	float t = XMVector3Dot(edge2, beta).vecX * invDet;
	if (t < 0.0f)
	{
		return false;
	}

	*distance = t;
	return true;
}

//Zバッファへの書き込みON/OFF
void Direct3D::SetDepthBafferWriteEnable(bool isWrite)
{
	if (isWrite)
	{
		///////////////// ON /////////////////
		
		//Zバッファ（デプスステンシルを指定する）
		pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView_);
	}
	else
	{
		///////////////// OFF /////////////////

		pContext_->OMSetRenderTargets(1, &pRenderTargetView_, nullptr);
	}
}
