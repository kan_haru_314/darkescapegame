#pragma once

//インクルード
#include <DirectXMath.h>
#include <list>
#include <string>
#include <assert.h>

#include "SphereCollider.h"
#include "BoxCollider.h"
#include "Transform.h"

//ネームスペース
using namespace DirectX;


//全てのゲームオブジェクト（シーンも含めて）が継承するインターフェース
class GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//

	//フラグ
	struct OBJECT_STATE
	{
		unsigned initialized : 1;	//初期化済みか
		unsigned entered : 1;		//更新するか
		unsigned visible : 1;		//描画するか
		unsigned dead : 1;			//削除するか
	};


	//------------------ フィールド ------------------//
	OBJECT_STATE state_;				//フラグステータス
	GameObject* pParent_;				//親オブジェクト
	std::list<GameObject*> childList_;	//子オブジェクトリスト


	//------------------ メソッド ------------------//
	
	//オブジェクト削除（再帰）
	//引数：GameObject*(削除するオブジェクト)
	void KillObjectSub(GameObject* obj);

	

protected:
	//----------------------------- [ protected ] -----------------------------//
	
	//------------------ フィールド ------------------//
	Transform				transform_;			//位置や向きなどを管理するオブジェクト
	std::string				objectName_;		//オブジェクトの名前
	std::list<Collider*>	colliderList_;		//衝突判定リスト



public:
	//----------------------------- [ protected ] -----------------------------//
	
	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	GameObject();

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	GameObject(GameObject* parent);
	
	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//引数：const std::string&(オブジェクト名)
	//返値：なし
	GameObject(GameObject* parent, const std::string& name);

	//デストラクタ
	//引数：なし
	//返値：なし
	virtual ~GameObject();

	//ワールド行列の取得(親の影響を受けた最終的な行列)
	//引数：なし
	//返値：XMMATRIX(ワールド行列)
	XMMATRIX GetWorldMatrix();

	//子オブジェクトリストを取得
	//引数：なし
	//返値：std::list<GameObject*>*(子オブジェクトリスト)
	std::list<GameObject*>* GetChildList();

	//親オブジェクトを取得
	//引数：なし
	//返値：GameObject*(親オブジェクトのアドレス)
	GameObject* GetParent();

	//名前でオブジェクトを検索（対象は自分の子供以下）
	//引数：const std::string&(検索する名前)
	//返値：GameObject*(見つけたオブジェクトのアドレス(見つからなければnullptr))
	GameObject* FindChildObject(const std::string& name);

	//名前でオブジェクトを検索（対象は全体）
	//引数：const std::string&(検索する名前)
	//返値：GameObject*(見つけたオブジェクトのアドレス(見つからなければnullptr))
	GameObject* FindObject(const std::string& name) { return GetRootJob()->FindChildObject(name); }

	//子供の中からオブジェクトを探し、リストにする
	//引数：const std::string&(検索する名前)
	//返値：std::list<GameObject*>（見つけたゲームオブジェクトリスト）
	std::list<GameObject*> FindChildObjectList(const std::string& name);

	//オブジェクトを探し、リストにする
	//引数：const std::string&(検索する名前)
	//返値：std::list<GameObject*>（見つけたゲームオブジェクトリスト）
	std::list<GameObject*> FindObjectList(const std::string& name);

	//オブジェクトの名前を取得
	//引数：void
	//返値：const std::string&(名前)
	const std::string& GetObjectName(void) const;

	//子オブジェクトを追加（リストの最後へ）
	//引数：GameObject*(追加するオブジェクト)
	//返値：void
	void PushBackChild(GameObject* obj);

	//子オブジェクトを追加（リストの先頭へ）
	//引数：GameObject*(追加するオブジェクト)
	//返値：void
	void PushFrontChild(GameObject* obj);

	//子オブジェクトを全て削除
	//引数：なし
	//返値：void
	void KillAllChildren();

	//コライダー（衝突判定）を追加する
	//引数：Collider*(衝突判定)
	//返値：void
	void AddCollider(Collider* collider);

	//何かと衝突した場合に呼ばれる（オーバーライド用）
	//引数：GameObject*(衝突した相手)
	//返値：void
	virtual void OnCollision(GameObject* pTarget) {};

	//衝突判定
	//引数：GameObject*(衝突してるか調べる相手)
	//返値：void
	void Collision(GameObject* pTarget);

	//テスト用の衝突判定枠を表示
	//引数：なし
	//返値：void
	void CollisionDraw();

	//RootJobを取得
	//引数：なし
	//返値：GameObject*(ルートオブジェクト)
	GameObject* GetRootJob();


	//------------------- 各オブジェクトで必ず作るメソッド -------------------//

	//初期化
	virtual void Initialize(void) = 0;

	//更新
	virtual void Update(void) = 0;
	
	//描画
	virtual void Draw() = 0;
	
	//解放
	virtual void Release(void) = 0;


	//------------------- 子供の関数を呼ぶメソッド -------------------//

	//更新
	void UpdateSub();
	
	//描画
	void DrawSub();
	
	//解放
	void ReleaseSub();


	//------------------- 各フラグの制御メソッド -------------------//

	//削除するかどうか
	//返値：bool(true = 削除, false = 削除しない)
	bool IsDead();			

	//自分を削除する
	void KillMe();			

	//Updateを許可
	void Enter();			

	//Updateを拒否
	void Leave();			

	//Drawを許可
	void Visible();			

	//Drawを拒否
	void Invisible();		

	//初期化済みかどうか
	//返値：bool(true = 初期化済み, false = 初期化してない)
	bool IsInitialized();	

	//初期化済みにする
	void SetInitialized();	

	//Update実行していいか
	//返値：bool(true = 更新, false = 更新しない)
	bool IsEntered();		

	//Draw実行していいか
	//返値：bool(true = 描画, false = 描画しない)
	bool IsVisibled();		


	//------------------- 各アクセスメソッド -------------------//

	//位置座標を取得
	XMVECTOR GetPosition() { return transform_.position_; }

	//回転を取得
	XMVECTOR GetRotate() { return transform_.rotate_; }

	//サイズを取得
	XMVECTOR GetScale() { return transform_.scale_; }

	//ワールド行列上の位置座標を取得
	XMVECTOR GetWorldPosition() { return GetParent()->transform_.position_ + transform_.position_; }
	
	//ワールド行列上の回転を取得
	XMVECTOR GetWorldRotate() { return GetParent()->transform_.rotate_ + transform_.rotate_; }
	
	//ワールド行列上のサイズを取得
	XMVECTOR GetWorldScale() { return GetParent()->transform_.scale_ + transform_.scale_; }
	
	//位置座標をセット
	void SetPosition(XMVECTOR position) { transform_.position_ = position; }
	
	//位置座標をセット
	void SetPosition(float x, float y, float z) { SetPosition(XMVECTOR{ x, y, z }); }
	
	//回転をセット
	void SetRotate(XMVECTOR rotate) { transform_.rotate_ = rotate; }
	
	//回転をセット
	void SetRotate(float x, float y, float z) { SetRotate(XMVECTOR{ x, y, z }); }
	
	//X軸回転をセット
	void SetRotateX(float x) { SetRotate(x, transform_.rotate_.vecY, transform_.rotate_.vecZ); }
	
	//Y軸回転をセット
	void SetRotateY(float y) { SetRotate(transform_.rotate_.vecX, y, transform_.rotate_.vecZ); }
	
	//Z軸回転をセット
	void SetRotateZ(float z) { SetRotate(transform_.rotate_.vecX, transform_.rotate_.vecY, z); }
	
	//サイズをセット
	void SetScale(XMVECTOR scale) { transform_.scale_ = scale; }
	
	//サイズをセット
	void SetScale(float x, float y, float z) { SetScale(XMVectorSet(x, y, z, 0)); }

};


//オブジェクトを作成するテンプレート
template <class T>
T* Instantiate(GameObject* pParent)
{
	T* pNewObject = new T(pParent);
	if (pParent != nullptr)
	{
		pParent->PushBackChild(pNewObject);
	}
	pNewObject->Initialize();
	return pNewObject;
}

