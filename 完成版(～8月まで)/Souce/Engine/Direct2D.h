#pragma once

//インクルード
#include <Windows.h>
#include <d2d1.h>
#include <dwrite.h>
#include <string>

//リンカ
#pragma comment( lib, "d2d1.lib" )
#pragma comment( lib, "dwrite.lib" )


//画面の描画に関する処理
namespace Direct2D
{
	//----------------------------- [ public ] -----------------------------//

	//------------------ 変数 ------------------//
	extern IDWriteFactory* pDWriteFactory;
	extern ID2D1RenderTarget* pRT;

	//初期化
	//引数：HWND(ウィンドウハンドル)
	//返値：HRESULT(状態)
	HRESULT Init(HWND hWnd);

	//描画
	//引数：なし
	//返値：void
	void Draw();

	//解放
	//引数：なし
	//返値：void
	void Release();

	//2D表示もできるフラグ
	//引数：なし
	//返値：UINT(フラグ)
	UINT Get2dFlags();
};

