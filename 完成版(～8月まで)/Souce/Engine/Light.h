#pragma once

//インクルード
#include "Transform.h"
#include "Direct3D.h"

//ライト
namespace Light
{
	//----------------------------- [ public ] -----------------------------//
	
	//------------------ 関数 ------------------//

	//初期化
	//引数：なし
	//返値：HRESULT(状態)
	HRESULT Initialize();

	//描画
	//引数：なし
	//返値：void
	void Draw();

	//解放
	//引数：なし
	//返値：void
	void Release();

	//Drawを許可
	//引数：なし
	//返値：void
	void Visible();

	//Drawを拒否
	//引数：なし
	//返値：void
	void Invisible();		

	//位置を設定
	//引数：XMVECTOR(位置)
	//返値：void
	void SetPosition(XMVECTOR position);

	//位置を設定
	//引数：XMVECTOR(大きさ)
	//返値：void
	void SetScale(XMVECTOR scale);

	//明るさを設定
	//引数：float(明るさ = 0 〜 1)
	//返値：void
	void SetBright(float bright);

	//位置を取得
	//引数：なし
	//返値：XMVECTOR(位置)
	XMVECTOR GetPosition();

	//位置をXMFLOAT4で取得
	//引数：なし
	//返値：XMFLOAT4(位置)
	XMFLOAT4 GetConvertPosition();

	//大きさを取得
	//引数：なし
	//返値：XMVECTOR(大きさ)
	XMVECTOR GetScale();

	//明るさを取得
	//引数：なし
	//返値：float(明るさ = 0 〜 1)
	float GetBright();
};