#pragma once

//インクルード
#include "GameObject.h"


//一番トップに来るオブジェクト
//すべてのオブジェクトは、これの子孫になる
class RootObject :	public GameObject
{
public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	RootObject();

	//デストラクタ
	//引数：なし
	//返値：なし
	~RootObject();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};