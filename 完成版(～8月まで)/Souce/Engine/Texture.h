#pragma once

//インクルード
#include <d3d11.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <wincodec.h>
#include <string>

//リンカ
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")
#pragma comment( lib, "WindowsCodecs.lib" )

//ネームスペース
using namespace DirectX;


//テクスチャ関連を扱うクラス
class Texture
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------- フィールド -------------------//
	ID3D11SamplerState*			pSampleLinear_;		//テクスチャサンプラー（テクスチャの貼り方）
	ID3D11ShaderResourceView*	pTextureSRV_;		//シェーダーリソースビュー（テクスチャをシェーダーに送るためのもの）

	IWICImagingFactory*		pFactory_;				//ファクトリー
	IWICBitmapDecoder*		pDecoder_;				//デコーダー
	IWICBitmapFrameDecode*	pFrame_;				//フレーム
	IWICFormatConverter*	pFormatConverter_;		//フォーマットコンバーター

	XMVECTOR size_;									//画像ファイルのサイズ

	HRESULT checkResult_;							//状態チェック



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------- メソッド -------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	Texture();

	//デストラクタ
	//引数：なし
	//返値：なし
	~Texture();

	//ロード
	//引数：std::string(画像ファイル名)
	//返値：HRESULT(状態)
	HRESULT Load(std::string fileName);

	//サンプラーの取得
	//引数：なし
	//返値：ID3D11SamplerState*(サンプラー)
	ID3D11SamplerState* GetSampler() { return pSampleLinear_; }	

	//シェーダーリソースビューの取得
	//引数：なし
	//返値：ID3D11SamplerState*(テクスチャのSRV)
	ID3D11ShaderResourceView* GetSRV() { return pTextureSRV_; }
	
	//画像サイズの取得
	//引数：なし
	//返値：XMVECTOR(サイズ)
	XMVECTOR GetSize() { return size_; }	
};

