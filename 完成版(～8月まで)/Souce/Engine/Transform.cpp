//インクルード
#include "Transform.h"

#include "Camera.h"

//コンストラクタ
Transform::Transform(): pParent_(nullptr)
{
	position_ = XMVectorSet(0, 0, 0, 0);
	rotate_ = XMVectorSet(0, 0, 0, 0);
	scale_ = XMVectorSet(1, 1, 1, 0);
	matTranslate_ = XMMatrixIdentity();
	matRotate_ = XMMatrixIdentity();
	matScale_ = XMMatrixIdentity();
}

//デストラクタ
Transform::~Transform()
{
}

//各行列の計算
void Transform::Calclation()
{
	//移動行列
	matTranslate_ = XMMatrixTranslation(position_.vecX, position_.vecY, position_.vecZ);

	//回転行列
	XMMATRIX rotateX, rotateY, rotateZ;
	rotateX = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));
	rotateY = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));
	rotateZ = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));
	matRotate_ = rotateZ * rotateX * rotateY;

	//拡大縮小
	matScale_ = XMMatrixScaling(scale_.vecX, scale_.vecY, scale_.vecZ);
}

//ワールド行列を取得
XMMATRIX Transform::GetWorldMatrix(bool isBillBoard)
{
	//行列計算
	Calclation();

	//何もしない行列
	XMMATRIX matBill = XMMatrixIdentity();

	//ビルボード行列を使用するか
	if (isBillBoard)
	{
		//ビルボード行列
		matBill = XMMatrixInverse(nullptr, Camera::GetViewMatrix());
		matBill.r[3].vecX = 0.0f;
		matBill.r[3].vecY = 0.0f;
		matBill.r[3].vecZ = 0.0f;
		matBill.r[3].vecW = 1.0f;
	}

	if (pParent_)
	{
		return  matScale_ * matBill *  matRotate_ * matTranslate_ * pParent_->GetWorldMatrix();
	}

	return  matScale_ * matBill * matRotate_ * matTranslate_;
}

