#pragma once

//インクルード用定数宣言
#define DIRECTINPUT_VERSION 0x0800

//インクルード
#include <dInput.h>
#include <DirectXMath.h>

#include "XInput.h"

//リンカ
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib, "Xinput.lib")

//ネームスペース
using namespace DirectX;


//DirectInputを使ったキーボード入力処理
namespace Input
{
	//----------------------------- [ public ] -----------------------------//
	
	//------------------ 関数 ------------------//
	
	//初期化
	//引数：HWND(ウィンドウハンドル)
	//返値：HRESULT(状態)
	HRESULT Initialize(HWND hWnd);

	//更新
	//引数：なし
	//返値：void
	void Update();


	//------------------ キーボード ------------------//

	//キーが押されているか調べる
	//引数：int(調べたいキーのコード)
	//返値：bool(true = 押された, false = 押されてない)
	bool IsKey(int keyCode);

	//キーを今押したか調べる（押しっぱなしは無効）
	//引数：int(調べたいキーのコード)
	//返値：bool(true = 押された, false = 何もしてない)
	bool IsKeyDown(int keyCode);

	//キーを今放したか調べる
	//引数：int(調べたいキーのコード)
	//返値：bool(true = 放した, false = 何もしてない)
	bool IsKeyUp(int keyCode);


	//------------------ マウス ------------------//

	//マウスのボタンが押されているか調べる
	//引数：int(調べたいボタンの番号)
	//返値：bool(true = 押された, false = 押されてない)
	bool IsMouseButton(int buttonCode);

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：int(調べたいボタンの番号)
	//返値：bool(true = 押された, false = 何もしてない)
	bool IsMouseButtonDown(int buttonCode);

	//マウスのボタンを今放したか調べる
	//引数：int(調べたいボタンの番号)
	//返値：bool(true = 放した, false = 何もしてない)
	bool IsMouseButtonUp(int buttonCode);


	//マウスカーソルの位置を取得
	//引数：なし
	//返値：XMVECTOR（マウスカーソルの位置）
	XMVECTOR GetMousePosition();

	//マウスカーソルの位置をセット
	//引数：int（x座標）
	//引数：int（y座標）
	//返値：void
	void SetMousePosition(int x, int y);

	//そのフレームでのマウスの移動量を取得
	//引数：なし
	//返値：XMVECTOR（X = Xの差, Y = Yの差, Z = ホイールの差）
	XMVECTOR GetMouseMove();


	//------------------ パッド ------------------//

	//パッドのボタンが押されているか調べる
	//引数：int(調べたいボタンの番号)
	//引数：int(パッドの識別番号)
	//返値：bool(true = 押された, false = 押されてない)
	bool IsPadButton(int buttonCode, int padID = 0);

	//パッドのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：int(調べたいボタンの番号)
	//引数：int(パッドの識別番号)
	//返値：bool(true = 押された, false = 何もしてない)
	bool IsPadButtonDown(int buttonCode, int padID = 0);

	//パッドのボタンを今放したか調べる
	//引数：int(調べたいボタンの番号)
	//引数：int(パッドの識別番号)
	//返値：bool(true = 放した, false = 何もしてない)
	bool IsPadButtonUp(int buttonCode, int padID = 0);

	//左スティックの傾きを取得
	//引数：int(パッドの識別番号)
	//返値：XMVECTOR(傾き : -1 〜 1)
	XMVECTOR GetPadStickL(int padID = 0);

	//右スティックの傾きを取得
	//引数：int(パッドの識別番号)
	//返値：XMVECTOR(傾き : -1 〜 1)
	XMVECTOR GetPadStickR(int padID = 0);

	//左トリガーの押し込み具合を取得
	//引数：int(パッドの識別番号)
	//返値：float(押し込み具合 : 0 〜 1)
	float GetPadTrrigerL(int padID = 0);

	//右トリガーの押し込み具合を取得
	//引数：int(パッドの識別番号)
	//返値：float(押し込み具合 : 0 〜 1)
	float GetPadTrrigerR(int padID = 0);

	//振動させる
	//引数：int(左モーターの強さ)
	//引数：int(右モーターの強さ)
	//引数：int(パッドの識別番号)
	//返値：void
	void SetPadVibration(int l, int r, int padID = 0);




	//開放
	void Release();
};
