#pragma once

//インクルード
#include "Command.h"

//走行コマンド
class RunCommand : public Command
{
public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	RunCommand();

	//デストラクタ
	//引数：なし
	//返値：なし
	~RunCommand();

	//コマンド動作
	//引数：Charactor*(キャラオブジェクト)
	//返値：void
	void Execute(Charactor* chara) override;
};