#pragma once

//プロトタイプ宣言
class Charactor;


//コマンドクラス
class Command
{
protected:
	//----------------------------- [ protected ] -----------------------------//

	//------------------ フィールド ------------------//
	float exeTime_;			//動作時間(s)
	float exeMaxTime_;		//最大動作時間(s)



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//
	
	//コンストラクタ
	//引数：なし
	//返値：なし
	Command();

	//デストラクタ
	//引数：なし
	//返値：なし
	virtual ~Command();

	//コマンド動作
	//引数：Charactor*(キャラオブジェクト)
	//返値：void
	virtual void Execute(Charactor* chara) = 0;

	//動作時間を超えたか
	//引数：なし
	//返値：bool(true = 実行中, false = 実行終了)
	bool IsExecute();

	//時間処理
	//引数：なし
	//返値：void
	void ExeTime();

	//処理時間の設定
	//引数：float(制限時間)
	//返値：void
	void SetMaxTime(float time);
};

