//インクルード
#include "WaitCommand.h"
#include "../GameObject/Charactor.h"

//コンストラクタ
WaitCommand::WaitCommand()
{
	//時間設定
	exeMaxTime_ = 0.0f;
	exeTime_ = 0.0f;
}

//デストラクタ
WaitCommand::~WaitCommand()
{
}

//コマンド動作
void WaitCommand::Execute(Charactor* chara)
{
	//実行
	chara->Wait();
}