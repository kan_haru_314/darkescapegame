#pragma once

//インクルード
#include "Command.h"

//待機コマンド
class WaitCommand : public Command
{
public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：なし
	//返値：なし
	WaitCommand();

	//デストラクタ
	//引数：なし
	//返値：なし
	~WaitCommand();

	//コマンド動作
	//引数：Charactor*(キャラオブジェクト)
	//返値：void
	void Execute(Charactor* chara) override;
};