//インクルード
#include "RunCommand.h"
#include "../GameObject/Charactor.h"

//コンストラクタ
RunCommand::RunCommand()
{
	//時間設定
	exeMaxTime_ = 0.0f;
	exeTime_ = 0.0f;
}

//デストラクタ
RunCommand::~RunCommand()
{
}

//コマンド動作
void RunCommand::Execute(Charactor* chara)
{
	//実行
	chara->Run();
}