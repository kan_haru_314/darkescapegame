//インクルード
#include "Map.h"

#include "../Engine/Model.h"
#include "../Engine/Displayment.h"
#include "../Engine/Global.h"

//コンストラクタ
Map::Map(GameObject * parent)
	:GameObject(parent, "Map")
	, MAP_RANGE(0.9f)
	, map_(nullptr)
{
	mapScale_ = (float)GetPrivateProfileInt("MAP", "Scale", 512, ".\\GameSetting.ini");
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	//マップ生成
	map_ = new Displayment(this);
	map_->Load("Model/Ground.fbx");

	//巨大化
	transform_.scale_ *= mapScale_;
	transform_.scale_.vecY /= 8.0f;

	transform_.position_.vecY = -transform_.scale_.vecY;
}

//更新
void Map::Update()
{
}

//描画
void Map::Draw()
{
	map_->Draw(transform_);
}

//開放
void Map::Release()
{
	SAFE_DELETE(map_);
}

//指定位置の高さを取得
float Map::GetPositionHeight(XMVECTOR pos)
{
	return map_->GetPositionHeight(pos, transform_.scale_);
}

//マップの大きさを取得
float Map::GetMapScale()
{
	return mapScale_;
}

//マップの使用許可範囲を取得
float Map::GetMapRange()
{
	return mapScale_ * MAP_RANGE;
}
