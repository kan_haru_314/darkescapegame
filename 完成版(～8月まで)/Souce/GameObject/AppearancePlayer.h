#pragma once

//インクルード
#include "../Engine/GameObject.h"

//プロトタイプ宣言
class Lantern;

//プレイヤーを管理するクラス
class AppearancePlayer : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ 構造体 ------------------//

	//アニメーション情報
	struct AnimeState
	{
		float speed_;			//アニメーションスピード
		int animeStart_;		//アニメーションの開始フレーム
		int animeEnd_;			//アニメーションの終了フレーム
	};


	//------------------ フィールド ------------------//
	AnimeState runAnime_;	//走行アニメーションデータ
	AnimeState walkAnime_;	//歩行アニメーションデータ
	Lantern* lantern_;		//ランタンのポインタ
	int hModel_;			//モデル番号



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	AppearancePlayer(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~AppearancePlayer();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//歩くアニメーション設定
	//引数：なし
	//返値：void
	void WalkAnimation();

	//走るアニメーション設定
	//引数：なし
	//返値：void
	void RunAnimation();
};