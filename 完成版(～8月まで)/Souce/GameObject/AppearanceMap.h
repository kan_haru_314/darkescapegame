#pragma once

//インクルード
#include "../Engine/GameObject.h"

//マップを管理するクラス
class AppearanceMap : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const float SCALE_MAGNIFICATION;	//サイズ倍率
	const float ORIGIN_POS;				//最大位置
	const float MAX_POS;				//移動後の位置
	const float RUN_SPEED;				//走りの移動速度
	const float WALK_SPEED;				//歩きの移動速度
	float speed_;						//移動速度
	int hModel_;						//モデル番号



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	AppearanceMap(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~AppearanceMap();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//歩く速度に合わせる
	//引数：なし
	//返値：void
	void WalkSpeed();

	//走る速度に合わせる
	//引数：なし
	//返値：void
	void RunSpeed();
};