//インクルード
#include "Coin.h"

#include "../Engine/Model.h"
#include "../Engine/SphereCollider.h"

#include "Map.h"
#include "../Image/CoinCount.h"

//定数宣言
#define COLLIDER_SIZE 1.5f		//球体型の当たり判定の大きさ

//コンストラクタ
Coin::Coin(GameObject* parent)
	: GameObject(parent, "Coin")
	, hModel_(-1)
{
}

//デストラクタ
Coin::~Coin()
{
}

//初期化
void Coin::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Model/Coin.fbx");
	assert(hModel_ >= 0);

	//高さは後調整
	transform_.position_.vecY = ((Map*)FindObject("Map"))->GetPositionHeight(transform_.position_);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorZero(), COLLIDER_SIZE);
	AddCollider(collision);
}

//更新
void Coin::Update()
{
	transform_.rotate_.vecY++;
}

//描画
void Coin::Draw()
{
	//モデル描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//解放
void Coin::Release()
{
}

//衝突判定
void Coin::OnCollision(GameObject* pTarget)
{
	if (pTarget->GetObjectName() == "Player")
	{
		//コイン回収
		((CoinCount*)FindObject("CoinCount"))->CoinCountUp();
		KillMe();
	}
}
