//インクルード
#include "Player.h"

#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/SphereCollider.h"

#include "Map.h"
#include "Lantern.h"

#include "../Image/Stamina.h"

//定数宣言
#define STAMINA_DIFF -0.001f		//走る際のスタミナ減少(1 〜 -1)
#define COLLIDER_SIZE 1.0f			//球体型の当たり判定の大きさ

//コンストラクタ
Player::Player(GameObject * parent)
	:Charactor(parent, "Player")
	, CAMERA_LENGTH(25.0f)
	, WALK_STATE{ 0.2f, 0, 13, 87, 1.2f }
	, RUN_STATE{ 0.7f, 88, 101, 175, 1.5f }
	, DEPTH_VEC(XMVectorSet( 0.0f, 0.0f, 1.0f, 0.0f ))
	, CAMERA_HEIGHT(XMVectorSet(0.0f, 5.0f, 0.0f, 0.0f))
	, camAngle_(XMVectorZero())
	, lantern_(nullptr)
	, map_(nullptr)
	, stamina_(nullptr)
	, dirAngle_(0.0f)
{
	camSensitivity_ = (float)GetPrivateProfileInt("CAMERA", "Sensitivity", 10, ".\\GameSetting.ini");
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//コマンドキュー作成
	Charactor::Initialize();

	//モデルデータのロード
	hModel_ = Model::Load("Model/Player.fbx");
	assert(hModel_ >= 0);

	//マップオブジェクトを探す
	map_ = (Map*)FindObject("Map");

	//カメラの角度初期化
	camAngle_ = XMVectorZero();
	camAngle_.vecY = 135.0f;
	camAngle_.vecX = 180.0f;

	//ランタンの作成
	lantern_ = Instantiate<Lantern>(GetParent());

	//初期アニメーション設定
	Model::SetAnimFrame(hModel_, WALK_STATE.animeStart_, WALK_STATE.animeEnd_, 0.0f);
	Model::SetNowAnimFrame(hModel_, 0.0f);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorZero(), COLLIDER_SIZE);
	AddCollider(collision);
}

//更新
void Player::Update()
{
	//--------------------------- カメラの角度 ---------------------------//

	//移動量の加算
	camAngle_ += Input::GetMouseMove() / camSensitivity_;

	//Y軸の回転制限
	if (camAngle_.vecX >= 360.0f)
	{
		camAngle_.vecX = 360.0f - camAngle_.vecX;
	}
	else if (camAngle_.vecX < 0.0f)
	{
		camAngle_.vecX = 360.0f + camAngle_.vecX;
	}

	//X軸の回転制限
	if (camAngle_.vecY > 179.0f)
	{
		camAngle_.vecY = 179.0f;
	}
	else if (camAngle_.vecY < 80.0f)
	{
		camAngle_.vecY = 80.0f;
	}

	//回転行列
	XMMATRIX mX, mY;
	mX = XMMatrixRotationY(XMConvertToRadians(camAngle_.vecX));					//Y軸で回転させる行列
	mY = XMMatrixRotationX(XMConvertToRadians(-(camAngle_.vecY - 90.0f)));		//X軸で回転させる行列


	//--------------------------- 移動操作 ---------------------------//

	//ローカルな移動方向
	moveDir_ = XMVectorSet(
		(float)(Input::IsKey(DIK_A) - Input::IsKey(DIK_D)),
		0.0f,
		(float)(Input::IsKey(DIK_S) - Input::IsKey(DIK_W)), 
		0.0f
	);

	//移動量調整
	moveDir_ = XMVector3Normalize(moveDir_);

	//カメラに方向を合わせる
	moveDir_ = XMVector3TransformCoord(moveDir_, mX);
	moveDir_.vecY = 0;


	//--------------------------- 向く方向 ---------------------------//

	//移動方向の正規化
	moveDir_ = XMVector3Normalize(moveDir_);

	//外積を求める
	XMVECTOR crossDir = XMVector3Cross(DEPTH_VEC, moveDir_);

	//方向を「-180 〜 180」の範囲にするための符号
	float sign = 1.0f;
	if (crossDir.vecY < 0)
	{
		sign *= -1.0f;
	}

	//内積を求める
	float dotDir = XMVector3Dot(DEPTH_VEC, moveDir_).vecX;

	//回転角度
	dirAngle_ = XMConvertToDegrees((float)acos((double)dotDir)) * sign;


	//--------------------------- 入力 ---------------------------//

	//行動
	CommandNumber cmd = WAIT;

	//移動したか
	if (XMVector3Length(moveDir_).vecX > 0.0f)
	{
		//走っているか
		if (Input::IsKey(DIK_LSHIFT) && stamina_->GetGauge() > 0.0f)
		{
			//走り
			cmd = RUN;
		}
		else
		{
			//歩き
			cmd = WALK;
		}
	}

	//コマンドセット
	inv_->SetCmmand(cmd);

	//実行
	inv_->Execute();

	//高さは後調整
	transform_.position_.vecY = map_->GetPositionHeight(transform_.position_);


	//--------------------------- カメラ操作 ---------------------------//

	//カメラ移動範囲の中心
	XMVECTOR centerPos = transform_.position_ + CAMERA_HEIGHT;

	//カメラ設定
	Camera::SetPosition(centerPos + XMVector3TransformCoord(DEPTH_VEC * CAMERA_LENGTH, mY * mX));
	Camera::SetTarget(centerPos);
}

//描画
void Player::Draw()
{
	//モデル描画
	Charactor::Draw();

	//ランタンの位置更新(アニメーション処理後)
	lantern_->SetRotateY(transform_.rotate_.vecY);
	lantern_->SetPosition(Model::GetBonePosition(hModel_, "MiddleProximal_R"));
}

//スタミナを設定
void Player::SetStamina(Stamina* stamina)
{
	stamina_ = stamina;
}

//通行止め
bool Player::IsBackRange()
{
	float range = map_->GetMapRange() / 2.0f;

	if (transform_.position_.vecX > range)
	{
		return true;
	}
	else if (transform_.position_.vecX < -range)
	{
		return true;
	}

	if (transform_.position_.vecZ > range)
	{
		return true;
	}
	else if (transform_.position_.vecZ < -range)
	{
		return true;
	}

	return false;
}

//待機
void Player::Wait()
{
	//------------------ アニメーション ------------------//

	//アニメーション不動
	Model::SetAnimSpeedFrame(hModel_, 0.0f);

	//アニメーションを初期に
	Model::SetNowAnimFrame(hModel_, 0.0f);
}

//歩く
void Player::Walk()
{
	//------------------ 移動 ------------------//

	transform_.position_ += moveDir_ * WALK_STATE.SPEED_;

	if (IsBackRange())
	{
		transform_.position_ -= moveDir_ * WALK_STATE.SPEED_;
	}

	transform_.rotate_.vecY = dirAngle_;


	//------------------ アニメーション ------------------//
	
	//歩きアニメーションに現フレーム数を変更
	if (Model::GetNowAnimFrame(hModel_) >= RUN_STATE.animeStart_)
	{
		Model::SetNowAnimFrame(hModel_, (float)(Model::GetNowAnimFrame(hModel_) - RUN_STATE.animeStart_));
	}

	//歩き始め
	if (Model::GetNowAnimFrame(hModel_) < WALK_STATE.animeRoopStart_)
	{
		//歩き始め
		Model::SetAnimFrame(hModel_, WALK_STATE.animeStart_, WALK_STATE.animeEnd_, WALK_STATE.animeCount_);
	}
	else
	{
		//歩きループ
		Model::SetAnimFrame(hModel_, WALK_STATE.animeRoopStart_, WALK_STATE.animeEnd_, WALK_STATE.animeCount_);
	}
}

//走る
void Player::Run()
{
	//------------------ 移動 ------------------//

	transform_.position_ += moveDir_ * RUN_STATE.SPEED_;

	if (IsBackRange())
	{
		transform_.position_ -= moveDir_ * RUN_STATE.SPEED_;
	}

	transform_.rotate_.vecY = dirAngle_;


	//------------------ アニメーション ------------------//
	
	//走りアニメーションに現フレーム数を変更
	if (Model::GetNowAnimFrame(hModel_) < RUN_STATE.animeStart_)
	{
		Model::SetNowAnimFrame(hModel_, (float)(Model::GetNowAnimFrame(hModel_) + RUN_STATE.animeStart_));
	}

	//走り始め
	if (Model::GetNowAnimFrame(hModel_) < RUN_STATE.animeRoopStart_)
	{
		//走り始め
		Model::SetAnimFrame(hModel_, RUN_STATE.animeStart_, RUN_STATE.animeEnd_, RUN_STATE.animeCount_);
	}
	else
	{
		//走りループ
		Model::SetAnimFrame(hModel_, RUN_STATE.animeRoopStart_, RUN_STATE.animeEnd_, RUN_STATE.animeCount_);
	}


	//------------------ スタミナ ------------------//
	stamina_->DiffGauge(STAMINA_DIFF);
}