//インクルード
#include "AppearancePlayer.h"

#include "../Engine/Model.h"
#include "../Engine/Global.h"

#include "Lantern.h"

//コンストラクタ
AppearancePlayer::AppearancePlayer(GameObject* parent)
	: GameObject(parent, "AppearancePlayer")
	, runAnime_{ 1.5f, 101, 175 }
	, walkAnime_{ 1.2f, 13, 87 }
	, lantern_(nullptr)
	, hModel_(-1)
{
}

//デストラクタ
AppearancePlayer::~AppearancePlayer()
{
}

//初期化
void AppearancePlayer::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Model/Player.fbx");
	assert(hModel_ >= 0);

	//ランタン作成
	lantern_ = Instantiate<Lantern>(GetParent());
	lantern_->Leave();
}

//更新
void AppearancePlayer::Update()
{	
}

//描画
void AppearancePlayer::Draw()
{
	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

	//ランタンの位置更新(アニメーション処理後)
	lantern_->SetRotateY(transform_.rotate_.vecY);
	lantern_->SetPosition(Model::GetBonePosition(hModel_, "MiddleProximal_R"));
}

//開放
void AppearancePlayer::Release()
{
}

//歩くアニメーション設定
void AppearancePlayer::WalkAnimation()
{
	//初期アニメーション設定
	Model::SetAnimFrame(hModel_, walkAnime_.animeStart_, walkAnime_.animeEnd_, walkAnime_.speed_);
	Model::SetNowAnimFrame(hModel_, (float)(walkAnime_.animeStart_));
}

//走るアニメーション設定
void AppearancePlayer::RunAnimation()
{
	//初期アニメーション設定
	Model::SetAnimFrame(hModel_, runAnime_.animeStart_, runAnime_.animeEnd_, runAnime_.speed_);
	Model::SetNowAnimFrame(hModel_, (float)(runAnime_.animeStart_));
}