//インクルード
#include "Lantern.h"

#include "../Engine/Model.h"
#include "../Engine/Light.h"

//コンストラクタ
Lantern::Lantern(GameObject* parent)
	: GameObject(parent, "Lantern")
	, HAND_LENGTH(-1.0f)
	, MAX_BRIGHT(0.8f)
	, MIN_BRIGHT(0.4f)
	, DELAT_BRIGHT(0.001f)
	, bright_(MIN_BRIGHT)
	, deltaTime_(DELAT_BRIGHT)
	, hModel_(-1)
{
}

//デストラクタ
Lantern::~Lantern()
{
}

//初期化
void Lantern::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Model/Lantern.fbx");
	assert(hModel_ >= 0);

	//ライト表示
	Light::Visible();

	//ライトの大きさ設定
	Light::SetScale(Light::GetScale() / 2.0f);
}

//更新
void Lantern::Update()
{
	//明るさの強弱調整
	if (bright_ <= MIN_BRIGHT)
	{
		deltaTime_ = DELAT_BRIGHT;
	}
	if (bright_ >= MAX_BRIGHT)
	{
		deltaTime_ = -DELAT_BRIGHT;
	}

	//明るさ変化
	bright_ += deltaTime_;

	//明るさを設定
	Light::SetBright(bright_);
}

//描画
void Lantern::Draw()
{
	//手持ちの位置に合わせる
	transform_.position_.vecY += HAND_LENGTH;

	//ライトの位置設定
	Light::SetPosition(transform_.position_);

	//モデル描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Lantern::Release()
{
}