#pragma once

//インクルード
#include "../Engine/GameObject.h"

//ゴーストを管理するクラス
class AppearanceGhost : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const float MAX_MOVE;		//最大の移動量
	const float MIN_MOVE;		//最低の移動量
	const float MOVE_GO;		//前進の変化量
	const float MOVE_BACK;		//後退の変化量
	XMVECTOR movePos_;			//移動位置
	float moveDir_;				//移動方向
	int hModel_;				//モデル番号



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	AppearanceGhost(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~AppearanceGhost();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};