//インクルード
#include "Charactor.h"

#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/Global.h"

#include "Map.h"

//コンストラクタ
Charactor::Charactor(GameObject* parent, std::string name)
	: GameObject(parent, name)
	, hModel_(-1)
	, inv_(nullptr)
	, moveDir_(XMVectorZero())
{
}

//デストラクタ
Charactor::~Charactor()
{
}

//初期化
void Charactor::Initialize()
{
	//コマンド管理者生成（キューは一つのみ）
	inv_ = new Invoker(this, true);
}

//更新
void Charactor::Update()
{
}

//描画
void Charactor::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Charactor::Release()
{
	//コマンド管理者を解放
	if (inv_)
	{
		SAFE_DELETE(inv_);
	}
}

//モデル番号を取得
int Charactor::GetModelHandle()
{
	return hModel_;
}