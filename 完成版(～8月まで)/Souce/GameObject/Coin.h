#pragma once

//インクルード
#include "../Engine/GameObject.h"

//コインを管理するクラス
class Coin : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	int hModel_;		//モデル番号



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	Coin(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Coin();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//衝突判定
	//引数：GameObject*(当たった相手)
	//返値：void
	void OnCollision(GameObject* pTarget) override;
};