//インクルード
#include "MushroomManager.h"

//コンストラクタ
MushroomManager::MushroomManager(GameObject* parent)
	: GameObject(parent, "MushroomManager")
{
}

//デストラクタ
MushroomManager::~MushroomManager()
{
}

//初期化
void MushroomManager::Initialize()
{
}

//更新
void MushroomManager::Update()
{
}

//描画
void MushroomManager::Draw()
{
}

//解放
void MushroomManager::Release()
{
}