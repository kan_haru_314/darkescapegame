//インクルード
#include "AppearanceMap.h"

#include "../Engine/Model.h"
#include "../Engine/Global.h"

//コンストラクタ
AppearanceMap::AppearanceMap(GameObject* parent)
	: GameObject(parent, "AppearanceMap")
	, SCALE_MAGNIFICATION(32.0f)
	, ORIGIN_POS(0.0f)
	, MAX_POS(-10.0f)
	, RUN_SPEED(0.15f)
	, WALK_SPEED(0.08f)
	, speed_(0.0f)
	, hModel_(-1)
{
}

//デストラクタ
AppearanceMap::~AppearanceMap()
{
}

//初期化
void AppearanceMap::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Model/AppearanceMap.fbx");
	assert(hModel_ >= 0);

	//サイズ拡大
	transform_.scale_ *= SCALE_MAGNIFICATION;
}

//更新
void AppearanceMap::Update()
{
	//移動
	transform_.position_.vecZ -= speed_;

	//位置リセット
	if (transform_.position_.vecZ <= MAX_POS)
	{
		transform_.position_.vecZ = ORIGIN_POS;
	}
}

//描画
void AppearanceMap::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_, SCALE_MAGNIFICATION);
}

//解放
void AppearanceMap::Release()
{
}

//歩く速度に合わせる
void AppearanceMap::WalkSpeed()
{
	speed_ = WALK_SPEED;
}

//走る速度に合わせる
void AppearanceMap::RunSpeed()
{
	speed_ = RUN_SPEED;
}
