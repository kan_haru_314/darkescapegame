//インクルード
#include "Mushroom.h"

#include "../Engine/Model.h"
#include "../Engine/SphereCollider.h"

#include "Map.h"

#include "../Image/Stamina.h"

//定数宣言
#define STAMINA_DIFF 0.4f		//スタミナ回復(1 〜 -1)
#define COLLIDER_SIZE 2.0f		//球体型の当たり判定の大きさ

//コンストラクタ
Mushroom::Mushroom(GameObject* parent)
	: GameObject(parent, "Mushroom")
	, hModel_(-1)
{
}

//デストラクタ
Mushroom::~Mushroom()
{
}

//初期化
void Mushroom::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Model/Mushroom.fbx");
	assert(hModel_ >= 0);

	//高さは後調整
	transform_.position_.vecY = ((Map*)FindObject("Map"))->GetPositionHeight(transform_.position_);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorZero(), COLLIDER_SIZE);
	AddCollider(collision);
}

//更新
void Mushroom::Update()
{
}

//描画
void Mushroom::Draw()
{
	//モデル描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//解放
void Mushroom::Release()
{
}

//衝突判定
void Mushroom::OnCollision(GameObject* pTarget)
{
	if (pTarget->GetObjectName() == "Player")
	{
		//スタミナ回復
		((Stamina*)FindObject("Stamina"))->DiffGauge(STAMINA_DIFF);
		KillMe();
	}
}
