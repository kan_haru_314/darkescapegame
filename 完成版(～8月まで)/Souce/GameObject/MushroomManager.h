/************************************************************/
/*		描画順番を変えるためだけの空のゲームオブジェクト	*/
/*		そのため、なにもしない								*/
/************************************************************/

#pragma once

//インクルード
#include "../Engine/GameObject.h"

//ゲームシーンの親代わりするクラス
class MushroomManager : public GameObject
{
public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	MushroomManager(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~MushroomManager();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};