#pragma once

//インクルード
#include "../Engine/GameObject.h"

#include "Charactor.h"

//プロトタイプ宣言
class Player;
class Map;

//幽霊を管理するクラス
class Ghost : public Charactor
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const XMVECTOR DEPTH_VEC;		//奥に行くベクトル
	const float SPEED;				//移動速度
	int hModel_;					//モデル番号
	float viewAngle_;				//視角の角度
	float viewLength_;				//視認距離
	float maxSarchTime_;			//サーチモードの最大継続時間
	float dirAngle_;				//向いた方向の角度
	float sarchTime_;				//プレイヤーサーチモード
	Player* player_;				//プレイヤー情報
	Map* map_;						//マップ情報


	//------------------ メソッド ------------------//

	//視野角内にあるか
	//引数：GameObject*(視認するゲームオブジェクト)
	//引数：XMVECTOR(視線ベクトル)
	//返値：XMVECTOR(ゲームオブジェクトの方向)
	XMVECTOR IsViewAngle(GameObject* obj, XMVECTOR view);



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	Ghost(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Ghost();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//衝突判定
	//引数：GameObject*(当たった相手)
	//返値：void
	void OnCollision(GameObject* pTarget) override;


	//------------------ コマンド動作 ------------------//

	//待機
	//引数：なし
	//返値：void
	void Wait() override;

	//歩く
	//引数：なし
	//返値：void
	void Walk() override;

	//走る
	//引数：なし
	//返値：void
	void Run() override;
};