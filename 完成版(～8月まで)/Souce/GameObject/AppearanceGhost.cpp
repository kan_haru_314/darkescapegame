//インクルード
#include "AppearanceGhost.h"

#include "../Engine/Model.h"
#include "../Engine/Global.h"

//コンストラクタ
AppearanceGhost::AppearanceGhost(GameObject* parent)
	: GameObject(parent, "AppearanceGhost")
	, MAX_MOVE(25.0f)
	, MIN_MOVE(0.0f)
	, MOVE_GO(0.02f)
	, MOVE_BACK(-0.04f)
	, movePos_(XMVectorZero())
	, moveDir_(MOVE_GO)
	, hModel_(-1)
{
	movePos_.vecZ = MIN_MOVE;
}

//デストラクタ
AppearanceGhost::~AppearanceGhost()
{
}

//初期化
void AppearanceGhost::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Model/Ghost.fbx");
	assert(hModel_ >= 0);
}

//更新
void AppearanceGhost::Update()
{
	//位置を前後
	if (movePos_.vecZ <= MIN_MOVE)
	{
		//前進
		moveDir_ = MOVE_GO;		
	}
	if (movePos_.vecZ >= MAX_MOVE)
	{
		//後退
		moveDir_ = MOVE_BACK;
	}

	//位置調整
	movePos_.vecZ += moveDir_;
}

//描画
void AppearanceGhost::Draw()
{
	//移動分加算
	Transform trans = transform_;
	trans.position_ += movePos_;

	//描画
	Model::SetTransform(hModel_, trans);
	Model::Draw(hModel_);
}

//開放
void AppearanceGhost::Release()
{
}