//インクルード
#include "Ghost.h"

#include <list>
#include <random>
#include <time.h>

#include "../Engine/Model.h"
#include "../Engine/SphereCollider.h"
#include "../Engine/Global.h"

#include "../Scene/GameScene.h"

#include "Map.h"
#include "Player.h"

//定数宣言
#define COLLIDER_SIZE 1.5f		//球体型の当たり判定の大きさ


//コンストラクタ
Ghost::Ghost(GameObject* parent)
	: Charactor(parent, "Ghost")
	, DEPTH_VEC(XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f))
	, SPEED(0.4f)
	, hModel_(-1)
	, dirAngle_(0.0f)
	, sarchTime_(0.0f)
	, player_(nullptr)
	, map_(nullptr)
{
	viewAngle_ = (float)GetPrivateProfileInt("GHOST", "Angle", 110, ".\\GameSetting.ini");
	viewLength_ = (float)GetPrivateProfileInt("GHOST", "Length", 50, ".\\GameSetting.ini");
	maxSarchTime_ = (float)GetPrivateProfileInt("GHOST", "SarchTime", 6, ".\\GameSetting.ini");
}

//デストラクタ
Ghost::~Ghost()
{
}

//初期化
void Ghost::Initialize()
{
	//コマンドキュー作成
	Charactor::Initialize();

	//モデルデータのロード
	hModel_ = Model::Load("Model/Ghost.fbx");
	assert(hModel_ >= 0);

	//マップオブジェクトを探す
	map_ = (Map*)FindObject("Map");

	//高さは後調整
	transform_.position_.vecY = map_->GetPositionHeight(transform_.position_);

	//開始地点
	transform_.position_.vecZ -= map_->GetMapScale() * 0.8f / 2.0f;

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorZero(), COLLIDER_SIZE);
	AddCollider(collision);

	//プレイヤー情報取得
	player_ = (Player*)FindObject("Player");
}

//更新
void Ghost::Update()
{
	//------------------ 向いてる方向(前) ------------------//

	XMMATRIX mY;
	mY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

	//視線ベクトル
	XMVECTOR view = XMVector3TransformCoord(XMVectorSet(0, 0, 1.0f, 0), mY);

	//正規化
	view = XMVector3Normalize(view);


	//------------------ 動作 ------------------//
	sarchTime_ -= CONVERT_TO_SECOND(1.0f);

	if (sarchTime_ > 0)
	{
		//------------------ サーチモード ------------------//
		moveDir_ = player_->GetPosition() - transform_.position_;
	}
	else
	{
		//------------------ プレイヤー視認 ------------------//
		moveDir_ = IsViewAngle(player_, view);

		//プレイヤーがいるなら、キノコ無視
		if (XMVector3Length(moveDir_).vecX == 0.0f)
		{
			//------------------ キノコを目指す ------------------//
			moveDir_ = FindObject("Mushroom")->GetPosition() - transform_.position_;
		}
	}
	


	//------------------ 向く向いてる方向(後) ------------------//

	//移動方向の正規化
	moveDir_ = XMVector3Normalize(moveDir_);

	//外積を求める
	XMVECTOR crossDir = XMVector3Cross(DEPTH_VEC, moveDir_);

	//方向を「-180 〜 180」の範囲にするための符号
	float sign = 1.0f;
	if (crossDir.vecY < 0)
	{
		sign *= -1.0f;
	}

	//内積を求める
	float dotDir = XMVector3Dot(DEPTH_VEC, moveDir_).vecX;

	//回転角度
	dirAngle_ = XMConvertToDegrees(acos(dotDir)) * sign;


	//------------------ 実行 ------------------//
	
	//コマンドセット
	inv_->SetCmmand(RUN);

	//実行
	inv_->Execute();

	//高さは後調整
	transform_.position_.vecY = map_->GetPositionHeight(transform_.position_);
}

//描画
void Ghost::Draw()
{
	//モデル描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//解放
void Ghost::Release()
{
}

//衝突判定
void Ghost::OnCollision(GameObject* pTarget)
{
	//キノコと衝突
	if (pTarget->GetObjectName() == "Mushroom")
	{
		//プレイヤーサーチ
		sarchTime_ = maxSarchTime_;
		pTarget->KillMe();
	}

	//プレイヤーと衝突
	if (pTarget->GetObjectName() == "Player")
	{
		//ゲームオーバーと伝える
		((GameScene*)GetParent())->GameOver();
	}
}

//視野角内にあるか
XMVECTOR Ghost::IsViewAngle(GameObject* obj, XMVECTOR view)
{
	//プレイヤーへの方向ベクトル
	XMVECTOR objDir = obj->GetPosition() - transform_.position_;

	//視認距離内か
	if (XMVector3Length(objDir).vecX <= viewLength_)
	{
		objDir = XMVector3Normalize(objDir);

		//内積
		float dot = XMVector3Dot(view, objDir).vecX;

		//角度差を取得
		float rad = acos(dot);
		rad = XMConvertToDegrees(rad);

		//視野角
		if (rad <= viewAngle_ / 2.0f)
		{
			//視野内
			objDir.vecY = 0.0f;
			return objDir;
		}
	}

	//未発見の場合は0を返す
	return XMVectorZero();
}

//待機(不使用)
void Ghost::Wait()
{
}

//歩く(不使用)
void Ghost::Walk()
{
}

//走る
void Ghost::Run()
{
	transform_.position_ += moveDir_ * SPEED;

	transform_.rotate_.vecY = dirAngle_;
}
