//インクルード
#include "GameScene.h"

#include <random>
#include <time.h>

#include "../Engine/Text.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Global.h"

#include "../GameObject/Map.h"
#include "../GameObject/Player.h"
#include "../GameObject/MushroomManager.h"
#include "../GameObject/Mushroom.h"
#include "../GameObject/Coin.h"
#include "../GameObject/Ghost.h"

#include "../Image/CoinCount.h"
#include "../Image/Stamina.h"


//コンストラクタ
GameScene::GameScene(GameObject * parent)
	: GameObject(parent, "GameScene")
	, START_MUSHROOM(10)
	, hTextTime_(-1)
	, time_(0)
	, coinCount_(nullptr)
	, map_(nullptr)
	, agentM_(nullptr)
{
	MushroomInterval_ = (float)GetPrivateProfileInt("MUSHROOM", "Interval", 10, ".\\GameSetting.ini");
	MaxCoin_ = GetPrivateProfileInt("COIN", "Create", 10, ".\\GameSetting.ini");
	ClearCoin_ = GetPrivateProfileInt("COIN", "Clear", 5, ".\\GameSetting.ini");
}

//乱数による位置
XMVECTOR GameScene::CreatePosition()
{
	//位置
	XMVECTOR position;

	//乱数で位置設定
	position.vecX = ((float)rand() / 32767.0f - 0.5f) * map_->GetMapRange();
	position.vecZ = ((float)rand() / 32767.0f - 0.5f) * map_->GetMapRange();

	//その位置の高さ取得
	position.vecY = map_->GetPositionHeight(position);

	return position;
}

//ランダムな位置に作成
template<class T>
inline T* GameScene::CreateObject(GameObject* parent)
{
	T* pObject = Instantiate<T>(parent);
	pObject->SetPosition(CreatePosition());

	return pObject;
}


//初期化
void GameScene::Initialize()
{
	//乱数の種セット
	srand((UINT)time(NULL));

	//マップ作成
	map_ = Instantiate<Map>(this);

	//プレイヤー作成
	Player* player = Instantiate<Player>(this);

	//ゴースト作成
	Instantiate<Ghost>(this);

	//コイン作成
	for (int i = 0; i < MaxCoin_; i++)
	{
		CreateObject<Coin>(this);
	}

	//キノコ管理者作成
	agentM_ = Instantiate<MushroomManager>(this);

	//キノコ作成
	for (int i = 0; i < START_MUSHROOM; i++)
	{
		CreateObject<Mushroom>(agentM_);
	}

	//スタミナ作成
	player->SetStamina(Instantiate<Stamina>(this));

	//コインカウント作成
	coinCount_ = Instantiate<CoinCount>(this);
	coinCount_->SetCoinCount(ClearCoin_);

	//経過時間のテキスト
	hTextTime_ = Text::SetText(std::to_string((int)CONVERT_TO_SECOND(time_)), "Meiryo", Text::Point{ 1600.0f, 10.0f }, 70.0f, Text::ConvertColor(255, 255, 255));
}

//更新
void GameScene::Update()
{
	//キノコ作成
	if (!(time_ % (int)CONVERT_TO_FRAME(MushroomInterval_)))
	{
		CreateObject<Mushroom>(agentM_);
	}

	//コイン回収終了(ゲームクリア)
	if (coinCount_->GetCoinCount() >= ClearCoin_)
	{
		GameEnd(true);
	}

	//経過時間
	time_++;

	//時間表示
	Text::ChangeText(hTextTime_, std::to_string((int)CONVERT_TO_SECOND(time_)));
}

//描画
void GameScene::Draw()
{
}

//開放
void GameScene::Release()
{
}

//ゲーム終了処理
void GameScene::GameEnd(bool isClear)
{
	//シーン管理する親クラス
	SceneManager* manager = (SceneManager*)GetParent();

	//ゲームの状態を伝える
	manager->SetGameState(GameState{ isClear, (int)CONVERT_TO_SECOND(time_), coinCount_->GetCoinCount() });

	//エンドシーンへ移動
	manager->ChangeScene(SCENE_ID_END);
}

//ゲームオーバー処理
void GameScene::GameOver()
{
	GameEnd(false);
}
