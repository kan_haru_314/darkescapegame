//インクルード
#include "TitleScene.h"

#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Light.h"

#include "../GameObject/AppearanceMap.h"
#include "../GameObject/AppearancePlayer.h"
#include "../GameObject/AppearanceGhost.h"

#include "../Image/TitleName.h"
#include "../Image/TitleStart.h"

//コンストラクタ
TitleScene::TitleScene(GameObject* parent)
	: GameObject(parent, "TitleScene")
	, CAMERA_POS(XMVectorSet(-5.0f, 4.0f, 15.0f, 0.0f))
	, CAMERA_TARGET(XMVectorSet(5.0f, 2.0f, 0.0f, 0.0f))
	, GHOST_POS(XMVectorSet(0.0f, 0.0f, -50.0f, 0.0f))
	, NAME_POS(XMVectorSet(-0.4f, 0.4f, 0.0f, 0.0f))
	, START_POS(XMVectorSet(0.0f, -0.4f, 0.0f, 0.0f))
	, NAME_SCALE(2.0f)
	, START_SCALE(1.5f)
	, MAX_BRIGHT(0.1f)
	, MIN_BRIGHT(0.001f)
	, DELAT_BRIGHT(0.001f)
	, bright_(MIN_BRIGHT)
	, deltaTime_(DELAT_BRIGHT)
{
}

//初期化
void TitleScene::Initialize()
{
	//マップ作成
	AppearanceMap* map = Instantiate<AppearanceMap>(this);
	map->WalkSpeed();				//歩行スピード

	//プレイヤー作成
	AppearancePlayer* player = Instantiate<AppearancePlayer>(this);
	player->WalkAnimation();		//歩行アニメーション

	//ゴースト作成
	AppearanceGhost* ghost = Instantiate<AppearanceGhost>(this);
	ghost->SetPosition(GHOST_POS);

	//ラベル作成
	TitleName* name = Instantiate<TitleName>(this);
	name->SetPosition(NAME_POS);
	name->SetScale(XMVectorOne * NAME_SCALE);

	//スタート作成
	TitleStart* start = Instantiate<TitleStart>(this);		
	start->SetPosition(START_POS);
	start->SetScale(XMVectorOne * START_SCALE);

	//ライト
	Light::SetBright(bright_);
	Light::SetScale(XMVectorOne);

	//カメラ設定
	Camera::SetPosition(CAMERA_POS);
	Camera::SetTarget(CAMERA_TARGET);
}

//更新
void TitleScene::Update()
{
	//明るさの強弱調整
	if (bright_ <= MIN_BRIGHT)
	{
		deltaTime_ = DELAT_BRIGHT;
	}
	if (bright_ >= MAX_BRIGHT)
	{
		deltaTime_ = -DELAT_BRIGHT;
	}

	//明るさ変化
	bright_ += deltaTime_;

	//明るさを設定
	Light::SetBright(bright_);


	//入力受付
	char input = 0;
	input += Input::IsKeyDown(DIK_RETURN);						//EnterKey
	input += Input::IsKeyDown(DIK_SPACE);						//SpaceKey
	//input += Input::IsMouseButtonDown(DIMOUSE_BUTTON0);		//マウスクリック

	//シーン切り替え入力
	if (input)
	{
		//ゲームへ
		((SceneManager*)GetParent())->ChangeScene(SCENE_ID_GAME);
	}
}

//描画
void TitleScene::Draw()
{
}

//開放
void TitleScene::Release()
{
}
