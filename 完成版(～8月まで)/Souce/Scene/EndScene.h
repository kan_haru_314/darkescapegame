#pragma once

//インクルード
#include "../Engine/GameObject.h"

//プロトタイプ宣言
class Lantern;
class SceneManager;

//エンドシーンを管理するクラス
class EndScene : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const XMVECTOR CAMERA_POS;		//カメラの位置
	const XMVECTOR CAMERA_TARGET;	//カメラの焦点
	const XMVECTOR RETURN_POS;		//リターンテキストの位置
	const XMVECTOR RESULT_POS;		//リザルトテキストの位置
	const float RETURN_SCALE;		//リターンテキストの大きさ
	const float RESULT_SCALE;		//リザルトテキストの大きさ
	Lantern* lantern_;				//ランタンのポインタ
	SceneManager* manager_;			//マネージャーのポインタ



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	EndScene(GameObject* parent);

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};