#pragma once

//インクルード
#include "../Engine/GameObject.h"

//タイトルシーンを管理するクラス
class TitleScene : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const XMVECTOR CAMERA_POS;			//カメラの位置
	const XMVECTOR CAMERA_TARGET;		//カメラの焦点
	const XMVECTOR GHOST_POS;			//ゴーストの位置
	const XMVECTOR NAME_POS;			//タイトルネームの位置
	const XMVECTOR START_POS;			//スタートテキストの位置
	const float NAME_SCALE;				//タイトルネームのサイズ
	const float START_SCALE;			//スタートテキストのサイズ
	const float MAX_BRIGHT;				//最大の明るさ
	const float MIN_BRIGHT;				//最低の明るさ
	const float DELAT_BRIGHT;			//明るさの変化量
	float bright_;						//明るさ
	float deltaTime_;					//時間変化量



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	TitleScene(GameObject* parent);

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};