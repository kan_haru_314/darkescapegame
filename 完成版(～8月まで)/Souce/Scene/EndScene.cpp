//インクルード
#include "EndScene.h"

#include "../Engine/SceneManager.h"
#include "../Engine/Text.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"

#include "../GameObject/AppearanceMap.h"
#include "../GameObject/AppearancePlayer.h"
#include "../GameObject/Lantern.h"

#include "../Image/EndResult.h"
#include "../Image/EndReturn.h"

//コンストラクタ
EndScene::EndScene(GameObject* parent)
	: GameObject(parent, "EndScene")
	, CAMERA_POS(XMVectorSet(3.0f, 3.0f, 15.0f, 0.0f))
	, CAMERA_TARGET(XMVectorSet(5.0f, 2.0f, 0.0f, 0.0f))
	, RETURN_POS(XMVectorSet(0.0f, -0.5f, 0.0f, 0.0f))
	, RESULT_POS(XMVectorSet(-0.4f, 0.6f, 0.0f, 0.0f))
	, RETURN_SCALE(2.0f)
	, RESULT_SCALE(1.5f)
	, lantern_(nullptr)
	, manager_(nullptr)
{
}

//初期化
void EndScene::Initialize()
{
	//シーン管理する親クラス
	manager_ = (SceneManager*)GetParent();

	//ゲームの状態
	GameState state = manager_->GetGameState();

	//マップ作成
	AppearanceMap* map = Instantiate<AppearanceMap>(this);

	//ゲームクリアか
	if (state.isClear_)
	{
		//------------------ ゲームクリア ------------------//

		//プレイヤー作成
		AppearancePlayer* player = Instantiate<AppearancePlayer>(this);

		//プレイヤーを走らせる
		player->RunAnimation();

		//地面を動かす
		map->RunSpeed();
	}
	else
	{
		//------------------ ゲームオーバー ------------------//

		//ランタン作成
		lantern_ = Instantiate<Lantern>(this);
	}

	//戻る画像作成
	EndReturn* returnText = Instantiate<EndReturn>(this);
	returnText->SetPosition(RETURN_POS);
	returnText->SetScale(XMVectorOne * RETURN_SCALE);

	//ラベル作成
	EndResult* result = Instantiate<EndResult>(this);
	result->SetPosition(RESULT_POS);
	result->SetScale(XMVectorOne * RESULT_SCALE);

	//経過時間表示
	Text::SetText("経過時間：" + std::to_string(state.second_) + "秒", "Meiryo", Text::Point{ 200.0f, 400.0f }, 50.0f, Text::ConvertColor(255, 255, 255));

	//コイン枚数表示
	Text::SetText("コイン：" + std::to_string(state.coinCount_) + "枚", "Meiryo", Text::Point{ 200.0f, 600.0f }, 50.0f, Text::ConvertColor(255, 255, 255));

	//カメラ設定
	Camera::SetPosition(CAMERA_POS);
	Camera::SetTarget(CAMERA_TARGET);
}

//更新
void EndScene::Update()
{
	//ゲームオーバーか
	if (!manager_->GetGameState().isClear_)
	{
		//ランタンの位置調整
		lantern_->SetPosition(XMVectorSet(0, 1.5f, 0, 0));
	}

	//入力受付
	char input = 0;
	input += Input::IsKeyDown(DIK_RETURN);					//EnterKey
	input += Input::IsKeyDown(DIK_SPACE);					//SpaceKey
	//input += Input::IsMouseButtonDown(DIMOUSE_BUTTON0);		//マウスクリック

	//シーン切り替え入力
	if (input)
	{
		//タイトルへ
		manager_->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void EndScene::Draw()
{
}

//開放
void EndScene::Release()
{
}
