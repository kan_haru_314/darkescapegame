#pragma once

//インクルード
#include "../Engine/GameObject.h"

//プロトタイプ宣言
class CoinCount;
class Map;
class MushroomManager;

//ゲームシーンを管理するクラス
class GameScene : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const float START_MUSHROOM;		//開始時のキノコ生成数
	float MushroomInterval_;		//キノコ生成間隔(s)
	int MaxCoin_;					//生成するコイン枚数
	int ClearCoin_;				//クリアに必要なコイン枚数
	int hTextTime_;					//経過時間のテキスト番号
	int time_;						//経過時間(frame)
	CoinCount* coinCount_;			//コインの枚数
	Map* map_;						//マップ情報
	MushroomManager* agentM_;		//キノコの親代理人


	//------------------ メソッド ------------------//

	//乱数による位置
	//引数：なし
	//返値：XMVECTOR(位置)
	XMVECTOR CreatePosition();

	//ランダムな位置に作成
	//引数：GameObject*(親オブジェクト)
	//返値：void
	template <class T>
	T* CreateObject(GameObject* parent);

	//ゲーム終了処理
	//引数：bool(true = クリア, false = ゲームオーバー)
	//返値：void
	void GameEnd(bool isClear);

public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	GameScene(GameObject* parent);

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//ゲームオーバー処理
	//引数：なし
	//返値：void
	void GameOver();
};