//インクルード
#include "EndResult.h"

#include "../Engine/Image.h"
#include "../Engine/Global.h"
#include "../Engine/SceneManager.h"

//コンストラクタ
EndResult::EndResult(GameObject* parent)
	: GameObject(parent, "EndResult")
	, hPict_(-1)
{
}

//デストラクタ
EndResult::~EndResult()
{
}

//初期化
void EndResult::Initialize()
{
	//モデルデータのロード
	if (((SceneManager*)FindObject("SceneManager"))->GetGameState().isClear_)
	{
		//クリア
		hPict_ = Image::Load("Image/EndGameClear.png");
	}
	else
	{
		//ゲームオーバー
		hPict_ = Image::Load("Image/EndGameOver.png");
	}

	assert(hPict_ >= 0);
}

//更新
void EndResult::Update()
{
}

//描画
void EndResult::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//解放
void EndResult::Release()
{
}