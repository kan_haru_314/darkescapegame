//インクルード
#include "Stamina.h"

#include "../Engine/Image.h"

//定数宣言
#define GAUGE_POS_CALC(scale) scale / 3.75f - 0.4f

//コンストラクタ
Stamina::Stamina(GameObject* parent)
	: GameObject(parent, "Stamina")
	, ICON_POS(XMVectorSet(-0.45f, 0.9f, 0.0f, 0.0f))
	, GAUGE_SIZE(1.5f)
	, hPictIcon_(-1)
	, hPictFrame_(-1)
	, hPictGaugeBack_(-1)
	, hPictGauge_(-1)
	, gauge_(1.0f)
{
}

//デストラクタ
Stamina::~Stamina()
{
}

//初期化
void Stamina::Initialize()
{
	//------------------ アイコン ------------------//
	//アイコン
	hPictIcon_ = Image::Load("Image/StaminaIcon.png");
	assert(hPictIcon_ >= 0);

	//位置調整
	transformIcon_.position_ = ICON_POS;


	//------------------ ゲージ ------------------//
	//背景
	hPictGaugeBack_ = Image::Load("Image/StaminaGaugeBack.png");
	assert(hPictGaugeBack_ >= 0);

	//青ゲージ
	hPictGauge_ = Image::Load("Image/StaminaGauge.png");
	assert(hPictGauge_ >= 0);

	//枠
	hPictFrame_ = Image::Load("Image/StaminaFrame.png");
	assert(hPictFrame_ >= 0);

	//サイズ、位置の調整
	transform_.scale_ *= GAUGE_SIZE;
	transform_.position_.vecY = ICON_POS.vecY;

	transformGauge_ = transform_;
}

//更新
void Stamina::Update()
{
}

//描画
void Stamina::Draw()
{
	//アイコン
	Image::SetTransform(hPictIcon_, transformIcon_);
	Image::Draw(hPictIcon_);

	//背景
	Image::SetTransform(hPictGaugeBack_, transform_);
	Image::Draw(hPictGaugeBack_);

	//青ゲージ
	Image::SetTransform(hPictGauge_, transformGauge_);
	Image::Draw(hPictGauge_);

	//枠
	Image::SetTransform(hPictFrame_, transform_);
	Image::Draw(hPictFrame_);
}

//解放
void Stamina::Release()
{
}

//ゲージを変化させる
void Stamina::DiffGauge(float value)
{
	//変化量を反映
	gauge_ = Clamp(gauge_ + value, 0.0f, 1.0f);

	//横幅
	transformGauge_.scale_.vecX = gauge_ * GAUGE_SIZE;

	//位置調整
	transformGauge_.position_.vecX = GAUGE_POS_CALC(gauge_ * GAUGE_SIZE);
}

//ゲージ量を取得
float Stamina::GetGauge()
{
	return gauge_;
}

//値を範囲内にとどめる（※公式リファレンスから抜粋）
template<class T>
inline constexpr const T& Stamina::Clamp(const T& value, const T& low, const T& high)
{
	if (high < value)
	{
		return high;
	}

	if (value < low)
	{
		return low;
	}

	return value;
}