#pragma once

//インクルード
#include "../Engine/GameObject.h"

//「-START-」を管理するクラス
class TitleStart : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const int MAX_ALPHA;		//最大透明度
	const int MIN_ALPHA;		//最低透明度
	const int DELAT_ALPHA;		//透明度の変化量
	int alpha_;					//透明度
	int deltaTime_;				//時間変化量
	int hPict_;					//画像番号



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	TitleStart(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~TitleStart();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};