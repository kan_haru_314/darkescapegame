#pragma once

//インクルード
#include "../Engine/GameObject.h"

//「RETURN TITLE」を管理するクラス
class EndReturn : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	int hPict_;			//画像番号



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	EndReturn(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~EndReturn();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;
};