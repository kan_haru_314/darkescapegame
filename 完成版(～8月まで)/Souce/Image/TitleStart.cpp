//インクルード
#include "TitleStart.h"

#include "../Engine/Image.h"
#include "../Engine/Global.h"

//コンストラクタ
TitleStart::TitleStart(GameObject* parent)
	: GameObject(parent, "TitleStart")
	, MAX_ALPHA(255)
	, MIN_ALPHA(125)
	, DELAT_ALPHA(1)
	, alpha_(MIN_ALPHA)
	, deltaTime_(DELAT_ALPHA)
	, hPict_(-1)
{
}

//デストラクタ
TitleStart::~TitleStart()
{
}

//初期化
void TitleStart::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Image/TitleStart.png");
	assert(hPict_ >= 0);
}

//更新
void TitleStart::Update()
{
	//透明度の強弱調整
	if (alpha_ <= MIN_ALPHA)
	{
		deltaTime_ = DELAT_ALPHA;
	}
	if (alpha_ >= MAX_ALPHA)
	{
		deltaTime_ = -DELAT_ALPHA;
	}

	//透明度変化
	alpha_ += deltaTime_;

	//透明度を設定
	Image::SetAlpha(hPict_, alpha_);
}

//描画
void TitleStart::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//解放
void TitleStart::Release()
{
}