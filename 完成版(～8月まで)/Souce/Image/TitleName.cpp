//インクルード
#include "TitleName.h"

#include "../Engine/Image.h"
#include "../Engine/Global.h"

//コンストラクタ
TitleName::TitleName(GameObject* parent)
	: GameObject(parent, "TitleName")
	, hPict_(-1)
{
}

//デストラクタ
TitleName::~TitleName()
{
}

//初期化
void TitleName::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Image/TitleName.png");
	assert(hPict_ >= 0);
}

//更新
void TitleName::Update()
{
}

//描画
void TitleName::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//解放
void TitleName::Release()
{
}