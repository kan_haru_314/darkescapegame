#pragma once

//インクルード
#include "../Engine/GameObject.h"

//スタミナを管理するクラス
class Stamina : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ アイコン ------------------//
	const XMVECTOR ICON_POS;		//アイコン画像の位置
	Transform transformIcon_;		//アイコンの画像のトランスフォーム
	int hPictIcon_;					//アイコン画像番号


	//------------------ ゲージ ------------------//
	const float GAUGE_SIZE;			//ゲージの長さ倍率
	Transform transformGauge_;		//ゲージの画像のトランスフォーム
	int hPictFrame_;				//枠の画像番号
	int hPictGaugeBack_;			//背景の画像番号
	int hPictGauge_;				//ゲージの画像番号
	float gauge_;					//ゲージ量


	//------------------ メソッド ------------------//

	//値を範囲内にとどめる（※公式リファレンスから抜粋）
	//引数：template&(操作する値)
	//引数：template&(最低値)
	//引数：template&(最大値)
	//返値：template&(範囲内の値)
	template <class T>
	constexpr const T& Clamp(const T& value, const T& low, const T& high);



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	Stamina(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~Stamina();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//ゲージを変化させる
	//引数：float(変化する値)
	//返値：void
	void DiffGauge(float value);

	//ゲージ量を取得
	//引数：なし
	//返値：float(ゲージ量)
	float GetGauge();
};