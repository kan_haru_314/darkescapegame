//インクルード
#include "EndReturn.h"

#include "../Engine/Image.h"
#include "../Engine/Global.h"

//コンストラクタ
EndReturn::EndReturn(GameObject* parent)
	: GameObject(parent, "EndReturn")
	, hPict_(-1)
{
}

//デストラクタ
EndReturn::~EndReturn()
{
}

//初期化
void EndReturn::Initialize()
{
	//モデルデータのロード
	hPict_ = Image::Load("Image/EndReturn.png");
	assert(hPict_ >= 0);
}

//更新
void EndReturn::Update()
{
}

//描画
void EndReturn::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//解放
void EndReturn::Release()
{
}