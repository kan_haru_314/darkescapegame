#pragma once

//インクルード
#include "../Engine/GameObject.h"

//スタミナゲージを管理するクラス
class CoinCount : public GameObject
{
private:
	//----------------------------- [ private ] -----------------------------//

	//------------------ フィールド ------------------//
	const std::string COUNT_TEXT;	//コイン表示の定型文
	const XMVECTOR COUNT_POS;		//位置
	std::string clearCoinText_;		//クリアに必要なコイン枚数
	int hPictIcon_;					//アイコン画像番号
	int hTextCount_;				//カウントテキスト番号
	int coinCount_;					//コインの枚数



public:
	//----------------------------- [ public ] -----------------------------//

	//------------------ メソッド ------------------//

	//コンストラクタ
	//引数：GameObject*(親オブジェクト)
	//返値：なし
	CoinCount(GameObject* parent);

	//デストラクタ
	//引数：なし
	//返値：なし
	~CoinCount();

	//初期化
	//引数：なし
	//返値：void
	void Initialize() override;

	//更新
	//引数：なし
	//返値：void
	void Update() override;

	//描画
	//引数：なし
	//返値：void
	void Draw() override;

	//解放
	//引数：なし
	//返値：void
	void Release() override;

	//コインカウント+1
	//引数：なし
	//返値：void
	void CoinCountUp();

	//獲得したコイン枚数を取得
	//引数：なし
	//返値：int(コイン枚数)
	int GetCoinCount();

	//必要なコイン枚数をセット
	//引数：int(コイン枚数)
	//返値：void
	void SetCoinCount(int clearCoin);
};