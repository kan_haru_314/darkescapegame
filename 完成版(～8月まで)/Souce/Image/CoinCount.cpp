//インクルード
#include "CoinCount.h"

#include "../Engine/Image.h"
#include "../Engine/Text.h"

//コンストラクタ
CoinCount::CoinCount(GameObject* parent)
	: GameObject(parent, "CoinCount")
	, COUNT_TEXT("x ")
	, COUNT_POS(XMVectorSet(-0.85f, 0.9f, 0.0f, 0.0f))
	, clearCoinText_("")
	, hPictIcon_(-1)
	, hTextCount_(-1)
	, coinCount_(0)
{
	int a = 0;
}

//デストラクタ
CoinCount::~CoinCount()
{
}

//初期化
void CoinCount::Initialize()
{
	//アイコンロード
	hPictIcon_ = Image::Load("Image/CoinIcon.png");
	assert(hPictIcon_ >= 0);

	//コインの枚数テキスト
	hTextCount_ = Text::SetText(COUNT_TEXT + clearCoinText_ + std::to_string(coinCount_), "Meiryo", Text::Point{ 200.0f, 10.0f }, 70.0f, Text::ConvertColor(255, 255, 255));

	//位置の調整
	transform_.position_ = COUNT_POS;
}

//更新
void CoinCount::Update()
{
}

//描画
void CoinCount::Draw()
{
	//アイコン
	Image::SetTransform(hPictIcon_, transform_);
	Image::Draw(hPictIcon_);
}

//解放
void CoinCount::Release()
{
}

//コインカウント+1
void CoinCount::CoinCountUp()
{
	coinCount_++;

	Text::ChangeText(hTextCount_, COUNT_TEXT + clearCoinText_ + std::to_string(coinCount_));
}

//獲得したコイン枚数を取得
int CoinCount::GetCoinCount()
{
	return coinCount_;
}

//必要なコイン枚数をセット
void CoinCount::SetCoinCount(int clearCoin)
{
	clearCoinText_ = std::to_string(clearCoin) + " / ";

	Text::ChangeText(hTextCount_, COUNT_TEXT + clearCoinText_ + std::to_string(coinCount_));
}
